<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

if(PHP_SAPI != 'cli') exit;

require_once(__DIR__ . "/../libs/libvgdb-scripts.php");

function usage() {
  $bn = basename(__FILE__);

  die(<<<EOF
$bn <COMMAND>       [COMMAND-OPTIONS]

$bn import-plats    [ifile=data/in/liste_plats.csv]
$bn plats           [ifile=data/in/noms-plats-nutrinet.txt [ofile=templates/plats-select.html]]
$bn import-groups   [ifile=data/in/FOOD_GROUPS-vgdb.csv] [reset]
$bn groups          [ofile=templates/group-select.html]
$bn import-food     [ifile=data/in/FOOD-vgdb.csv] [reset] [force]
$bn import-cpn      [ifile=data/in/COMPILED_DATA-vgdb.csv]
$bn alim            [ofile=data/gen/ciqual.json]
$bn masvol
$bn import-recette  <ifile>
$bn import-massvol
$bn import-massvol   <ifile> [reset]

Usage, COMMAND can be:
 import-plats     (ré)import une liste de plats standards tout en conservant ceux rajoutés postérieurement via l'UI'
 plats            régénère la liste des plats présentée dans le <select> utilsé pour l'ajout de correspondance Nutrinet à de nouveaux plats
 import-groups    (ré)import une liste de groupes d'aliments supplémentaires qui s'ajoutent à la base Ciqual (le mot clef optionnel "reset" supprime les groupes vegans précédents)
                  Colonnes attendues: ORIGGPCD (colonne n°1), ORIGGPFR (colonne n°2). TAB-separated
 groups           régénère la liste <select> des groupes d'aliments utilisable pour la saisie de nouveaux ingrédients

 import-food      (ré)import une liste d'aliments supplémentaires qui s'ajoutent à la base Ciqual (le mot clef optionnel "reset" supprime les aliments vegans précédents)
                  Colonne attendues: "ORIGGPCD (code groupe)", "ORIGFDCD (code ingrédient)", "ORIGFDNM (nom ingrédient)". TAB-separated
 import-cpn       import (en écrasant si nécessaire) les valeurs nutritionnelles pour des ingrédients vegan déjà importés au préalable
                  Colonnes attendues: ORIGFDCD et toutes les colonnes de COMPONENTS exactement orthographiées. TAB-separated
 alim             régénère la liste <select> des aliments utilisable pour la saisie de nouveaux ingrédients
 masvol           produit un export JSON des masses volumiques saisies via l'application
 import-recette   importe une recette au format JSON

 export-massvol    expoter une liste de masses volumiques au format JSON
 import-massvol    importe une liste de masses volumiques au format JSON

EOF
  );
}

switch(@$argv[1]) {
case "import-plats":
  \vgdb\scripts\import_plats(@$argv[2]);
  break
    ;;
case "plats":
  \vgdb\scripts\gen_plats(@$argv[2], @$argv[3]);
  break
    ;;
case "import-groups":
  import_groups(@$argv[2], @$argv[3] == 'reset');
  break
    ;;
case "groups":
  \vgdb\scripts\gen_groups(@$argv[2]);
  break
    ;;
case "import-food":
  import_food(@$argv[2], @$argv[3] == 'reset', @$argv[3] == 'force' || @$argv[4] == 'force');
  break
    ;;
case "import-cpn":
  \vgdb\scripts\import_cpn(@$argv[2]);
  break
    ;;
case "alim":
  \vgdb\scripts\gen_alim(@$argv[2]);
  break
    ;;
case "masvol":
  require_once(__DIR__ . '/../libs/libvgdb.php');
  echo json_encode(\vgdb\io\export_masvol());
  break
    ;;
case "import-recette":
  require_once(__DIR__ . '/../libs/libvgdb.php');
  $tab = array_slice($argv, 2);
  if(!$tab) exit(1);
  \vgdb\io\import($tab);
  break
    ;;
case "import-recette":
  require_once(__DIR__ . '/../libs/libvgdb.php');
  $tab = array_slice($argv, 2);
  if(!$tab) exit(1);
  \vgdb\io\import($tab);
  break
    ;;
case "export-massvol":
  require_once(__DIR__ . '/../libs/libvgdb-massvol.php');
  \vgdb\massvol\export(FALSE);
  break
    ;;
case "import-massvol":
  require_once(__DIR__ . '/../libs/libvgdb-massvol.php');
  if(!isset($argv[2]) || ! is_readable($argv[2])) exit(1);
  \vgdb\massvol\import($argv[2], @$argv[3] == 'reset');
  break
    ;;

default:
  usage();
  break;
}


// TODO: not vgdb specific
// move into libCiqual.php ?
function import_groups($ifile = NULL, $reset = FALSE) {
  require_once(__DIR__ . '/../connect.php');
  global $db, $DBPX;

  if(!$ifile) $ifile = DATADIR . '/in/FOOD_GROUPS-vgdb.csv';
  $ifile = realpath($ifile);

  if($reset) {
    $db->query("DELETE FROM ${DBPX}FOOD_GROUPS WHERE ORIGGPENG IS NULL");
  }

  $res = $db->query(<<<EOF
LOAD DATA INFILE "$ifile" REPLACE INTO TABLE ${DBPX}FOOD_GROUPS CHARACTER SET utf8 FIELDS TERMINATED BY '\t' ENCLOSED BY '"' ESCAPED BY '\\\\' IGNORE 1 LINES;
EOF
  );
  printf("%d lignes affectées : %s\n", $res->rowCount(), $res->rowCount() != 0 ? "OK" : "erreur ?");
}


// TODO: not vgdb specific, see outils.php
// move into libCiqual.php ?
function import_food($ifile = NULL, $reset = FALSE, $force = FALSE) {
  require_once(__DIR__ . '/../connect.php');
  require_once(__DIR__ . '/../libs/libCiqual.php');
  global $db, $DBPX;

  if(!$ifile) $ifile = DATADIR . '/in/FOOD-vgdb.csv';
  $ifile = realpath($ifile);

  try {
    $warn = [];
    list($stmt, $stmt_vars, $values) = \Ciqual\Food\importFromFilePrepare($ifile, $warn, $force);
    if(! $stmt) {
      printf("can't import food\n");
      return FALSE;
    }
  } catch (\Ciqual\Exception $e) {
    printf("error: can't import food: {$e->getMessage()}\n");
    return FALSE;
  }

  list($deleted, $added) = \Ciqual\Food\importFromFile($stmt, $stmt_vars, $values, $reset);
  printf("Import d'aliments ($deleted supprimés, $added ajoutés)\n");
}
