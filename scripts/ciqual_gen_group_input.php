<?php

// php ciqual_gen_group_input.php > templates/group-select.html

require_once(__DIR__ . "/../libCiqual.php");


echo '<select name="groups" id="groups">' . "\n";

// way 1:
/*
  foreach($db->query("SELECT ORIGGPCD, ORIGGPFR FROM {$DBPX}FOOD_GROUPS") as $cat) {
  echo '<option value="' . $cat['ORIGGPCD'] . '">' . $cat['ORIGGPFR'] . '</option>' . "\n";
  }
*/

// way 2: (group'ed <option>)
$optgroup = NULL;

$excluded = array('05.1', '07', '08.1', '08.2', '09', '10', '11', '12', '25.1', '25.2');
$glob_excluded = array('06.*', '13.*', '14.*');

$q = sprintf("SELECT g.ORIGGPCD, g.ORIGGPFR, COUNT(ORIGFDNM) AS c"
	     ." FROM {$DBPX}FOOD_GROUPS g LEFT JOIN {$DBPX}FOOD f ON f.ORIGGPCD = g.ORIGGPCD"
	     ." WHERE g.ORIGGPCD NOT IN (%s)"
	     ." AND g.ORIGGPCD NOT REGEXP %s"
	     ." GROUP BY g.ORIGGPCD",

	     implode(',', array_map(array($db, "quote"), $excluded)),
	     $db->quote('^(' . implode('|', $glob_excluded) . ')$'));


foreach($db->query($q) as $cat) {

  if($cat['c'] == 0) {
    // un "group" d'<option> est ouvert ? on le referme
    if(! is_null($optgroup)) {
      echo '</optgroup>' . "\n";
      $optgroup = NULL;
    }
    // et on ouvre le nouveau
    echo '<optgroup label="' . $cat['ORIGGPFR'] . '">' . "\n";
    $optgroup = $cat['ORIGGPCD'];
    continue;
  }

  // un optgroup est ouvert et l'option n'a pas le même préfix ?
  // alors on ferme cet optgroup.
  if($optgroup && strpos($cat['ORIGGPCD'], $optgroup . '.', 0) === FALSE) {
    echo '</optgroup>' . "\n";
    $optgroup = NULL;
  }
  echo '<option value="' . $cat['ORIGGPCD'] . '">' . $cat['ORIGGPFR'] . '</option>' . "\n";       
}

echo '</select>' . "\n";
