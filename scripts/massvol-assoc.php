<?php

// deux sources
// http://www.zpag.net/Cuisine/Equivalence.htm
// http://www.fao.org/infoods/infoods/tables-et-bases-de-donnees/bases-de-donnees-faoinfoods-sur-la-composition-des-aliments/fr/
$conf_split = ','; // ';'
$conf_cmp = 'ENGFDNAM'; // ORIGFDNM

if($argc == 2 && $argv[1] == '-') $handle = fopen('php://stdin', 'r');
else $handle = fopen(DATADIR . '/masses-volumiques.orig.csv', 'r');

mb_internal_encoding ("UTF-8"); // mb_strlen();


function remove_plural($s, $length = 5) {
  $t = [];
  foreach(explode(' ', $s) as $w) {
    if(mb_strlen($w) >= $length && substr($w, -1) == 's') $t[] = substr($w, 0, -1);
    else $t[] = $w;
  }
  return implode(' ', $t);
}


$massvol = array();
while (($m = fgetcsv($handle, 500, ',')) !== FALSE) {
  $massvol[$m[0]] = $m[1];
}
$alim = array_keys($massvol);

require_once(__DIR__ . '/../libCiqual.php');

$res = array_map(function($n) use($conf_cmp) { return $n[$conf_cmp]; }, $db->query("SELECT DISTINCT ORIGFDNM, ENGFDNAM from vgdb_FOOD")->fetchAll(PDO::FETCH_ASSOC));

$res_leven = array();
$substitutions = array();
foreach($alim as $v) {
  $levs = NULL;

  $v = strtolower($v);
  // classique
  $filtered_v = str_replace([ ' crue', ' cru', ' cuit', ' moulue', ' moulu', ' sec', ' hachée', ' haché', 'poudre' ],
			    [ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' ],
			    $v);
  // FAO XLS

  $filtered_v = str_replace([ ' boiled', ' dry', '(average)', ' dried', ' unsalted', ' canned', ' unsweetened' ],
			    [ ' ', ' ', '', ' ', ' ', ' ', ' ' ],
			    $filtered_v);

  // pre-filtering
  // way 1
  if(TRUE) {
    $prefilter = $res;
    $words = explode(' ', $v);

    foreach($words as $i => $word) {
      $w = trim($word);
      $w = remove_plural($w);
      if($i != 0 && mb_strlen($w) < 3) continue;
      $temp_list = array_filter($prefilter, function ($n) use($w, $i) {
	  $n = remove_plural($n);
	  if(mb_strlen($w) > 5) {
	    return strpos(strtolower($n), $w) !== FALSE;
	  }
	  else {
	    // if($i != 0) var_dump("$n -- $w");
	    return preg_match("/\b" . preg_quote($w) . "\b/", strtolower($n));
	  }
	});
      if(!$temp_list) { if($i == 0) break /* TODO: no match on 1st word... what to do */ ; else continue; } // 1er mot obligatoire, les autres affinent optionnellement
      $prefilter = $temp_list;
    }
  } else {
    // way 2
    $v = $first_word = trim($v);
    if(strpos($v, ' ')) $first_word = substr($v, 0, strpos($v, ' ') ? : NULL);
    $prefilter = array_filter($res, function ($n) use($first_word) { return strpos(strtolower($n), $first_word) !== FALSE; });
  }



  // echo $v . ": "; print_r($prefilter) . "\n";
  if(! $prefilter || count($prefilter) == count($res)) continue;




  foreach($prefilter as $k => $name) {
    $name = $name_first_word = strtolower($name);
    // if(strpos($name, ' ')) $name_first_word = substr($name, 0, strpos($name, ' '));

    $lev = levenshtein($v, $name);
    if(is_null($levs) || $lev < $levs) {
      $levs = $lev;
      $res_leven[$v] = $name; //$k;
    }
  }

  // sed -r 's/:\s*([0-9]+)/!\1!/' data/masses-volumiques.1.csv|sort -nk2 -t '!'
  if($levs < 6) {
    // $country = explode('-', $res_leven[$v]);
    // $country = strtoupper(end($country));
    /* if(isset($countries[$country])) {
      $substitutions[$country][] = $v;
      // drush_print("$v: {$res_leven[$v]}");
    }
    else */
    echo "$v: {$res_leven[$v]}" . "\n";
    
  }
  else
    echo "nearest match for\t\"$v\"\t\t:\t$levs ({$res_leven[$v]})" . "\n";
}
