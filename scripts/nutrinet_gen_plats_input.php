<?php

// php nutrinet_gen_plats_input.php > ../templates/plats-select.html

$h = fopen(__DIR__ . '/../data/noms-plats-nutrinet.txt', 'r');

echo '<select name="nom_nutrinet[]" id="nom-nutrinet" multiple="multiple">' . "\n";
echo '<option value="" selected="selected">Aucun</option>' . "\n";

while( ($f = fgets($h)) ) { // while read f; do wc -m <<<"$f"; done < ../data/noms-plats-nutrinet.txt|sort -n|tail -1
  printf('<option value="%s">%s</option>' . "\n", addcslashes(trim($f), '"'), trim($f));
}

echo '</select>' . "\n";
