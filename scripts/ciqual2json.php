<?php

require_once("libCiqual.php");

$tab = array();
foreach($db->query("SELECT ORIGGPCD, ORIGGPFR FROM {$DBPX}FOOD_GROUPS") as $cat) {
  $y = array();
  foreach($db->query("SELECT ORIGFDCD, ORIGFDNM FROM {$DBPX}FOOD WHERE ORIGGPCD = {$cat['ORIGGPCD']}")  as $ingr) {
    $y[] = array($ingr['ORIGFDCD'], $ingr['ORIGFDNM']);
  }
  $tab[$cat['ORIGGPCD']] = array($cat['ORIGGPFR'], $y);
}
print json_encode($tab);
