<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

require_once("libs/libCiqual.php");
require_once("libs/libvgdb.php");
require_once("libs/libvgdb-sys.php");

if(!isset($_REQUEST['recette_id']) && !isset($_REQUEST['import'])) {
  header('Location: index.php');
  exit;
}

// REQUEST: #recette-div-metadata POST, #ingredients_form_liste GET (TODO)
if(isset($_REQUEST['recette_id'])) $recette_id = intval($_REQUEST['recette_id']);

if(isset($_FILES['file'])) {
  $recette = \vgdb\Recette\get($recette_id);
  if(! $recette) die(json_encode([]));
  if(! is_writable(__DIR__ . '/upload')) die(json_encode([]));

  $ifile = $_FILES['file']['tmp_name'];
  if(! file_exists($ifile)) die(json_encode([]));

  $pi = pathinfo($_FILES['file']['name']);

  $slug = sprintf('%03d_%s',
                  $recette['id'],
                  strtolower(iconv('utf-8', 'ascii//TRANSLIT', trim(preg_replace('~[^\\pL\d]+~u', '-', $pi['filename']), '-'))));
  if(! $slug) die(json_encode([]));

  $dfile = NULL;
  if(! file_exists(__DIR__ . '/upload/' . $slug . '.' . $pi['extension'])) {
    $dfile = $slug . '.' . $pi['extension'];
  } else {
    $i = 0;
    do {
      if(! file_exists(__DIR__ . '/upload/' . $slug . '-' . $i . '.' . $pi['extension'])) {
        $dfile = $slug . '-' . $i . '.' . $pi['extension'];
        break;
      }
    } while($i++ < 10);
  }

  if(! $dfile) die(json_encode([]));

  $x = move_uploaded_file($ifile, __DIR__ . '/upload/' . $dfile);
  header('Content-type: application/json');
  die(json_encode(\vgdb\Recette\getImages($recette['id'])));
}

if(isset($_GET['getfiles'])) {
  $recette = \vgdb\Recette\get($recette_id);
  if(! $recette) die(json_encode([]));
  header('Content-type: application/json');
  die(json_encode(\vgdb\Recette\getImages($recette['id'])));
}

if(isset($_POST['delete_img']) && $_POST['delete_img']) {
  $recette = \vgdb\Recette\get($recette_id);
  if(! $recette) die('1');

  $imgs = \vgdb\Recette\getImages($recette['id'], TRUE);
  $search = $_POST['delete_img'];
  array_walk($imgs, function ($v) use($search)
             {
               if($v['uri'] === $search) {
                 die(unlink($v['rel']) ? '0' : '1');
                 return;
               }
             });
  die('1');
}


if(isset($_POST['metamod'])) {
  $mod = \vgdb\Recette\alter($_POST['recette_id'], $_POST['nom'], $_POST['recette-metadata'], $_POST['recette-freetext']);
  if($mod === TRUE) // true
    list($last_code, $last_message) = [0, "Modification d'informations de la recette effectuée"];
  elseif($mod === FALSE) // false
    list($last_code, $last_message) = [1, "Erreur de modification de la recette"];
  // else $mod === 0: UPDATE sans modification SQL
}
/*
  Les modifications effectuées par l'UI ne sont pas synchrones.
  Un formulaire HTML interne est modifié puis entièrement posté.
  Ici, côté serveur, nous supprimons tous les ingrédients de cette
  recette et rajoutons les nouveaux.
  Raisons, non-exhaustives:
  - moins d'appels javascript asynchrones et plus de réactivité en faible latence
  - moins de gestion d'états des ingrédients individuels
  - moins de risque de données inconsistantes dans une recette
  - moins d'opérations à gérer côté DB (ingrédient modifié, ...)
  - pas d'opération sur un ID d'ingrédient individuel

  Problème: gestion collaborative d'une recette soumise aux risques d'écrasement
  en cas d'édition simultanée. À fixer avec un pooling régulier de la recette côté client ?
*/
elseif(isset($_POST['add'])) {
  $recette = \vgdb\Recette\get($recette_id);
  if(! $recette) die('error: aucun plat correspondant');

  try {
    $ingredients = \vgdb\Recette\_ingredientFormToArray(@$_POST['ingr'], $recette['id'], $recette['id_plat']);
  } catch(Exception $e) { die($e->getMessage()); }

  if(\vgdb\Recette\addIngredients($recette['id'], $ingredients)) {
    /* header("Location: " . $_SERVER['PHP_SELF'] . "?recette_id=$recette_id");
       exit; */
    list($last_code, $last_message) = [0, "Ingrédients modifiés"];
  }
  else {
    // debug
    var_dump($db, $ingredients, implode(',', $ingredients), $db->errorInfo());die;
  }
}




$recette = \vgdb\Recette\get($recette_id, TRUE);
if(!$recette) { die('Une erreur de définition de n° de recette est survenue'); }

$recette_nom = $recette['nom'];

$plat = \vgdb\Plat\get($recette['id_plat']);
$ingr_sql_liste = \vgdb\Recette\getIngredients($recette_id, 0x01); // grab extended data

$stats = []; $masse_recette = $sql_calories = 0;
if(isset($_GET['stats']) && $_GET['stats'] == 1 && count($ingr_sql_liste)) {
  // require_once("stats-ingredients.php");
  
  require_once("libs/libvgdb-ciqual.php");
  $stats = ['nutriIndex' => [],
            'total' => [],
            'data' => [] ];

  \vgdb\CiqualTools\GetAllNutrientsForRecipe($recette_id,
                                             $stats['nutriIndex'],
                                             $masse_recette,
                                             VGDB_ALL_KEEP);

  \vgdb\CiqualTools\ComputeRecetteValNut($stats['nutriIndex'],
                                         $masse_recette,
                                         $stats['total'],
                                         VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_ROUND|VGDB_TRACES_KEEP);

  // reloop to grab intermediate values... :|
  \vgdb\CiqualTools\ComputeDetailledRelativeRecetteValNut($stats['nutriIndex'],
                                                          $masse_recette,
                                                          $stats['data'],
                                                          VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_ROUND|VGDB_TRACES_KEEP);

  // simply to sort ingredients array lines by decreasing weight
  $s = $stats['order'] = \vgdb\CiqualTools\getOrderedIngredientsFromNutrientsArray(current($stats['nutriIndex']));

  uksort($stats['data'], function($a, $b) use($s) { return $s[$a] <= $s[$b]; });
  $stats['order'] = array_reverse($stats['order'], TRUE);

  // fetch the names
  $s = &$stats['names'];
  array_walk($ingr_sql_liste, function ($v) use(&$s) { $s[$v['ORIGFDCD']] = $v['ORIGFDNM']; });

  // gen the header
  $stats["header"] = \vgdb\CiqualTools\generateNutriHeader($stats["total"]);

  // sames final values from another calculation method to do check we are right.
  list($masse_recette2, $somme_calories, $total_calories_100g2) = \vgdb\CiqualTools\reliable_check_kcal($recette_id);
  $stats['misc'] = ['masse2' => $masse_recette2, 'sum_kcal2' => $somme_calories ];

  $total_calories_100g = $stats['total'][\Ciqual\Component\KCAL_CODE]['somme'];
  if(intval($total_calories_100g) != intval($total_calories_100g2)) {
    list($last_code, $last_message) = [1, "ERREUR de CALCUL des calories : $total_calories_100g <> $total_calories_100g2 !" ];
  }
 
  // var_dump($stats);
  define('FINAL_TIME',	time() - $_SERVER['REQUEST_TIME']);
}


if(isset($_REQUEST['dump'])) {
  $t = [// 'id' => $recette_id,
    'recette' => $recette,
    'ingredients' => $ingr_sql_liste ];
  if($stats) $t['stats'] = $stats;
  // var_dump($t);die;
  header("Content-Type: text/plain");
  echo json_encode($t);
  die;
}

require_once("libs/rain.tpl.class.php");
$tpl = new raintpl();
raintpl::$tpl_dir = "templates/";
// conserve nos chemins custom avec <base>, pour l'instant
// raintpl::$path_replace = false;

$tpl->assign(array(
  // header
  "summary_link" => TRUE,
  "summary_plat_id" => $plat['id'],
  "last_message" => isset($last_message) ? $last_message : NULL,
  "last_code" => isset($last_code) ? $last_code : NULL,

  "recette" => $recette,
  "plat" => $plat,
  "ingredients_saisis_form" => $ingr_sql_liste,

  // stats-recette
  "stats" => $stats,
  "masse_recette" => $masse_recette,
));

// includes "form-ajout-ingredient" template
// ... which itself includes "group-select" template
$tpl->draw( "ingredients" );
