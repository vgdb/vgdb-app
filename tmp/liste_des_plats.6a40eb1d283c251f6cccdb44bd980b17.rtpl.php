<?php if(!class_exists('raintpl')){exit;}?><!doctype html>
<html>
  <head>
    <link href="templates/misc.css" rel="stylesheet" type="text/css" />
  </head>

  <body>
    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>


    <h1> Liste des plats </h1>

    <ul>
      <?php $counter1=-1; if( isset($liste_des_plats) && is_array($liste_des_plats) && sizeof($liste_des_plats) ) foreach( $liste_des_plats as $key1 => $value1 ){ $counter1++; ?>

      <li>
	<a href="recettes.php?plat_id=<?php echo $value1["id"];?>"><?php echo $value1["nom_vegetalise"];?></a>
	<?php if( $value1["nom_traditionnel"] ){ ?>(équiv. <em><?php echo $value1["nom_traditionnel"];?></em>)<?php } ?> [<?php echo $value1["c"];?> recettes]
      </li>
      <?php } ?>

    </ul>

    <form action="index.php">
      <h2>Ajouter un nouveau plat</h2>
      <label for="v">Nom végétalisé :</label>
      <input id="v" type="text" name="nom_vegetalise" />
      <label for="plats-nutrinet">Nom du plat traditionnel :</label>
      <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("plats-select") . ( substr("plats-select",-1,1) != "/" ? "/" : "" ) . basename("plats-select") );?>

      <input type="hidden" name="add" value="1" />
      <input type="submit" value="Ajouter" />
    </form>

    <p id="#index-notice"> Si vous constatez qu'un intitulé de plat
      est trop vague et devrait être divisé en deux plats différents, merci d'en
      informer <a href="mailto:nutrinetvege@gmail.com">nutrinetvege@gmail.com</a> en indiquant
      "Liste de plats" dans le sujet du mail. </p>
