<?php if(!class_exists('raintpl')){exit;}?><!doctype html>
<html>
  <head>
    <link href="templates/misc.css" rel="stylesheet" type="text/css" />
    <title>Masses volumiques</title>
  </head>

  <body>
    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>


    <h1>Masses volumiques</h1>

    <p><a href="masvol.php?export">Effectuer un export des valeurs des masses volumiques</a></p>

    <form id="form-masvol" method="POST">
      <input type="hidden" name="go" value="go" />
      <ul>
	<?php $counter1=-1; if( isset($ingr_volume) && is_array($ingr_volume) && sizeof($ingr_volume) ) foreach( $ingr_volume as $key1 => $value1 ){ $counter1++; ?>

	<li <?php if( is_null($value1["masvol"]) ){ ?>class="todo"<?php } ?>>
	  <input type="text"
		 size="10"
		 id="<?php echo $value1["ORIGFDCD"];?>"
		 name="<?php echo $value1["ORIGFDCD"];?>"
		 value="<?php echo $value1["masvol"];?>" /> g/mL
	  <label for="<?php echo $value1["ORIGFDCD"];?>">
	    <a href="ingredients.php?recette_id=<?php echo $value1["id_recette"];?>">
	      <?php echo $value1["ORIGFDNM"];?></a>
	    (<?php echo $value1["ORIGFDCD"];?>)
	  </label>
	</li>
      <?php }else{ ?>

      Aucun ingrédient saisi en volume pour l'instant
      <?php } ?>

      </ul>
      <input type="submit" value="Modifier" />
    </form>

    <p><a href="masvol.php?purge">Supprimer les masses volumiques inutilisées (ex: suite au passage de l'unité d'un ingrédient de mL à grammes)</a></p>

  </body>
</html>
