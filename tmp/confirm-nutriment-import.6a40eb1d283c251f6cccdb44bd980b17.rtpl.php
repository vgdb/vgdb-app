<?php if(!class_exists('raintpl')){exit;}?><!doctype html>
<html>
  <head>
    <link href="templates/misc.css" rel="stylesheet" type="text/css" />
  </head>

  <body>
    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>


    <div>
      <p>Pour importer les valeurs nutritionnelles de ces <?php echo count( $new );?> aliments, l'import supprimera toutes les valeurs nutritionnelles pré-existantes pour <strong><?php echo $deleted;?></strong> d'entre eux <?php if( $reset ){ ?>(<strong>car la reinitialisation a été demandée</strong>)<?php } ?>.</p>
      <p><em>debug</em>: Le fichier envoyé est <strong><a href="<?php echo \vgdb\sys\path2uri( $fichier );?>"><?php echo $fichier;?></a></strong></p>

      <p>Est-ce bien ce que vous souhaitez ?</p>
    </div>
    <form method="POST">
      <input type="hidden" name="q-import-cpn" value="1"/>
      <input type="hidden" name="q-import-cpn-reset" value="<?php echo $reset;?>"/>
      <input type="hidden" name="sessid" value="<?php echo $sessid;?>" />
      <input type="submit" value="Confirmer" />
    </form>
  </body>
</html>

