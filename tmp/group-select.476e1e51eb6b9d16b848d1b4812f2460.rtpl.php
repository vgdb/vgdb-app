<?php if(!class_exists('raintpl')){exit;}?><select name="groups" id="groups">
<optgroup label="Aliments &quot;moyens&quot; pour les enqu&ecirc;tes Inca"><!-- AM -->
<option value="AM">Aliments "moyens" pour les enquêtes Inca</option><!-- n:66:66 -->
</optgroup>
<optgroup label="C&eacute;r&eacute;ales et p&acirc;tes"><!-- 01 -->
<option value="01.1">Farines et amidons</option>
<option value="01.2">Riz et autres graines</option>
<option value="01.3">Pâtes et semoules</option>
<option value="01.4V">Seitan et autres dérivés des céréales</option>
<option value="01.5V">Steaks végétaux et galettes à base de céréales</option>
</optgroup>
<optgroup label="Boulangerie-viennoiserie"><!-- 02 -->
<option value="02.1">Pains</option>
<option value="02.3">Biscottes et pains non levés</option>
<option value="02.4">Viennoiseries et brioches</option>
</optgroup>
<optgroup label="P&acirc;tisseries et biscuits"><!-- 03 -->
<option value="03.1">Gâteaux et pâtisseries</option>
<option value="03.2">Biscuits secs sucrés</option>
<option value="03.3">Biscuits salés apéritifs</option>
<option value="03.4">Préparations pour pâtisseries</option>
</optgroup>
<optgroup label="C&eacute;r&eacute;ales petit d&eacute;jeuner et barres c&eacute;r&eacute;ali&egrave;res"><!-- 04 -->
<option value="04">Céréales petit déjeuner et barres céréalières</option><!-- n:29:29 -->
</optgroup>
<optgroup label="Mati&egrave;res grasses"><!-- 08 -->
<option value="08.3">Margarines et matières grasses composées</option>
<option value="08.4">Huiles et graisses végétales</option>
</optgroup>
<optgroup label="L&eacute;gumes"><!-- 15 -->
<option value="15">Légumes</option><!-- n:92:97 -->
<option value="15.1V">Algues</option>
</optgroup>
<optgroup label="L&eacute;gumes secs"><!-- 16 -->
<option value="16">Légumes secs</option><!-- n:10:17 -->
<option value="16.1V">Tofu et autres dérivés des légumineuses</option>
<option value="16.2V">Steaks végétaux et galettes à base de légumineuses</option>
</optgroup>
<optgroup label="Pommes de terre et apparent&eacute;s"><!-- 17 -->
<option value="17">Pommes de terre et apparentés</option><!-- n:15:15 -->
</optgroup>
<optgroup label="Fruits"><!-- 18 -->
<option value="18.1">Fruits frais</option>
<option value="18.2">Fruits séchés ou lyophilisés</option>
<option value="18.3">Fruits au sirop/au jus, compotes</option>
</optgroup>
<optgroup label="Jus et nectars"><!-- 19 -->
<option value="19">Jus et nectars</option><!-- n:38:38 -->
</optgroup>
<optgroup label="Graines ol&eacute;agineuses et ch&acirc;taignes"><!-- 20 -->
<option value="20">Graines oléagineuses et châtaignes</option><!-- n:25:29 -->
<option value="20.1V">Pâtes nature à base de graines oléagineuses</option>
<option value="20.2V">Pâtes aromatisées à base de graines oléagineuses</option>
</optgroup>
<optgroup label="Sucres et confiseries"><!-- 21 -->
<option value="21.1">Sucres, miels, sirops</option>
<option value="21.2">Confitures</option>
<option value="21.3">Confiseries non chocolatées</option>
<option value="21.4">Chocolats et produits à base de chocolat</option>
</optgroup>
<optgroup label="Boissons sans alcool"><!-- 22 -->
<option value="22.1">Boissons rafraîchissantes sans alcool</option>
<option value="22.2">Eaux</option>
<option value="22.4">Café, thé, infusions, boissons au cacao</option>
<option value="22.5">Préparations pour boissons sans alcool</option>
</optgroup>
<optgroup label="Boissons alcoolis&eacute;es"><!-- 23 -->
<option value="23.1">Bières et similaires</option>
<option value="23.2">Cidres et similaires</option>
<option value="23.3">Vins</option>
<option value="23.4">Liqueurs et alcools</option>
<option value="23.5">Cocktails</option>
</optgroup>
<optgroup label="Salades compos&eacute;es et crudit&eacute;s"><!-- 24 -->
<option value="24">Salades composées et crudités</option><!-- n:9:9 -->
</optgroup>
<optgroup label="Plats compos&eacute;s"><!-- 25 -->
<option value="25">Plats composés</option><!-- n:6:56 -->
<option value="25.3">Plats à base de légumes ou légumineuses</option>
<option value="25.4">Plats à base de céréales ou pâtes</option>
<option value="25.5">Plats à base de fromage</option>
<option value="25.6">Pizzas, crêpes et tartes salées</option>
</optgroup>
<optgroup label="Sandwichs"><!-- 26 -->
<option value="26">Sandwichs</option><!-- n:28:28 -->
</optgroup>
<optgroup label="Soupes et bouillons"><!-- 27 -->
<option value="27.1">Soupes prêtes à consommer</option>
<option value="27.2">Bouillons prêts à consommer</option>
<option value="27.9">Soupes et bouillons non reconstitués</option>
</optgroup>
<optgroup label="Assaisonnements et sauces"><!-- 28 -->
<option value="28.3">Herbes, épices et assaisonnements</option>
<option value="28.4">Sauces salées et condiments</option>
</optgroup>
<optgroup label="Denr&eacute;es destin&eacute;es &agrave; une alimentation particuli&egrave;re"><!-- 29 -->
<option value="29">Denrées destinées à une alimentation particulière</option><!-- n:6:6 -->
</optgroup>
<optgroup label="Compl&eacute;ments alimentaires"><!-- 31 -->
<option value="31">Compléments alimentaires</option><!-- n:2:2 -->
</optgroup>
<optgroup label="Aliments infantiles"><!-- 33 -->
<option value="33">Aliments infantiles</option><!-- n:28:28 -->
</optgroup>
<optgroup label="Boissons v&eacute;g&eacute;tales et produits d&eacute;riv&eacute;s"><!-- 105 -->
<option value="105.1">Boissons végétales</option>
<option value="105.3">Crèmes végétaliennes et spécialités à base de crème</option>
<option value="105.4">Yaourts et spécialités végétales type yaourts</option>
</optgroup>
<optgroup label="Fromages v&eacute;g&eacute;taux"><!-- 106 -->
<option value="106.1">Fromages végétaux type pâte molle</option>
<option value="106.5">Fromages végétaux type pâte ferme</option>
</optgroup>
<optgroup label="Simili carn&eacute;s et plats &agrave; base de simili carn&eacute;s"><!-- 109 -->
<option value="109.3">Simili charcuteries</option>
</optgroup>
</select>
