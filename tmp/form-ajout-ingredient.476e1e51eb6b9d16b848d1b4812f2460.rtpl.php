<?php if(!class_exists('raintpl')){exit;}?><div>
  <label for="form-select-ingredient"> ou sélectionner l'ingrédient parmi les groupes :</label>
  <form id="form-select-ingredient" name="add" action="javascript:ajoutIngredient()">
    <!-- groups d'ingrédients -->
    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("group-select") . ( substr("group-select",-1,1) != "/" ? "/" : "" ) . basename("group-select") );?>

    <!-- select dynamique de sélection d'un ingrédient d'un groupe -->
    <select name="ingredients" id="ingredients"></select>
    <input type="text" name="quantite" id="quantite">
    <select name="unite" id="unite">
      <option value="g">gramme</option>
      <option value="mg">milli-gramme</option>
      <option value="L">litre</option>
      <option value="mL">milli-litre</option>
      <option value="cup">tasse</option>
    </select>
    <input type="submit" value="OK">
  </form>
</div>
