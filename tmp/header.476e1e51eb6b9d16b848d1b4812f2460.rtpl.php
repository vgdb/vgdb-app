<?php if(!class_exists('raintpl')){exit;}?><!-- préfix espace du href pour bypasser le système de remplacement de chemin de RainTPL sans se prendre la tête -->
<!-- ce système de déconnexion ne fonctionne qu'avec firefox -->
<div id="logout-link"><a href=" http://nobody@<?php  echo MY_CONST_HOSTNAME;?>/<?php  echo MY_CONST_PATH;?>#" onclick="alert('Malgré le message d\'avertissement, vous pouvez sans aucun risque cliquer sur OUI pour vous déconnecter');">Déconnexion</a></div>
<div id="lastlog"><?php echo vgdb\log\show(); ?></div>

<?php if( isset($summary_link) ){ ?>

<ul id="breadcrumb">
  <?php if( ! @$no_main_link ){ ?>

  <li>Retour à la <a href="index.php">Liste des plats</a></li>
  <?php } ?>


  <?php if( @$summary_plat_id ){ ?>

  <li>Retour à la <a href="recettes.php?plat_id=<?php echo $summary_plat_id;?>">Liste des recette pour ce plat</a></li>
  <?php } ?>


</ul>
<?php } ?>


<?php if( isset($last_message) ){ ?>

<div class="last-message return-<?php echo $last_code;?>"><?php echo $last_message;?></div>
<?php } ?>

