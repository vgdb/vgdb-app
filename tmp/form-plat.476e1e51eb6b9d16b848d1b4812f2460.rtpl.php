<?php if(!class_exists('raintpl')){exit;}?><?php if( ! UI_PLATS ){ ?>

<p><em>La modification de plat via interface est désactivée, veuillez utiliser l'import</em></p>

<?php }else{ ?>


<form method="POST">
  <table><tr><td>
	<label for="p">Prioritaire :</label>
      </td><td>
	<input id="p" type="checkbox" name="prioritaire" value="1" 
	       <?php if( @$plat && $plat['prioritaire'] == 1 ){ ?>

	       checked="checked"
	       <?php } ?>

	       />
    </td></tr><tr><td>
	<label for="v">Nom végétalisé :</label>
      </td><td>
	<input id="v" type="text" name="nom_vegetalise" size="40"
	       value="<?php if( @$plat ){ ?><?php echo $plat["nom_vegetalise"];?><?php } ?>"/>
    </td></tr><tr><td>
  <label for="nom-nutrinet">Nom du plat traditionnel :</label>
      </td><td>
	<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("plats-select") . ( substr("plats-select",-1,1) != "/" ? "/" : "" ) . basename("plats-select") );?>

    </td></tr><tr><td colspan="2">
  <?php if( @$plat ){ ?>

  <input type="hidden" name="plat_id" value="<?php echo $plat["id"];?>" />
  <input type="hidden" name="mod-plat" value="1" />
  <input type="submit" value="Modifier" />
  <?php }else{ ?>

  <input type="hidden" name="add-plat" value="1" />
  <input type="submit" value="Ajouter" />
  <?php } ?>

  </td></tr></table>
</form>

<?php if( @$plat ){ ?>

<script>
$(function() {
	$("#nom-nutrinet").val(<?php echo json_encode( $nom_traditionnels );?>);
});
</script>
<?php } ?>


<?php } ?>

