<?php if(!class_exists('raintpl')){exit;}?><!doctype html>
<html>
  <head>
    <script type="text/javascript" src="templates/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="templates/nutrinet-vg-saisie-recette.js"></script>
    <link href="templates/misc.css" rel="stylesheet" type="text/css" />

    <!-- autocompletion -->
    <link href="templates/jquery-ui-1.10.3/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="templates/jquery-ui-1.10.3/ui/minified/jquery-ui.min.js"></script>
    <script type="text/javascript" src="templates/my-ingredient-autocomplete.js"></script>

    <script src="templates/dmuploader.min.js"></script>
    <script src="templates/dmuploader-config.js"></script>

    <script>
      $('document').ready(function() {
      bindddz(<?php echo $recette["id"];?>);
      showpreview(<?php echo $recette["id"];?>);
      });
    </script>

    <title>Ingrédients (saisie) pour <?php echo $recette["nom"];?></title>
  </head>

  <body>

    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>


    <h1><?php echo $recette["nom"];?>, variante de <u><?php echo $plat["nom_vegetalise"];?></u> <?php if( isset($plat["nom_traditionnel"]) ){ ?> (équiv. <?php echo $plat["nom_traditionnel"];?>) <?php } ?></h1>

    <div class="inline-links">
      <img src="templates/images/edit.png" alt="Modifier" title="Modifier" border="0" />&nbsp;
      <a href="" onclick="$('#recette-div-metadata').toggle(); return false;">Méta-données</a>

      <img src="templates/images/table.png" alt="Export Table HTML" title="Export Table HTML" border="0" />&nbsp;
      <a href="export.php?ids=<?php echo $recette["id"];?>&amp;fmt=table">Exporter en table HTML</a>

      <img src="templates/images/save.png" alt="Save (json dump)" title="Save (json dump)" border="0" />&nbsp;
      <a href="ingredients.php?recette_id=<?php echo $recette["id"];?>&dump=1">JSON dump (fichier de sauvegarde réimportable)</a>

      <?php if( @$_GET["stats"] == 1 ){ ?>

      
      <img src="templates/images/nostats.png" alt="No statistiques" title="No statistiques" border="0" />&nbsp;
      <a href="ingredients.php?recette_id=<?php echo $recette["id"];?>">Cacher les statistiques nutritionnelles</a>
      <?php }else{ ?>

      <img src="templates/images/stats.gif" alt="Statistiques" title="Statistiques" border="0" />&nbsp;
      <a href="ingredients.php?recette_id=<?php echo $recette["id"];?>&stats=1">Afficher les statistiques nutritionnelles</a>
      <?php } ?>


      <img src="templates/images/delete.png" alt="Supprimer" title="Supprimer" border="0" />&nbsp;
      <a onclick="return window.confirm('Supprimer la recette : <?php echo $recette["nom"];?> ?')" href="recettes.php?del=1&amp;recette_id=<?php echo $recette["id"];?>&amp;plat_id=<?php echo $plat["id"];?>">Supprimer</a>

    </div>

    <div id="recette-div-metadata" cellpadding="10px">
      <table><tr><td style="vertical-align: top">
	    <form method="POST">
	      <input type="hidden" name="metamod" value="1" />
	      <input type="hidden" name="recette_id" value="<?php echo $recette["id"];?>" />
              <label for="nom-vg">Nom de la recette :</label>
	      <input type="text" name="nom" id="nom-vg" value="<?php echo $recette["nom"];?>" size="40"/><br/>
	      <label for="recette-metadata">Meta-données (<span onmousedown="$('#recette-meta-help').toggle();" id="toggle-info">information sur la notation des champs normalisés</span>) :</label>
	      <textarea name="recette-metadata" id="recette-metadata" cols="40" rows="10"><?php echo htmlentities( $recette["metadata"] );?></textarea>
	      <label for="recette-freetext">Texte libre :</label>
	      <textarea name="recette-freetext" id="recette-freetext" cols="40" rows="10"><?php echo htmlentities( $recette["freetext"] );?></textarea>
	      <input type="submit" style="width: 70px" value="Modifier" />
	    </form>
	  </td>
	  <td style="vertical-align: top;">
	    <div id="recette-meta-help"><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("recette-meta-help") . ( substr("recette-meta-help",-1,1) != "/" ? "/" : "" ) . basename("recette-meta-help") );?></div>
      </td></tr>
	<!-- inside a table -->
	<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("upload-recette-img") . ( substr("upload-recette-img",-1,1) != "/" ? "/" : "" ) . basename("upload-recette-img") );?>

      </table>
    </div>

    <p><u>Ingrédients saisis</u></p>

    <p style="font-size: small">
      Saisissez la quantité totale de chaque ingrédient de la recette <strong>sans se vous préoccuper du nombre de personnes</strong> (les quantités seront automatiquement normalisées).<br/>
      La recette détaillée peut être précisée grâce au lien d'édition des <em>méta-données</em> ci-dessus.<br/>
      En cas <strong>d'ingrédient manquant</strong>, merci d'<strong>envoyer un email</strong> à <a href="mailto:thomas@l214.com?subject=ingrédient manquant&body=Bonjour Thomas, un ingrédient manque pour la recette <?php echo $recette["id"];?> : <indiquez ici votre ingrédient>">thomas@l214.com</a>.</p>

    <noscript>
      <p style="text-align:center; font-size:large; text-decoration: underline; color: red">Veuillez activer le javascript pour éditer les ingrédients</p>
    </noscript>

    <form method="POST" id="ingredients_form_liste">
      <input type="hidden" name="add" value="1" />
      <input type="hidden" name="recette_id" value="<?php echo $recette["id"];?>" />

      <!-- le template boucle sur $ingr_sql_liste (ingredients.php) -->
      <?php $counter1=-1; if( isset($ingredients_saisis_form) && is_array($ingredients_saisis_form) && sizeof($ingredients_saisis_form) ) foreach( $ingredients_saisis_form as $key1 => $value1 ){ $counter1++; ?>

      <input id="ingr-<?php echo $value1["ORIGFDCD"];?>" class="un-ingredient" type="hidden" value="<?php echo $value1["ORIGFDCD"];?>:<?php echo $value1["quantite"];?>:<?php echo $value1["unite"];?>" name="ingr[]">
      <?php } ?>


      <table id="ingredients_table_liste" border="1"><!-- JS filled --></table>
      <input type="submit" id="valider-recette" value="Valider" />
    </form>

    <p><u>Ajouter des ingrédients</u></p>
    <div style="margin-bottom: 30px">
      <label for="inac">Rechercher un ingrédient :</label>
      <input type="text" name="ingr-autocompletion" id="inac" value="" size="70"/>
    </div>

    <div>
      <label id="label-form-select-ingredient" for="form-select-ingredient">et/ou sélectionner l'ingrédient parmi les groupes :</label>
      <form id="form-select-ingredient" name="add" action="javascript:ajoutIngredient()">
	<!-- groups d'ingrédients -->
	<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("group-select") . ( substr("group-select",-1,1) != "/" ? "/" : "" ) . basename("group-select") );?>

	<!-- select dynamique de sélection d'un ingrédient d'un groupe -->
	<select name="ingredients" id="ingredients"></select>
	<input type="text" name="quantite" id="quantite" placeholder="quantité (valeur numérique)" />
	<select name="unite" id="unite">
	  <option value="g">gramme</option>
	  <option value="mL">milli-litre</option>
	  <option value="cc">cuillère à café (rase: <?php  echo VOL_CC;?> mL)</option>
	  <option value="cs">cuillère à soupe (rase: <?php  echo VOL_CS;?> mL)</option>
	  <!-- <option value="cup">tasse (250 mL)</option> -->
	</select>
	<input type="submit" value="OK">
      </form>
    </div>

    <?php if( $stats ){ ?>

    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("stats-recette") . ( substr("stats-recette",-1,1) != "/" ? "/" : "" ) . basename("stats-recette") );?>


    <footer style="margin-top: 50px; text-align: right">Page chargée en <strong><?php  echo FINAL_TIME;?></strong> secondes.</footer>
    <?php } ?>


    <p style="font-size: small; margin-top: 50px">En participant à la saisie de recettes vous accepter la diffusion des compositions et usages nutritionnels sous licence <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/fr/">Creative Common BY-NC-SA</a><br/>
      <em>© L214 et contributeurs</em><br/>
      <img src="http://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </p>

