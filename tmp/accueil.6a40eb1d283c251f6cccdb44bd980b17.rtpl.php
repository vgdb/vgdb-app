<?php if(!class_exists('raintpl')){exit;}?><!doctype html>
<html>
  <head>
    <link href="templates/misc.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="templates/jquery-2.0.3.min.js"></script>
    <title>Accueil. <?php echo $nb_plats;?> plats, <?php echo $nb_recettes;?> recettes</title>
  </head>

  <body>
    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>


    <p id="intro"> Bienvenue sur vgdb, la base de données nutritionnelle et culinaire du pôle Nutrition de L214.<br/>
      Veillez à consulter le <a href="templates/manual_fr.html" target="_blank">manuel</a> avant de participer.</p>

    <p id="index-notice">Les plats dont les recettes sont à saisir en priorité se distingue par un <a href="index.php?prio=1">fond rouge</a><br/>
      Si vous constatez qu'un intitulé de plat est trop vague et devrait être divisé en deux plats différents,<br/>
      merci d'en informer <a href="mailto:nutrinetvege@gmail.com?subject=Liste de plats">nutrinetvege@gmail.com</a> en indiquant <em>Liste de plats</em> dans le sujet du mail. </p>

    <h1>Liste des plats</h1>

    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("plats-filter2") . ( substr("plats-filter2",-1,1) != "/" ? "/" : "" ) . basename("plats-filter2") );?>


    <hr/>

    <ul id="accueil-alpha-index">
	<?php $counter1=-1; if( isset($alpha) && is_array($alpha) && sizeof($alpha) ) foreach( $alpha as $key1 => $value1 ){ $counter1++; ?>

	<li><a href="#accueil-plats-<?php echo $key1;?>"><?php echo $key1;?></a>&nbsp;</li>
	<?php } ?>

    </ul>

    <div id="plats">
      <?php $counter1=-1; if( isset($alpha) && is_array($alpha) && sizeof($alpha) ) foreach( $alpha as $key1 => $value1 ){ $counter1++; ?>

      <p id="accueil-plats-<?php echo $key1;?>"><?php echo $key1;?></p>
      <ul>
	<?php $counter2=-1; if( isset($value1) && is_array($value1) && sizeof($value1) ) foreach( $value1 as $key2 => $value2 ){ $counter2++; ?>

	<li class="prio-<?php echo $value2["prioritaire"];?>"><a href="recettes.php?plat_id=<?php echo $value2["id"];?>"><?php echo $value2["nom_vegetalise"];?></a>
	<?php if( $value2["nom_traditionnel"] ){ ?>(équiv. <em><?php echo $value2["nom_traditionnel"];?></em>)<?php } ?> [<?php echo $value2["c"];?> recettes] <?php if( $value2["id"] >= 1000 ){ ?>(<span class="plat-from-ui">new!</span>)<?php } ?>

	</li>
	<?php } ?>

      </ul>
      <?php }else{ ?>

      <em>Aucun plat n'existe ou ne correspond aux critères spécifiés.</em>
      <?php } ?>

    </div>

    <!--
    <ul>
      <?php $counter1=-1; if( isset($liste_des_plats) && is_array($liste_des_plats) && sizeof($liste_des_plats) ) foreach( $liste_des_plats as $key1 => $value1 ){ $counter1++; ?>

      <li>
	<a href="recettes.php?plat_id=<?php echo $value1["id"];?>"><?php echo $value1["nom_vegetalise"];?></a>
	<?php if( $value1["nom_traditionnel"] ){ ?>(équiv. <em><?php echo $value1["nom_traditionnel"];?></em>)<?php } ?> [<?php echo $value1["c"];?> recettes]
      </li>
      <?php } ?>

    </ul>
-->

    <hr/>

    <?php if( UI_PLATS && \vgdb\sys\isadmin() ){ ?>

    <h2 id="index-add-plat">Ajouter un nouveau plat</h2>
    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("form-plat") . ( substr("form-plat",-1,1) != "/" ? "/" : "" ) . basename("form-plat") );?>

    <hr/>
    <?php } ?>


    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("sommaire-outils") . ( substr("sommaire-outils",-1,1) != "/" ? "/" : "" ) . basename("sommaire-outils") );?>

    <hr/>
