<?php

ini_set("display_startup_errors", "1");
ini_set("display_errors", "1");
error_reporting(E_ALL);

global $db, $DBPX, $O_params;
$db = NULL;
$DBPX = '';

// volume d'une cuillère à café
define('VOL_CC', 5); // could be 6.6
// volume d'une cuillère à soupe
define('VOL_CS', 15); // could be 20

// whether or not new dishes are allowed to be entered from the UI
// (otherwise only import of a whole set is available)
define('UI_PLATS', FALSE);

// if you change this one, take car of fixing manually
// templates/nutrinet-vg-saisie-recette.js which use a fixed path
define('DATADIR', __DIR__ . '/data');

define('TMPDIR', __DIR__ . '/tmp');
define('TPLDIR', __DIR__ . '/templates');
define('MY_CONST_PATH', 'vgdb/');

// see also \Ciqual\Component\NB_NUTRI (could be 57 or 58)
// in libs/libCiqual.php which defines the number of nutrients expected in
// the current Ciqual database (57 in v2012, 58 in v2013)


// connection to the database from data within .htdb.ini
function connect() {
  global $db, $O_params;
  if(!$db) {
    // httpd.conf pattern denies access to ^.ht.*$
    $db_conf_file = '.htdb.ini';

    // local/beta
    if(! isset($_SERVER['HTTP_HOST']) || $_SERVER['HTTP_HOST'] == 'nut') $db_conf_file = '.htdb.local.ini';


    $x = array_intersect_key(parse_ini_file(__DIR__ . '/' . $db_conf_file, FALSE),
                             array_flip(['dsn','dbu','dbp', 'px', 'export.masvol', 'export.outfile', 'admins']));

    // these could be used later (storing dumps & co)
    $O_params = array_intersect_key($x, array_flip(['export.masvol', 'export.outfile', 'admins']));
    // these à used for the DB
    extract(array_intersect_key($x, array_flip(['dsn','dbu','dbp', 'px'])));

    if($px && preg_match('/^[a-z0-9_]{1,8}/i', $px)) {
      global $DBPX; $DBPX = $px;
    }
    $db = new PDO($dsn, $dbu, $dbp);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
  return $db;
}

connect();

if(PHP_SAPI !== 'cli') {
  // RainTPL sux at handling $GLOBALS._SERVER.HTTP_HOST inside <a href>
  define('MY_CONST_HOSTNAME', $_SERVER['HTTP_HOST']);
}
