<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

require_once("connect.php");
require_once("libs/libCiqual.php");
require_once("libs/libvgdb.php");
require_once("libs/libvgdb-sys.php");

if(isset($_POST['useradd'])) {
  \vgdb\sys\noAdmin_bailOut();

  list($last_code, $last_message) = \vgdb\users\add(".htpasswd", $_POST['user'], $_POST['pass']);
}

if(isset($_GET['updjson'])) {
  require_once("libs/libvgdb-scripts.php");
  \vgdb\scripts\gen_alim(NULL);

  list($last_code, $last_message) = [0, "fichier d'autocomplétion mis à jour"];
}


elseif(isset($_REQUEST['import-recettes'])) {
  \vgdb\sys\noAdmin_bailOut();

  if(isset($_FILES['data']['tmp_name']) && array_filter($_FILES['data']['tmp_name'])) {
    $ret = \vgdb\io\import($_FILES['data']['tmp_name']);
    if($ret['log']) {
      list($last_code, $last_message) = [ 1, implode('<br/>', $ret['log']) ];
    }
    elseif($ret['total']) {
      list($last_code, $last_message) = [ $ret['total'] == 0 || $ret['succeeded'] > 0 ? 0 : 1,
                                         sprintf("%d/%d recettes importées", $ret['succeeded'], $ret['total']) ];
    }
  } else {
    list($last_code, $last_message) = [ 1, 'no files' ];
  }
}

elseif(isset($_GET['dump'])) {
  $t = [];
  foreach(\vgdb\Recette\getAll(TRUE) as $r) {
    $ingr = \vgdb\Recette\getIngredients($r['id'], 0x01);
    $t[] = [ 'recette' => $r, 'ingredients' => $ingr ];
  }
  // header("Content-Type: text/plain");
  echo json_encode($t);
  die;
}
elseif(isset($_GET['dump-masvol'])) {
  // header("Content-Type: text/plain");
  echo json_encode(\vgdb\io\export_masvol());
  die;
}

elseif(isset($_POST['import-plats'])) { // equiv scripts/import_plats.sql
  \vgdb\sys\noAdmin_bailOut();

  session_start();
  $dfile = null;

  if(isset($_POST['sessid']) && $_POST['sessid'] == session_id() && isset($_SESSION['platfiletoopen'])  && $_SESSION['platfiletoopen']) {
    // confirmation
    $dfile = $_SESSION['platfiletoopen'];
    session_destroy();
  }
  elseif(isset($_FILES['data']['tmp_name']) && $_FILES['data']['tmp_name']) {
    $ifile = $_FILES['data']['tmp_name'];
    $dfile = TMPDIR . '/new-plat-db-' . basename($ifile);
    move_uploaded_file($ifile, $dfile);
  }

  // TODO: remove this dup' check once this is stable
  // keep the one in \vgdb\Plat\importFromFilePrepare()
  if(! file_exists($dfile)) {
    list($last_code, $last_message) = [1, "Import du fichier de plats impossible : fichier inexistant"];
    goto genpage;
  }

  $force = isset($_POST['q-import-dish-force']) && $_POST['q-import-dish-force'] == 1;
  try {
    $warn = [];
    list($stmt, $stmt_vars, $values) = \vgdb\Plat\importFromFilePrepare($dfile, $warn);
  } catch (\vgdb\Exception $e) {
    list($last_code, $last_message) = [1, "Import du fichier de plats impossible: {$e->getMessage()}\n" ];
    goto genpage;
  }

  if(! $stmt_vars) { // 0 lines
    list($last_code, $last_message) = [1, "Import du fichier de plats impossible : rien à importer"];
    goto genpage;
  }

  if(isset($_POST['confirm']) && $_POST['confirm']) {
    $_SESSION['platfiletoopen'] = $dfile;

    // formulaire intermédiaire
    $to_delete = $db->query("SELECT count(id) FROM {$DBPX}plat WHERE id < 1000")->fetch(PDO::FETCH_NUM)[0];
    $recette_affected = $db->query("SELECT count(id) FROM {$DBPX}recette WHERE id_plat < 1000")->fetch(PDO::FETCH_NUM)[0];
    require_once("libs/rain.tpl.class.php");
    $tpl = new raintpl();
    raintpl::$tpl_dir = "templates/";
    $tpl->assign(array(
      // header
      "summary_link" => TRUE,
      "no_main_link" => FALSE,
      "last_message" => isset($last_message) ? $last_message : NULL,
      "last_code" => isset($last_code) ? $last_code : NULL,
      //
      "sessid" => session_id(),
      "deleted" => $to_delete,
      "recette_affected" => $recette_affected,
      "added" => count($stmt_vars),
      "fichier" => \vgdb\sys\path2uri($dfile),
    ));
    $tpl->draw( "confirm-plat-import" );
    exit;
  }

  try {
    $saved_file = makedump();
  } catch(\Ciqual\Exception $e) {
    list($last_code, $last_message) = [1, "Impossible d'effectuer une simili-sauvegarde préliminaire d'import : " . $e->getMessage()];
    goto genpage;
  }


  list($deleted, $added) = \vgdb\Plat\importFromFile($stmt, $stmt_vars, $values);
  list($last_code, $last_message) = [0,  sprintf(
    '%d plats retirés, %d plats insérés. La simili-sauvegarde (export) est <a href="%s">ici</a>', 
    $deleted, $added, $saved_file) ];
}





elseif(isset($_POST['q-import-cpn'])) { // equiv scripts/import_plats.sql
  \vgdb\sys\noAdmin_bailOut();

  session_start();
  $dfile = null;

  if(isset($_POST['sessid']) && $_POST['sessid'] == session_id() && isset($_SESSION['cpnfiletoopen'])  && $_SESSION['cpnfiletoopen']) {
    // confirmation
    $dfile = $_SESSION['cpnfiletoopen'];
    session_destroy();
  }
  elseif(isset($_FILES['data']['tmp_name']) && $_FILES['data']['tmp_name']) {
    $ifile = $_FILES['data']['tmp_name'];
    $dfile = TMPDIR . '/new-cpn-db-' . basename($ifile);
    move_uploaded_file($ifile, $dfile);
  }

  // TODO: remove this dup' check once this is stable
  // keep the one in \vgdb\Plat\importFromFilePrepare()
  if(! file_exists($dfile)) {
    list($last_code, $last_message) = [1, "Import du fichier de nutriments impossible: fichier inexistant"];
    goto genpage;
  }

  try {
    list($stmt, $stmt_vars, $values, $fdcd) = \Ciqual\NutriData\importFromFilePrepare($dfile);
  } catch (\Ciqual\Exception $e) {
    list($last_code, $last_message) = [1, "Import du fichier de nutriments impossible: {$e->getMessage()}\n" ];
    goto genpage;
  }

  if(isset($_POST['confirm']) && $_POST['confirm']) {
    $_SESSION['cpnfiletoopen'] = $dfile;

    $reset = isset($_POST['q-import-cpn-reset']) && $_POST['q-import-cpn-reset'] == 1;

    // formulaire intermédiaire
    if($reset) {
      $to_delete = $db->query("SELECT COUNT(DISTINCT ORIGFDCD) AS c FROM {$DBPX}COMPILED_DATA WHERE ORIGFDCD >= 100000")
                      ->fetch(PDO::FETCH_NUM)[0];
    } else {
      $to_delete = $db->query(sprintf(
        "SELECT COUNT(DISTINCT ORIGFDCD) AS c FROM {$DBPX}COMPILED_DATA WHERE ORIGFDCD IN (%s)",
        implode(',', $fdcd)
      ))->fetch(PDO::FETCH_NUM)[0];
    }

    require_once("libs/rain.tpl.class.php");
    $tpl = new raintpl();
    raintpl::$tpl_dir = "templates/";
    $tpl->assign(array(
      // header
      "summary_link" => TRUE,
      "no_main_link" => FALSE,
      "last_message" => isset($last_message) ? $last_message : NULL,
      "last_code" => isset($last_code) ? $last_code : NULL,
      //
      "sessid" => session_id(),
      "deleted" => $to_delete,
      "new" => $fdcd,
      "reset" => $reset,
      "fichier" => $dfile,
    ));
    $tpl->draw( "confirm-nutriment-import" );
    exit;
  }

  try {
    $saved_file = makedump();
  } catch(\Ciqual\Exception $e) {
    list($last_code, $last_message) = [1, "Impossible d'effectuer une simili-sauvegarde préliminaire d'import : " . $e->getMessage()];
    goto genpage;
  }

  $reset = isset($_POST['q-import-cpn-reset']) && $_POST['q-import-cpn-reset'] == 1;
  if($reset) {
    \vgdb\log\record("Import d'un nouveau fichier de valeurs nutritionnelles avec réinitialisation forcée...");
  }

  list($deleted, $added) = \Ciqual\NutriData\importFromFile($stmt, $stmt_vars, $values, $fdcd, $reset);
  \vgdb\log\record(sprintf("Import de nutriments (%d ingrédients, %d nutriments ajoutés, %d nutriments supprimés)",
                           count($fdcd), $added, $deleted));
  list($last_code, $last_message) = [0,  sprintf(
    '%d ingrédients traités soit %d valeurs nutritionnelles insérées. %d valeurs nutritionnelles supprimées.<br/>La simili-sauvegarde (export) est <a href="%s">%s</a>', 
    count($fdcd), $added, $deleted, \vgdb\sys\path2uri($saved_file), $saved_file) ];
}




elseif(isset($_POST['q-import-food'])) {
  \vgdb\sys\noAdmin_bailOut();

  $file = null;
  if(isset($_FILES['data']['tmp_name']) && $_FILES['data']['tmp_name']) {
    $ifile = $_FILES['data']['tmp_name'];
    $ofile = TMPDIR . '/new-ingr-db-' . basename($ifile);
    if(move_uploaded_file($ifile, $ofile)) {
      $file = $ofile;
    }
  }

  if(! $file) {
    list($last_code, $last_message) = [1, "Import du fichier d'aliments impossible"];
    goto genpage;
  }

  $reset = isset($_POST['q-import-food-reset']) && $_POST['q-import-food-reset'] == 1;
  $force = isset($_POST['q-import-food-force']) && $_POST['q-import-food-force'] == 1;
  if($reset) {
    \vgdb\log\record("Import d'un nouveau fichier d'aliments VG avec réinitialisation forcée...");
  }

  try {
    $warn = [];
    list($stmt, $stmt_vars, $values) = \Ciqual\Food\importFromFilePrepare($file, $warn, $force);
  } catch (\Ciqual\Exception $e) {
    list($last_code, $last_message) = [1, "Erreur à l'import du fichier d'aliments : {$e->getMessage()}\n"];
    goto genpage;
  }

  list($deleted, $added) = \Ciqual\Food\importFromFile($stmt, $stmt_vars, $values, $reset);
  \vgdb\log\record("Import d'un nouveau fichier d'aliments: effectué");
  list($last_code, $last_message) = [0, sprintf("Import d'aliments ($deleted supprimés, $added ajoutés)\n") ];
}

genpage:

$users = \vgdb\users\getAll();
$log = \vgdb\log\getFullLog();

require_once("libs/rain.tpl.class.php");
$tpl = new raintpl();
raintpl::$tpl_dir = "templates/";

$tpl->assign(array(
  // header
  "summary_link" => TRUE,
  "last_message" => isset($last_message) ? $last_message : NULL,
  "last_code" => isset($last_code) ? $last_code : NULL,

  "users" => $users,
  "log" => $log
));

$tpl->draw( "outils" );
