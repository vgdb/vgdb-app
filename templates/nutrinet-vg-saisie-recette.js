var group_list;
var next_focus;
var changed = false;

$(function() {
	$.ajax({url: "data/gen/ciqual.json"}).done(function(data) {
		group_list = data;
		$("#groups").trigger("change");
		updateTableFromForm();
	});

	$("#groups").change(function() {
		if(!group_list) alert("error: liste d'aliments JSON non-chargée");
		$("#ingredients").html('');
		ORIGGPCD = $("#groups option:selected").val();
		var html = '';
		for(i = 0; i < group_list[ORIGGPCD][1].length; i++) {
			html += '<option value="' + group_list[ORIGGPCD][1][i][0] + '">' + group_list[ORIGGPCD][1][i][1] + '</option>';
		}
		// console.log(html);
		$("#ingredients").html(html);
	});

	$(window).on('beforeunload', function(e) { // note: $(window).unload() won't work
		if (changed) {
			alert("Les changements d'ingrédients n'ont pas encore été sauvegardés, une confirmation va vous demander si vous souhaitez véritablement renoncer à ces changements ?");
			return false;
		}
	});

	$("form#ingredients_form_liste").submit(function() {
		$(window).off('beforeunload');
	});
});


function ajoutIngredient() {
	if( ! $("#quantite").val().match(/^[0-9]+([,.][0-9]+)?$/) ) {
		$("#quantite").animateHighlight("red", 3500);
		return;
	}
	// refuse 0 == value
	if( $("#quantite").val() == 0 || $("#quantite").val() == "0,0") {
		$("#quantite").animateHighlight("red", 3500);
		return;
	}

	var ORIGFDCD = $("#ingredients option:selected").val();
	ajoutIngredientForm(ORIGFDCD);
	// ajoutIngredientTable(ORIGFDCD); // précédente méthode de "double-génération"
	updateTableFromForm();
	resetFormSaisie();
	dispSubmitButton();
	if(next_focus && $(next_focus)) $(next_focus).focus();
	next_focus = null;
}

function ajoutIngredientTable(ORIGFDCD) {
	$("#ingredients_table_liste").append(
		'<tr id="tingr-' + ORIGFDCD + '">'
			+ "<td>" + ORIGFDCD + "</td>"
			+ "<td>" + $("#groups option:selected").text() + "</td>"
			+ "<td>" + $("#ingredients option:selected").text() + "</td>"

		+ "<td>"
			+ $("#quantite").val() + ' ' + $("#unite option:selected").text() + "</td>"

		+ "<td>"
			+ '<input type="button" value="Suppr" onClick="supprIngredient(\'' + $("#ingredients").val() + '\')" />'
			+ "</td></tr>"
	);
}

function ajoutIngredientForm(ORIGFDCD) {
	var ivalue = ORIGFDCD + ':' + $("#quantite").val() + ':' + $("#unite option:selected").val();
	if($("#ingredients_form_liste #ingr-" + ORIGFDCD).val()) {
		// met simplement à jour la valeur dans le champ hidden déjà existant
		// pour cet ingrédient
		$("#ingredients_form_liste #ingr-" + ORIGFDCD).val(ivalue);
		return;
	}
	$("#ingredients_form_liste")
		.append('<input type="hidden" class="un-ingredient" name="ingr[]" id="ingr-' + ORIGFDCD + '" value="' + ivalue + '" />');
}

function resetFormSaisie() {
	$("#groups").css("border", "");
	$("#ingredients").css("border", "");
	$("#label-form-select-ingredient").text("et/ou sélectionner l'ingrédient parmi les groupes :");

	$("#groups").val("01.1");
	$("#groups").trigger("change");
	$("#unite").val('g');
	$("#quantite").val('');
	$("#groups").focus();
}

function dispSubmitButton() {
	changed = true;
	$("#valider-recette").show();
}


function supprIngredient(ORIGFDCD) {
	$("#ingredients_form_liste #ingr-" + ORIGFDCD).remove();
	updateTableFromForm();
	dispSubmitButton();
}

function updateTableFromForm() {
	var html = '';
	$("#ingredients_form_liste input[class=un-ingredient]").each(function(i, v) {
		elems = $(v).val().split(":");
		ORIGFDCD = elems[0];
		quantite = elems[1];
		unite = elems[2];

		d = origfdcd_to_nom_group(ORIGFDCD);
		ORIGGPFR = d.group;
		ORIGFDNM = d.nom;


		html += '<tr id="tingr-' + ORIGFDCD + '">'
			+ "<td>" + ORIGFDCD + "</td>"
			+ "<td>" + ORIGGPFR + "</td>"
			+ "<td>" + '<a href="#" onclick="return switchSelectTo(\'' + ORIGFDCD + '\', \'' + unite + '\')">' + ORIGFDNM + "</a></td>"
		
			+ "<td>"
			+ quantite + ' ' + unite + "</td>"
		
			+ "<td>"
			+ '<input type="button" value="Suppr" onClick="supprIngredient(\'' + ORIGFDCD + '\')" />'
			+ "</td></tr>";
	});
	$("#ingredients_table_liste").html(html);
}

function switchSelectTo(ORIGFDCD, unite) {
	setAutoSelectFocuses(origfdcd_to_origgpcd(ORIGFDCD), ORIGFDCD);
	$("#unite").val(unite);
	$("#quantite").focus();
	return false; // preventDefaults() (cf: href="#")
}

function setAutoSelectFocuses(ORIGGPCD, ORIGFDCD) {
	$("#groups").val(ORIGGPCD);
	$("#groups").trigger("change");
	$("#ingredients").val(ORIGFDCD);
}


function origfdcd_to_nom_group(ORIGFDCD) {
	for(i in group_list) {
		for(j in group_list[i][1]) {
			if(group_list[i][1][j][0] == ORIGFDCD) return {nom: group_list[i][1][j][1], group: group_list[i][0]};
		}
	}
	// console.log("warning: origfdcd_to_nom_group, groupe introuvable pour le ORIGFDCD = " + ORIGFDCD);
	return {group: "??????????????", nom: "??????????????"};
}

function origfdcd_to_origgpcd(ORIGFDCD) {
	for(i in group_list) {
		for(j in group_list[i][1]) {
			if(group_list[i][1][j][0] == ORIGFDCD) return i;
		}
	}
	// console.log("warning: origfdcd_to_origgpcd, groupe introuvable pour le ORIGFDCD = " + ORIGFDCD);
	return null;
}
