// http://jqueryui.com/autocomplete/#multiple-remote

// http://stackoverflow.com/questions/275931/how-do-you-make-an-element-flash-in-jquery
$.fn.animateHighlight = function(highlightColor, duration) {
	var highlightBg = highlightColor || "#FFFF9C";
	var animateMs = duration || 1500;
	var originalBg = this.css("backgroundColor");
	this.effect("pulsate", { times:3 });
	this.stop().css("background-color", highlightBg).animate({backgroundColor: originalBg}, animateMs);
};

$(function() {
	$("#inac")
	// don't navigate away from the field on tab when selecting an item
		.bind( "keydown", function( event ) {
			if ( event.keyCode === $.ui.keyCode.TAB &&
			     $( this ).data( "ui-autocomplete" ).menu.active ) {
				     event.preventDefault();
			     }
		})
		.autocomplete({
			source: function( request, response ) {
				$.getJSON( "completion.php", {
					t: "ingr",
					term: request.term
				}, response );
			},
			focus: function() {
				// prevent value inserted on focus
				return false;
			},
			select: function( event, ui ) {
				switchSelectTo(ui.item.id, "g");
				// après la validation, get back here
				next_focus = "#inac";
				$('#inac').val('');
				$("#groups").css("border", "1px solid green");
				$("#ingredients").css("border", "1px solid green");
				$("#quantite").animateHighlight("red", 3500);
				$("#label-form-select-ingredient").text("saisissez le dosage avant de valider :");
				return false; // important pour le reset ci-dessus
			}
		});
});
