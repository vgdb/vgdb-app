<?php if(!class_exists('raintpl')){exit;}?><!doctype html>
<html>
  <head>
    <link href="templates/misc.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="templates/jquery-2.0.3.min.js"></script>
    <title>Accueil. <?php echo $nb_plats;?> plats, <?php echo $nb_recettes;?> recettes</title>
  </head>

  <body>
    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>


    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("sommaire-outils") . ( substr("sommaire-outils",-1,1) != "/" ? "/" : "" ) . basename("sommaire-outils") );?>


    <h1> Liste des plats </h1>
    <a style="font-size: x-small" 
       href=""
       onclick="$('#index-div-filter-form').toggle(); $('#index-div-aff-info').hide(); return false;">Filtrer les plats</a>
    <a style="font-size: x-small" 
       href=""
       onclick="$('#index-div-aff-info').toggle(); $('#index-div-filter-form').hide(); return false;">Info sur les filtres</a>
    <a style="font-size: x-small" 
       href="index.php?dump=1"><i>Dump</i>er (obtenir un fichier de sauvegarde réimportable)</a>


    <div id="index-div-filter-form" <?php if( ! @empty($_GET) ){ ?>style="display: block;"<?php } ?>>
      <form method="GET" id="form-filter-plats">
	<ul>
	  <li>
	    <label for="filter-text">Ne sélectionner que les plats dont une recette contient un ingrédient contenant cette chaîne :</label>
	    <input type="text" name="f" id="filter-text" size="10" value="<?php echo @htmlentities( $_GET["f"] );?>" />
	  </li>
	  <li>
	    <label for="filter-prio">Ne sélectionner que les plats prioritaires</label>
	    <input type="checkbox" name="prio" id="filter-prio" value="1" <?php if( empty(@$_GET) || @$_GET["prio"] ){ ?>checked="checked"<?php } ?> />
	  </li>
	  <li>
	    <label for="filter-new">Ne sélectionner que les plats saisis à l'aide de l'interface</label>
	    <input type="checkbox" name="new" id="filter-new" value="1" <?php if( @$_GET["new"] ){ ?>checked="checked"<?php } ?> />
	  </li>
	  <li>
	    <label for="filter-old">Ne sélectionner que les plats standards (liste Nutrinet présélectionnée)</label>
	    <input type="checkbox" name="old" id="filter-old" value="1" <?php if( @$_GET["old"] ){ ?>checked="checked"<?php } ?> />
	  </li>
	  <li>
	    <label for="filter-nonnull">Ne sélectionner que les plats comprenant au moins 1 recette</label>
	    <input type="checkbox" name="nonnull" id="filter-nonnull" value="1" <?php if( @$_GET["nonnull"] ){ ?>checked="checked"<?php } ?> />
	  </li>
	  <li>
	    <label for="filter-null">Ne sélectionner que les plats sans recette saisie recette</label>
	    <input type="checkbox" name="null" id="filter-nonnull" value="1" <?php if( @$_GET["null"] ){ ?>checked="checked"<?php } ?> />
	  </li>
	  </ul>
	<input type="submit" value="Filtrer" />
      </form>
    </div>
      
    <div id="index-div-aff-info">
      Une valeur numérique sous la forme d'un masque de bit peut être passée au paramètre d'URL nommé "f".<br/>
      Ce masque contient les valeurs 1, 2, 4 et 8 (et toutes les additions entre eux) (soit jusqu'à 15)). 
      <ul>
	<li>1 (GET_NEW) pour les plats saisis via l'interface. <a href="?f=1">exemple</a></li>
	<li>2 (GET_OLD) pour les plats standards (import CSV). <a href="?f=2">exemple</a></li>
	<li>4 (GET_NONNULL) pour les plats comprenant au moins 1 recette. <a href="?f=4">exemple</a></li>
	<li>8 (GET_NULL) pour les plats sans recettes. <a href="?f=8">exemple</a></li>
	<li>En conséquence et à titre d'exemple 1+2+4+8 = 15 pour <a href="?f=15">tous</a>, 2+4 = 6 pour <a href="?f=6">origine CSV ayant des recettes</a>, ...</li>
      </ul>
      <hr/>
      Une chaîne de caractère peut être passée au paramètre d'URL nommé "F".<br/>
      Les seuls plats retournés seront ceux ayant au moins un ingrédient dont l'appellation contient cette chaîne de caractères.
      Si "F" et "f" sont utilisés simultanément le résultat est l'intersection des deux critères (les plats correspondants aux <strong>deux</strong> critères).
      Si "F" est spécifié mais f=8 (ou f=9 ou f=10), <em>c'est à dire les plats sans recettes</em>, évidemment aucun plat ne correspondra aux critères.
      <ul>
	<li>Les plats dont au moins une recette contient de la biscotte : <a href="?F=biscotte">exemple</a></li>
	<li>Les plats dont au moins une recette contient du tofu : <a href="?F=tofu">exemple</a></li>
	<li>Les plats dont au moins une recette contient du tofu et faisant partie des plats initiaux (CSV) : <a href="?f=2&F=tofu">exemple</a></li>
      </ul>
      <hr/>
    </div>

    <ul id="accueil-alpha-index">
	<?php $counter1=-1; if( isset($alpha) && is_array($alpha) && sizeof($alpha) ) foreach( $alpha as $key1 => $value1 ){ $counter1++; ?>

	<li><a href="#accueil-plats-<?php echo $key1;?>"><?php echo $key1;?></a>&nbsp;</li>
	<?php } ?>

    </ul>

    <div id="plats">
      <?php $counter1=-1; if( isset($alpha) && is_array($alpha) && sizeof($alpha) ) foreach( $alpha as $key1 => $value1 ){ $counter1++; ?>

      <p id="accueil-plats-<?php echo $key1;?>"><?php echo $key1;?></p>
      <ul>
	<?php $counter2=-1; if( isset($value1) && is_array($value1) && sizeof($value1) ) foreach( $value1 as $key2 => $value2 ){ $counter2++; ?>

	<li class="prio-<?php echo $value2["prioritaire"];?>"><a href="recettes.php?plat_id=<?php echo $value2["id"];?>"><?php echo $value2["nom_vegetalise"];?></a>
	<?php if( $value2["nom_traditionnel"] ){ ?>(équiv. <em><?php echo $value2["nom_traditionnel"];?></em>)<?php } ?> [<?php echo $value2["c"];?> recettes] <?php if( $value2["id"] >= 1000 ){ ?>(<span class="plat-from-ui">new!</span>)<?php } ?>

	</li>
	<?php } ?>

      </ul>
      <?php }else{ ?>

      <em>Aucun plat n'existe ou ne correspond aux critères spécifiés.</em>
      <?php } ?>

    </div>

    <!--
    <ul>
      <?php $counter1=-1; if( isset($liste_des_plats) && is_array($liste_des_plats) && sizeof($liste_des_plats) ) foreach( $liste_des_plats as $key1 => $value1 ){ $counter1++; ?>

      <li>
	<a href="recettes.php?plat_id=<?php echo $value1["id"];?>"><?php echo $value1["nom_vegetalise"];?></a>
	<?php if( $value1["nom_traditionnel"] ){ ?>(équiv. <em><?php echo $value1["nom_traditionnel"];?></em>)<?php } ?> [<?php echo $value1["c"];?> recettes]
      </li>
      <?php } ?>

    </ul>
-->

    <hr/>

    <h2 id="index-add-plat">Ajouter un nouveau plat</h2>
    <?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("form-plat") . ( substr("form-plat",-1,1) != "/" ? "/" : "" ) . basename("form-plat") );?>


    <p id="index-notice"> Si vous constatez qu'un intitulé de plat
      est trop vague et devrait être divisé en deux plats différents, merci d'en
      informer <a href="mailto:nutrinetvege@gmail.com">nutrinetvege@gmail.com</a> en indiquant
      "Liste de plats" dans le sujet du mail. </p>

    <hr/>
