// https://github.com/danielm/uploader/

function message(success, msg) {
	return '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Erreur</strong><br>'
		+msg+"<br>"+msg.msg+'</div>';
}

function showpreview(recette_id) {
	imgs = $.ajax({
		dataType: 'json',
		data: {
			recette_id: recette_id,
			getfiles: 1
		      }
	}).done(function(data) {
		txt = '<ul>';
		for(i in data) {
			txt += '<li><a onClick="$.post(\'\', { recette_id: ' + recette_id + ', delete_img: \'' + data[i] + '\' })' +
				'.always(function () { showpreview(' + recette_id + '); });">' +

				'<img border="0" style="height: 24px;" title="Supprimer" alt="Supprimer" src="templates/images/delete.png"></a>' +
				' <a href="' + data[i] + '"><img src="' + data[i] + '" /> ' +  data[i] + '</a></li>';
		}
		$('td.images-preview').html(txt);
	});


}

// quickly (but heavily) inspired from lut.im config
function bindddz(recette_id) {
	$('#drag-and-drop-zone').dmUploader({
		url: '',
		dataType: 'json',
		allowedTypes: 'image/*',
		maxFileSize: 1024 * 1024, // 1 MB
		extraData: {
			recette_id: recette_id
		},
		onNewFile: function(id, file){
			$(".messages").append('<div id="'+id+'-div">'+file.name+'<br><div class="progress"><div id="'+id+'"class="progress-bar progress-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span id="'+id+'-text" class="pull-left" style="padding-left: 10px;"> 0%</span></div></div></div>');
		},
		onUploadProgress: function(id, percent){
			var percentStr = ' '+percent+'%';
			$('#'+id).prop('aria-valuenow', percent);
			$('#'+id).prop('style', 'width: '+percent+'%;');
			$('#'+id+'-text').html(percentStr);

		},
		onUploadSuccess: function(id, data){
			$(".messages").html('');
			showpreview(recette_id);
		},
		onUploadError: function(id, message){
			// $(".messages").append(message(false, ''));
			$(".messages").html('');
			showpreview(recette_id);
		},
		onFileSizeError: function(file){
			// $(".messages").append(message(false, { filename: file.name, msg: 'Le fichier dépasse la limite de taille (1 Mo)'}));
			$(".messages").html('');
			showpreview(recette_id);
		}
	});
}
