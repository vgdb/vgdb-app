DROP TABLE IF EXISTS `plat`;
CREATE TABLE `plat` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID du plat VG',
  `nom_traditionnel` varchar(150) NULL DEFAULT NULL COMMENT 'Nom du plat traditionnel',
  `nom_vegetalise` varchar(150) NULL DEFAULT NULL COMMENT 'Nom du plat végétalisé',
  `prioritaire` bool NULL DEFAULT 0 COMMENT 'Prioritaire',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `recette`;
CREATE TABLE `recette` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID de recette',
  `id_plat` smallint(6) unsigned NOT NULL DEFAULT 0 COMMENT 'référence l''ID du plat VG correspondant',
  `nom` varchar(150) NOT NULL DEFAULT '' COMMENT 'Nom de la recette',
  `metadata` TEXT NULL DEFAULT NULL COMMENT 'méta-données [normalisées ci-possible] pour la recette',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `composition`;
CREATE TABLE `composition` (
  `id_recette` smallint(6) unsigned NOT NULL COMMENT 'référence l''ID de recette',
  `id_plat` smallint(6) unsigned NOT NULL DEFAULT 0 COMMENT 'référence l''ID du plat VG correspondant',
  `ORIGFDCD` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'Code d''ingrédient',
  `quantite` varchar(150) NOT NULL DEFAULT '' COMMENT 'Quantité',
  `unite` varchar(150) NOT NULL DEFAULT '' COMMENT 'Unité (g, L, cup, ...)',
  PRIMARY KEY (`id_recette`, `id_plat`, `ORIGFDCD`)
) DEFAULT CHARSET=utf8;
