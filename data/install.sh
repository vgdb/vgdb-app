#!/bin/bash

MDB="Ciqual 2013.accdb"
DB=nut
PREFIX=vgdb_


if [[ ! -f $MDB ]]; then
    [[ ! -f CIQUAL2013-Donneesmdb.zip ]] && wget http://www.ansespro.fr/TableCIQUAL/Documents/CIQUAL2013-Donneesmdb.zip
    unzip  CIQUAL2013-Donneesmdb.zip
fi

[[ ! -f $MDB ]] && echo "no $MDB file" >&2 && exit 1

mysql<<<"CREATE DATABASE $DB CHARACTER SET utf8 COLLATE utf8_general_ci"

mdb-tables -1 ../../misc/"Ciqual 2013.accdb" | \
    sed -nr '/^(FOOD|FOOD_GROUPS|COMPONENT|COMPILED_DATA)/p' | \
    xargs -n1 -I "{}" mdb-schema -T "{}" "../../misc/Ciqual 2013.accdb" mysql | \
    sed -r "/CREATE TABLE/s/\`(.*)\`/\`${PREFIX}\1\`/"|mysql $DB

mdb-tables -1 "Ciqual 2013.accdb"|sed -e '/^~/d' -e '/REFERENCE/d'|xargs -n1 mdb-export -H -I mysql "Ciqual 2013.accdb"|sed -e "s/^INSERT INTO \`/INSERT INTO \`${PREFIX}/"|mysql $DB
sed -re "s/\`FOOD_GROUPS\`/${PREFIX}\`FOOD_GROUPS\`/" < sql/fix-ciqual-v2013.sql|mysql $DB



sed -re "/^[A-Z]+ TABLE/s/\`(.*)\`/${PREFIX}\1/" < sql/vgdb-struct.sql|mysql $DB


mysql $DB <<<"ALTER TABLE ${PREFIX}FOOD ADD PRIMARY KEY (\`ORIGFDCD\`)"
mysql $DB <<<"ALTER TABLE ${PREFIX}FOOD_GROUPS ADD PRIMARY KEY (\`ORIGGPCD\`)"
mysql $DB <<<"ALTER TABLE ${PREFIX}COMPONENT ADD PRIMARY KEY (\`ORIGCPCD\`)"

mysql $DB <<<"CREATE INDEX ORIGFDCD ON ${PREFIX}COMPILED_DATA (ORIGFDCD);"

sed -r "s/(fulltxt|FOOD|FOOD_GROUPS)/${PREFIX}\1/g" < sql/completion_gen_table.sql|mysql $DB
