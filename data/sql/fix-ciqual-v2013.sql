-- Ciqual v2013 miss many level-0 group names
-- CREATE TABLE FOOD_GROUPS LIKE FOOD_GROUPS_v2013;
-- INSERT INTO FOOD_GROUPS SELECT * FROM FOOD_GROUPS_v2012 WHERE ORIGGPCD NOT LIKE '%.%' AND ORIGGPCD NOT IN ( SELECT ORIGGPCD FROM FOOD_GROUPS_v2013 );
INSERT INTO `FOOD_GROUPS` VALUES ('0','non définie','undefined');
INSERT INTO `FOOD_GROUPS` VALUES ('01','Céréales et pâtes','Cereals and pasta');
INSERT INTO `FOOD_GROUPS` VALUES ('02','Boulangerie-viennoiserie','Breads and rolls');
INSERT INTO `FOOD_GROUPS` VALUES ('03','Pâtisseries et biscuits','Pastry and biscuits');
INSERT INTO `FOOD_GROUPS` VALUES ('05','Produits laitiers et entremets','Milk and milk products');
INSERT INTO `FOOD_GROUPS` VALUES ('06','Fromages','Cheeses');
INSERT INTO `FOOD_GROUPS` VALUES ('28','Assaisonnements et sauces','Seasonings and sauces');
INSERT INTO `FOOD_GROUPS` VALUES ('27','Soupes et bouillons','Soups and stocks');
INSERT INTO `FOOD_GROUPS` VALUES ('08','Matières grasses','Fats and oils');
INSERT INTO `FOOD_GROUPS` VALUES ('13','Poissons et batraciens','Fish and batrachians');
INSERT INTO `FOOD_GROUPS` VALUES ('14','Crustacés et mollusques','Shellfish and mollusks');
INSERT INTO `FOOD_GROUPS` VALUES ('18','Fruits','Fruits');
INSERT INTO `FOOD_GROUPS` VALUES ('21','Sucres et confiseries','Sugars and confectionery');
INSERT INTO `FOOD_GROUPS` VALUES ('22','Boissons sans alcool','Non-alcoholic beverages');
INSERT INTO `FOOD_GROUPS` VALUES ('23','Boissons alcoolisées','Alcoholic beverages');
INSERT INTO `FOOD_GROUPS` VALUES ('30','Ingrédients divers','Other ingredients');
