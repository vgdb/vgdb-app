<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

require_once("libs/libCiqual.php");
require_once("libs/libvgdb.php");
require_once("libs/libvgdb-sys.php");
use vgdb\Plat as Plat;

if(!isset($_REQUEST['plat_id'])) {
  header('Location: index.php');
  exit;
}

if(isset($_GET['add'])) {
  if(($id = \vgdb\Recette\add($_GET['plat_id'], $_GET['nom']))) {
    header("Location: ingredients.php?recette_id=$id");
    exit;
  }
  list($last_code, $last_message) = [1, "L'ajout de recette à échoué"];
}
elseif(isset($_GET['del'])) {
  list($last_code, $last_message) = \vgdb\Recette\del($_GET['recette_id']);
  // obsolète:
  /* le chargement se poursuit vers la page d'origine (celle de la liste des
     recettes correspondant au plat dont on vient de supprimer une recette
     // header("Location: recettes.php?plat_id=" . $r['id_plat']); */
}
elseif(isset($_POST['mod-plat'])) {
  if(! UI_PLATS) die('feature disabled for our current workflow !');
  list($last_code, $last_message) =
    \vgdb\Plat\alter(isset($_POST['plat_id']) ? $_POST['plat_id'] : NULL,
                     isset($_POST['nom_vegetalise']) ? $_POST['nom_vegetalise'] : NULL,
                     isset($_POST['nom_nutrinet']) ? $_POST['nom_nutrinet'] : NULL,
                     isset($_POST['prioritaire']) ? $_POST['prioritaire'] : NULL);
}

$plat_id = intval($_GET['plat_id']);
$plat = Plat\get($plat_id);
$recettes = \vgdb\Recette\getFromPlatIDIndexed($plat_id);

// statistics
$stats = [];
use vgdb\CiqualTools AS stat;

if(isset($_GET['stats']) && $_GET['stats'] == 1 && count($recettes)) {
  require_once("libs/libvgdb-ciqual.php");

  $can_mean = stat\PlatGetStatistics(
    $stats,
    $plat_id,
    VGDB_ALL_KEEP, /* individual ingrédients approximation handling during DB retrieval */
    VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_ROUND|VGDB_TRACES_KEEP /* recette */ );


  if($can_mean) {
    stat\_PlatComputeStatistics($stats['total'],
                                $stats['meta'],
                                $stats['recettes_totaux'],
                                VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_ROUND|VGDB_TRACES_KEEP,
                                FALSE /* no VALMIN, VALMAX */);
  } else {
    $stats['total'] = current($stats['recettes_totaux']);
    array_walk($stats['total'], function (&$n) { $n['somme'] = NULL; });
  }

  $stats["header"] = stat\generateNutriHeader($stats["total"]);
  stat\FormatNutValBis($stats["total"], NULL);
  define('FINAL_TIME',	time() - $_SERVER['REQUEST_TIME']);
}


// dump
if(isset($_GET['dump'])) {
  $t = [];
  foreach($recettes as $r) {
    $ingr = \vgdb\Recette\getIngredients($r['id'], 0x01);
    $t[] = ['recette' => $r,
            'ingredients' => $ingr ];
  }
  if($stats) $t['stats'] = $stats;
  header("Content-Type: text/plain");
  echo json_encode($t);
  die;
}


require_once("libs/rain.tpl.class.php");
$tpl = new raintpl();
raintpl::$tpl_dir = "templates/";

$tpl->assign(array(
  // header
  "summary_link" => TRUE,

  // pour la déconnexion
  "last_message" => isset($last_message) ? $last_message : NULL,
  "last_code" => isset($last_code) ? $last_code : NULL,

  //
  "plat" => $plat,
  "nom_traditionnels" => explode(" ; ", $plat['nom_traditionnel']),
  "recettes" => $recettes,

  // stats-plat
  "stats" => $stats,

));

$tpl->draw( "recettes" );
