<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

require_once("libs/libvgdb.php");
require_once("libs/libvgdb-sys.php");
require_once("libs/libCiqual.php");

require_once("libs/vgdb-export.php");
require_once("libs/vgdb-export.class.php");

use vgdb\export as vexp;

if(PHP_SAPI === 'cli') {
  if($argc > 1) parse_str($argv[1], $_REQUEST);
  // ini_set("variables_order", "EGPCS");
}

$fmtopts = VgdbExport::$fmtopts;
parseFmtOpt(@$_REQUEST['fmtopt'], $fmtopts);

// format
$fmt = NULL;
if(isset($_REQUEST['fmt'])) {
  if($_REQUEST['fmt'] == 'csv') $fmt = vexp\FMT_CSV;
  elseif($_REQUEST['fmt'] == 'table') $fmt = vexp\FMT_TABLE;
  // otherwise we'll decide according to the "out" param
}

// output
$out = NULL;
if(isset($_REQUEST['out'])) {
  if($_REQUEST['out'] == 'dl') $out = vexp\OUT_DOWNLOAD;
  elseif($_REQUEST['out'] == 'save') $out = vexp\OUT_SAVE;
}

// default format according to the requested output 
if(! $fmt) {
  if($out) $fmt = vexp\FMT_CSV;
  else $fmt = vexp\FMT_TABLE;
}

// aggegation
$agr = NULL;
if(isset($_REQUEST['agr'])) {
  if($_REQUEST['agr'] == 'plat') $agr = vexp\AGR_LVL_PLAT;
  elseif(@$_REQUEST['agr'] == 'recette') $agr = vexp\AGR_LVL_RECETTE;
}

// filters
$plats_restrict = $recettes_restrict = [];
$prio_only = NULL;
if(@$_REQUEST['pids'])
  $plats_restrict = array_filter(explode(',', $_REQUEST['pids']), 'is_numeric');
if(@$_REQUEST['ids'])
  $recettes_restrict = array_filter(explode(',', $_REQUEST['ids']), 'is_numeric');
if(@$_REQUEST['prio'] == '1')
  $prio_only = 1;



// helper class
$export = new VgdbExport();
$export->setFormat($fmt, $out, $fmtopts);


// options compatibility checking
if($plats_restrict || $recettes_restrict) $prio_only = 0;
if(! VgdbExport::opt('valnut') && (VgdbExport::opt('rangenut') || $agr == vexp\AGR_LVL_RECETTE)) {
  die('erreur: -valnut est incompatible avec +rangenut et agr=recette');
}

if($agr == vexp\AGR_LVL_PLAT && $recettes_restrict) { die('agr=plat & ids definition is non-sensical'); }


$entete_line = VgdbExport::genHeader($agr);

// this should happen before setOutputFile()
$export->setFilter(['prio' => $prio_only == 1,
                    'filter-plat' => $plats_restrict,
                    'filter-recette' => $recettes_restrict]);


if($out == vexp\OUT_SAVE) {
  $export->setLocal();
  $export->setOutputFile(__DIR__ . '/dumps/' . $export->getFileName());
}


$export->begin($entete_line);



require_once("libs/libvgdb-ciqual.php");
// HERE starts nested loops, the core of the process

// initialization
$plats_selected_for_export = $export->getPlats();

foreach($plats_selected_for_export as $p) {
  if($agr == vexp\AGR_LVL_RECETTE) {
    vexp\_export_recettes_means(\vgdb\Recette\getFromPlatID($p['id'], TRUE),
                                $p['nom_vegetalise'],
                                $p['nom_traditionnel'],
                                $export);
  }

  elseif($agr == vexp\AGR_LVL_PLAT) {
    vexp\_export_plats_means($p['id'],
                             $p['nom_vegetalise'],
                             $p['nom_traditionnel'],
                             $export);
  }

  elseif(FALSE) { // is_null($agr)
    vexp\_export_recettes($p['id'], $p['nom_vegetalise'], $p['nom_traditionnel'], $export);
  }
  else {
    if(! VgdbExport::opt('valnut')) {
      vexp\_export_recettes($p['id'], $p['nom_vegetalise'], $p['nom_traditionnel'], $export);
      continue;
    }

    $recettes_selected_for_export = \vgdb\Recette\getFromPlatID($p['id']);
    foreach($recettes_selected_for_export as $r) {
      if($export->filters['filter-recette'] &&
         ! in_array($r['id'], $export->filters['filter-recette'])) continue;

      vexp\_export_recette2($p['id'], $p['nom_vegetalise'], $p['nom_traditionnel'], $export, $r);
      $export->changeBGColor();
    }
  }
}

$export->end();
die();



function parseFmtOpt($input, &$variable) {
  if(! $input) return;

  foreach(explode(',', $input, 20) as $v) {
    if($v == 'strict') $variable['strict'] = TRUE;
    if($v == '-strict') $variable['strict'] = FALSE;

    if($v == 'valnut') $variable['valnut'] = TRUE;
    if($v == '-valnut') $variable['valnut'] = $variable['rangenut'] = FALSE;

    if($v == 'rangenut') $variable['rangenut'] = $variable['valnut'] = TRUE;
    if($v == '-rangenut') $variable['rangenut'] = FALSE;
  }
}
