<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

require_once("connect.php");
require_once("libs/libCiqual.php");
require_once("libs/libvgdb.php");
require_once("libs/libvgdb-sys.php");

if(isset($_POST['add-plat'])) {
  if(! UI_PLATS) die('feature disabled for our current workflow !');
  \vgdb\sys\noAdmin_bailOut();
  list($last_code, $last_message) = \vgdb\Plat\add(@$_POST['nom_vegetalise'],
                                                   @$_POST['nom_nutrinet'],
                                                   @$_POST['prioritaire']);
}

/* string-based filter:
   Select plat_id whose recipes contains ingredient whose name (ORIGFDNM) match % $_GET['F'] %.
   Those are passed to \vgdb\Plat\getAll() below */
$str_filter = [];
if(@$_GET['f']) {
    global $db, $DBPX;
    $stmt = $db->prepare("SELECT DISTINCT c.id_plat FROM {$DBPX}FOOD F INNER JOIN {$DBPX}composition c ON (c.ORIGFDCD = F.ORIGFDCD) WHERE ORIGFDNM LIKE ?");
    $stmt->execute(["%" . $_GET['f'] . "%"]);
    $ids = array_map(function($n) { return $n['id_plat']; }, $stmt->fetchAll(PDO::FETCH_ASSOC));
    $str_filter = $ids ? [ "r.id_plat IN (" . implode(',', $ids) . ")" ] : [ 0 ];
}

$filters_params = array_intersect_key($_GET, array_flip(['prio','new','old','null','nonnull']));
$liste_plats = \vgdb\Plat\getAllFiltered($filters_params, $str_filter);
$alpha = array();
foreach($liste_plats as $p) {
  $alpha[strtoupper(substr($p['nom_vegetalise'], 0, 1))][] = $p;
}
ksort($alpha);
$nb_plats = count($liste_plats);
$nb_recettes = array_sum(array_map(function($n) { return $n['c']; }, $liste_plats));

require_once("libs/rain.tpl.class.php");
$tpl = new raintpl();
raintpl::$tpl_dir = "templates/";

$tpl->assign(array(
  // header
  "summary_link" => TRUE,
  "no_main_link" => TRUE,
  "last_message" => isset($last_message) ? $last_message : NULL,
  "last_code" => isset($last_code) ? $last_code : NULL,

  // title
  "nb_plats" => $nb_plats,
  "nb_recettes" => $nb_recettes,

  // "liste_des_plats" => $liste_plats,
  "alpha" => $alpha
));

$tpl->draw( "accueil" );
