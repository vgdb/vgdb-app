<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

require_once("connect.php");
require_once("libs/libvgdb.php");
require_once("libs/libvgdb-sys.php");
require_once("libs/libvgdb-massvol.php");

$sql = "SELECT c.id_recette, c.ORIGFDCD, F.ORIGFDNM, r.nom, m.masvol"
  . " FROM {$DBPX}composition c"
  . " INNER JOIN {$DBPX}FOOD F ON (c.ORIGFDCD = F.ORIGFDCD)"
  . " INNER JOIN {$DBPX}recette r ON (c.id_recette = r.id)"
  . " LEFT JOIN {$DBPX}masvol m ON (c.ORIGFDCD = m.ORIGFDCD)"
  . " WHERE unite != 'g' GROUP BY ORIGFDCD";

load:
$ingr_volume = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

if(isset($_POST['go'])) {
  $map = array_map(function($n) { return $n['ORIGFDCD']; }, $ingr_volume);

  $s = $db->prepare("INSERT INTO {$DBPX}masvol"
		    . " (ORIGFDCD, masvol) VALUES (?, ?)"
		    . " ON DUPLICATE KEY UPDATE masvol = VALUES(masvol)");

  $s_del = $db->prepare("DELETE FROM {$DBPX}masvol WHERE ORIGFDCD = ?");

  $updated = $deleted = 0;
  foreach($map as $ORIGFDCD) {
    if(!isset($_POST[$ORIGFDCD])) continue;
    $masvol = str_replace(',', '.', trim($_POST[$ORIGFDCD]));
    if(empty($masvol)) { // DELETE
      $s_del->execute([$ORIGFDCD]);
      if($s_del->rowCount() > 0) $deleted++;
      continue;
    }

    if(! is_numeric($masvol)) continue;
    else { // UPDATE
      $s->execute([$ORIGFDCD, $masvol]);
      if($s->rowCount() > 0) $updated++; // ON DUP KEY, rowCount() ret 2
    }
  }

  if($updated + $deleted > 0) \vgdb\log\record("Mise à jour de " . ($updated + $deleted) . " masse(s) volumique(s)");

  $last_code = 0;
  $last_message = sprintf("masses volumiques mises à jour : %d, ".
			  "masses volumiques supprimées   : %d",
			  $updated, $deleted);

  // reload new values... hihihi
  unset($_POST['go'], $_GET['export']);
  goto load;
}
elseif(isset($_GET['export'])) {
  $ret = \vgdb\massvol\export($O_params['export.outfile'] && !(@$_GET['dl'] == '1'), $O_params['export.masvol']);
  if(!$ret) goto form;
  else list($last_code, $last_message) = $ret;
}
elseif(isset($_GET['purge'])) {
  list($last_code, $last_message) = \vgdb\massvol\purge($ingr_volume);
}
  

form:

require_once("libs/rain.tpl.class.php");
$tpl = new raintpl();
raintpl::$tpl_dir = "templates/";

$tpl->assign(array(
  // header
  "summary_link" => TRUE,
  "last_message" => isset($last_message) ? $last_message : NULL,
  "last_code" => isset($last_code) ? $last_code : NULL,

  "ingr_volume" => $ingr_volume
));

// includes "form-ajout-ingredient" template
// ... which itself includes "group-select" template
$tpl->draw( "masvol" );
