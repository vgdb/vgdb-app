<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

namespace vgdb {
  class Exception extends \Exception {}
}

namespace vgdb\Plat {
  use PDO;
  require_once(__DIR__ . '/../connect.php');
  require_once(__DIR__ . '/libvgdb-sys.php');

  function getAll() {
    global $db, $DBPX;
    return $db->query("SELECT p.*, COUNT(r.id) AS c FROM {$DBPX}plat p LEFT JOIN {$DBPX}recette r ON p.id = r.id_plat GROUP BY p.id")->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Return all the plats with optional filters applied.
   *
   * @param $opt: in: mask of options to differenciate between built-in plats
   *                  and "new" (UI-added) plats (id >= 1000 are plats inserted through the UI)
   * @param $owhere: additional AND-imploded conditions (used by index to build
   *                 ingredients-string-based filters relying on the plat_id to select.
   *
   * @return an array of plats and the amount of recipes for each of them
   **/
  function getAllFiltered($opt, $owhere = []) {
    global $db, $DBPX;
    $request_tpl = 'SELECT p.*, COUNT(r.id) AS c FROM %splat p LEFT JOIN %srecette r ON p.id = r.id_plat WHERE (%s) AND (%s) GROUP BY p.id %s';
    $where = $having = [];
    if(! array_diff(array_keys($opt), ['prio','new','old','null','nonnull'])) {
      // seulement si "pas toutes" les cases sont cochées (autrement les filtres n'ont aucun sens
      if(isset($opt['prio'])) $where[] = 'p.prioritaire = 1';
      if(isset($opt['new'])) $where[] = 'p.id >= 1000';
      if(isset($opt['old'])) $where[] = 'p.id < 1000';
      if(isset($opt['nonnull'])) $having[] = 'c > 0';
      if(isset($opt['null'])) $having[] = 'c = 0';
    }
    if((isset($opt['nonnull']) && isset($opt['null'])) ||
       (isset($opt['new']) && isset($opt['old']))) {
      $logic = " OR ";
    } else {
      $logic = " AND ";
    }

    $request_str = sprintf($request_tpl, $DBPX, $DBPX,
                           $where ? implode($logic, $where) : TRUE,
                           $owhere ? implode(' AND ', $owhere) : TRUE,
                           $having ? " HAVING " . implode(' OR ', $having) : '');
    // echo $request_str;
    return $db->query($request_str)->fetchAll(PDO::FETCH_ASSOC);
  }

  function get($plat_id) {
    global $db, $DBPX;
    return $db->query(sprintf("SELECT * FROM {$DBPX}plat WHERE id = %d",
                              intval($plat_id)))->fetch(PDO::FETCH_ASSOC);
  }

  function _check_liste_noms_tradi($data) {
    if(is_string($data)) return true;
    if(count($data) > 1 && in_array('', $data)) return false;
    if(count($data) > 50) return false;
    return true;
  }

  function _add_alter_prechecks($vgl, $tradi, $prio) {
    if(! $vgl) throw new \Exception("Pas de nom vegetalisé saisi");
    if(! _check_liste_noms_tradi($tradi)) {
      throw new \Exception("Aucune information de plat traditionnel correcte spécifiée");
    }

    // gestion nom tradi/nutrinet (array ou string)
    if(! is_string($tradi)) $nom_tradi = implode(' ; ', $tradi);
    else $nom_tradi = $tradi;
    $nom_tradi = trim($nom_tradi);
    if(!$nom_tradi || $nom_tradi == 'aucun') $nom_tradi = NULL;

    $prioritaire = $prio && $prio == 1;
    return [$vgl, $nom_tradi, $prioritaire];

  }

  function add($vgl, $tradi, $prio) {
    global $db, $DBPX;
    try {
      list($nom_vgl, $nom_tradi, $prioritaire) = _add_alter_prechecks($vgl, $tradi, $prio);
    } catch(\Exception $e) { return [1, $e->getMessage()]; }

    // les nouveaux plats rajouté "intéractivement" à travers l'interface plutôt qu'avec une liste initiale
    // prédéfinie se voient attribué un IDs > 1000. Ceci afin de permettre des mises à jour (des insertions) de données
    // issues de CSV "initiaux"
    $next_id = max(1000,
                   1 + $db->query("SELECT MAX(id) FROM {$DBPX}plat")->fetch(PDO::FETCH_NUM)[0]);

    if(! ($x = $db->prepare("INSERT INTO {$DBPX}plat (id, nom_traditionnel, nom_vegetalise, prioritaire) VALUES (?, ?, ?, ?)")
                  ->execute(array($next_id, $nom_tradi, $nom_vgl, $prioritaire))) ) {
      die('error');
    }

    \vgdb\log\record("Ajout du plat \"$nom_vgl\" #$next_id");
    // TODO: pas de redirect ?
    header("Location: recettes.php?plat_id=$next_id");
    exit;
  }

  function alter($plat_id, $vgl, $tradi, $prio) {
    global $db, $DBPX;
    if(! $plat_id) return [1, "Erreur"];
    try {
      list($nom_vgl, $nom_tradi, $prioritaire) = _add_alter_prechecks($vgl, $tradi, $prio);
    } catch(\Exception $e) { return [1, $e->getMessage()]; }

    if(! ($plat = get($plat_id)) )return [1, "Erreur (aucun plat correspondant)"];


    $stmt = $db->prepare("UPDATE {$DBPX}plat SET nom_traditionnel = ?, nom_vegetalise = ?, prioritaire = ? WHERE id = ?");
    if(! ($x = $stmt->execute(array($nom_tradi, $nom_vgl, $prioritaire, $plat['id']))) ) {
      var_dump($x, $stmt, $_POST, $db->errorInfo());die;
    }

    if($stmt->rowCount() == 1) {
      \vgdb\log\record("Modification du plat \"$nom_vgl\" #{$plat['id']}");
      return [0, "Modification effectuée"];
    }
    // else: pas de mise à jour des lignes
  }

  function importFromFilePrepare($dfile, &$warn, $force = FALSE) {
    global $DBPX;

    $x = fopen($dfile, 'r');
    if(! $x) throw new \vgdb\Exception("can't open file");

    $header = fgetcsv($x, 1024 * 4, "\t");

    // check all columns
    if(count($header) < 4) throw new \vgdb\Exception("Less than 4 columns or invalid format/separator");
    $off_header = ["ID_PLAT", "NOM_VEGETALIEN", "NOM_STANDARD", "PRIORITAIRE"];

    // bail-out if one of the expected header is absent
    $diff = array_diff($off_header, array_intersect($header, $off_header));
    if($diff) {
      throw new \vgdb\Exception("missing columns: {implode(', ', $diff)}");;
    }

    $header = array_flip($header);

    $stmt = "INSERT INTO {$DBPX}plat (id, nom_vegetalise, nom_traditionnel, prioritaire) VALUES ";
    $stmt_vars = $values = []; $i = 1; $test_new_values = NULL; $test_id = rand(4, 50);
    $dup_tab = [];
    while( ($d = fgetcsv($x, 1024, "\t")) ) {
      $id = $d[$header['ID_PLAT']];
      $nomvg = $d[$header['NOM_VEGETALIEN']];
      $nomstd = $d[$header['NOM_STANDARD']];
      $prio = $d[$header['PRIORITAIRE']];

      if(! is_numeric($id) || $id >= 1000) {
        throw new \vgdb\Exception("ID_PLAT:$id invalid");
      }

      if(isset($dup_tab[$id])) {
        throw new \vgdb\Exception("duplicated line for ID_PLAT:$id");
      }
      $dup_tab[$id] = TRUE;

      if($i == $test_id) $test_new_values = [ $id, $nomvg, $nomvg ];

      // format CSV: "id" "plat vgl", "plat nutrinet", "prioritaire"
      $stmt_vars[] = '(?, ?, ?, ?)';
      array_push($values, $id, $nomvg, $nomstd, $prio == 1);
      $i++;
    }
    fclose($x);

    if($test_new_values) {
      global $db, $DBPX;
      $current_values = $db->query(sprintf("SELECT id, nom_vegetalise, nom_traditionnel, prioritaire FROM ${DBPX}plat WHERE id = %d",
                                           $test_new_values[0]))->fetch(PDO::FETCH_ASSOC);
      if($current_values) {
        if($current_values['nom_vegetalise'] != $test_new_values[1] &&
           $current_values['nom_traditionnel'] != $test_new_values[2]) {
          $warn1 = "unconsistent update\n" . 
            "previously\t({$current_values['id']}, {$current_values['nom_vegetalise']}, {$current_values['nom_traditionnel']}),\nbecame\t\t({$test_new_values[1]}, {$test_new_values[2]}) !!\n";
          if(! $force) {
            throw new \vgdb\Exception("$warn1");
          }
          else $warn[] = $warn;
        }
        // else test pass !
      }
      // else: this value will be new, can't compare with a previous one
    }

    return [ $stmt, $stmt_vars, $values ];
  }

  function importFromFile($stmt, $stmt_vars, $values) {
    global $db, $DBPX;

    // préparation de l'insertion
    $s = $db->prepare($stmt . ' ' . implode(', ', $stmt_vars));
    // préparation de la suppression des anciennes données automatiques
    $s2 = $db->prepare("DELETE FROM {$DBPX}plat WHERE id < 1000");
    $s2->execute(); $deleted = $s2->rowCount();
    \vgdb\log\record("Réimport des plats ($deleted suppressions)...");

    // insertions proprement dite
    $s->execute($values); $added = $s->rowCount();
    \vgdb\log\record("... et $added additions");

    return [ $deleted, $added ];
  }

  function import($vgl, $tradi, $prio) {
  }
}


namespace vgdb\Recette {
  use PDO;
  require_once(__DIR__ . '/../connect.php');
  require_once(__DIR__ . '/libvgdb-sys.php');

  define('IMPORT_ASNEW',	0x001);
  define('IMPORT_OVERWRITE',	0x002);
  define('IMPORT_FORCE',	0x004);
  define('IMPORT_ALL',		IMPORT_ASNEW | IMPORT_OVERWRITE | IMPORT_FORCE);

  function getFromPlatID($plat_id, $with_ingredient_count = FALSE) {
    global $db, $DBPX;
    if(! $with_ingredient_count)
      return $db->query("SELECT * FROM {$DBPX}recette WHERE id_plat = $plat_id")->fetchAll(PDO::FETCH_ASSOC);
    else
      return $db->query("SELECT r.*, COUNT(c.ORIGFDCD) AS count FROM {$DBPX}recette r LEFT JOIN {$DBPX}composition c on c.id_recette = r.id WHERE r.id_plat = $plat_id GROUP BY r.id")->fetchAll(PDO::FETCH_ASSOC);
  }

  // In most-case a drop-in replacement for the above
  function getFromPlatIDIndexed($plat_id) {
    global $db, $DBPX;
    return $db->query("SELECT id, r.* FROM {$DBPX}recette r WHERE id_plat = $plat_id")->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);
  }

  function getAll($withmeta = FALSE) {
    global $db, $DBPX;
    return $db->query(sprintf("SELECT id, nom, id_plat %s FROM {$DBPX}recette",
                              $withmeta ? ", metadata, freetext" : ""))->fetchAll(PDO::FETCH_ASSOC);
  }

  function get($recette_id, $withmeta = FALSE) {
    global $db, $DBPX;
    if(! is_numeric($recette_id)) return FALSE;
    return $db->query(sprintf("SELECT id, nom, id_plat %s FROM {$DBPX}recette WHERE id = %d",
                              $withmeta ? ", metadata, freetext " : "", $recette_id))->fetch(PDO::FETCH_ASSOC);
  }

  function getFromName($recette_name) {
    global $db, $DBPX;
    return $db->query("SELECT * FROM {$DBPX}recette WHERE nom = " . $db->quote($recette_name))->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Retourne une liste des ingrédients formaté sur mesure.
   *
   * @param $recette_id un id de recette
   * @param $extras défini les extra-données
   * @param $mode PDO::FETCH_ASSOC par défaut, autre usage: PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE
   *
   * $extras = 0 : seules les valeurs de {composition} sont retournées
   * $extras & 0x01 : les noms et groupes des ingrédients sont retournés {FOOD} et {FOOD_GOUPS}
   * $extras & 0x02 : les noms, groupes et masses volumiques des ingrédients sont retournés
   *		      {FOOD}, {FOOD_GOUPS} et {masvol}
   */
  function getIngredients($recette_id, $extras = 0, $mode = PDO::FETCH_ASSOC) {
    global $db, $DBPX;
    if(! $extras) {
      return $db->query("SELECT ORIGFDCD, quantite, unite FROM {$DBPX}composition WHERE id_recette = $recette_id")->fetchAll($mode);
    }

    $select = ['c.ORIGFDCD', 'quantite', 'unite'];
    $joins = [];
    if(!! ($extras & 0x01)) {
      array_push($select, 'G.ORIGGPFR', 'F.ORIGFDNM');
      array_push($joins,
                 "INNER JOIN {$DBPX}FOOD F ON (c.ORIGFDCD = F.ORIGFDCD)",
                 "INNER JOIN {$DBPX}FOOD_GROUPS G ON (F.ORIGGPCD = G.ORIGGPCD)");
    }

    if(!! ($extras & 0x02)) {
      array_push($select, 'm.masvol');
      array_push($joins, "LEFT JOIN {$DBPX}masvol m ON (c.ORIGFDCD = m.ORIGFDCD)" );
    }

    return $db->query(sprintf("SELECT %s FROM {$DBPX}composition c %s WHERE id_recette = $recette_id",
                              implode(',', $select),
                              implode(' ', $joins)))->fetchAll($mode);
  }

  function getImages($recette_id, $with_filenames = FALSE) {
    require_once('libvgdb-sys.php');
    $x = glob(sprintf('%s/../upload/%03d_*', __DIR__, $recette_id));
    array_walk($x, function (&$v) use($with_filenames)
               {
                 if(! $with_filenames) $v = \vgdb\sys\path2uri(realpath($v));
                 else $v = [ 'rel' => realpath($v), 'uri' => \vgdb\sys\path2uri(realpath($v)) ];
               });
    return $x;
  }

  function add($plat_id, $nom) {
    global $db, $DBPX;
    if(! trim($nom)) return FALSE;

    $s = $db->prepare("INSERT INTO {$DBPX}recette (id_plat, nom) VALUES(?, ?)")->execute(array($plat_id, $nom));
    $id = $db->lastInsertId();
    if(is_numeric($id)) {
      \vgdb\log\record("Ajout de la recette \"$nom\" #$id");
      return $id;
    }
    return FALSE;
  }

  function del() {
    global $db, $DBPX;
    $s = $db->prepare("DELETE FROM {$DBPX}recette WHERE id = ?");
    $r = get($_GET['recette_id']);
    if($r) { // $recette['id_plat']
      // attention, les ingrédients pour cette recette ne sont PAS supprimés.
      // TODO ?
      if(! $s->execute(array($_GET['recette_id']))) die('error à la suppression de la recette!');
      \vgdb\log\record("Suppression de la recette #{$_GET['recette_id']}");
      // assumant que cette page n'est appelée que depuis le lien de recettes.php:
      return [0, "Recette supprimée"];
    }
    else {
      return [1, "Recette à supprimer non-spécifiée/non-existante"];
    }
  }

  function alter($id, $nom, $meta, $freetext) {
    global $db, $DBPX;
    if(! ($recette = get($id)) ) {
      // die('error: aucun plat correspondant');
      return false;
    }

    if(! $nom) {
      die('error: aucun nom-vg donné');
      return false;
    }

    $stmt = $db->prepare("UPDATE {$DBPX}recette SET nom = ?, metadata = ?, freetext = ? WHERE id = ?");
    $x = $stmt->execute( [ $nom, html_entity_decode($meta) ? : NULL, html_entity_decode($freetext) ? : NULL, $id ] );

    if(! $x) {
      var_dump($x, $stmt, $_POST, $db->errorInfo());die;
    }

    if($stmt->rowCount() == 1) {
      \vgdb\log\record("Modification de la recette \"$nom\" #$id");
      return true;
    }
    // else: pas de mise à jour des lignes
    return 0; // != (TRUE|FALSE) = nothing
  }

  /*
    créé ou écrase une recette à partir d'un fichier d'import

    $data: les valeurs (dé-json_encodées) représentant recette + ingrédient
    $id_recette: out: id de la recette utilisé (peut être celui de data, un autre)
    $opt: option(s) définissant l'écrasement ou la recréation de recette en cas de conflit
  */

  function import($data, $data_ingredient = NULL, $opt = IMPORT_ALL) {
    global $db, $DBPX;
    $recette = NULL;

    if(! $data) return FALSE;
    $recette = get(@$data['id'], true);
    if(! $recette && @$data['nom'])
      $recette = @getFromName($data['nom'])[0];


    if($recette) {
      // OVERWRITE ?
      if(! !!($opt & IMPORT_OVERWRITE) ) return FALSE;
      if($recette['id_plat'] != $data['id_plat'] && ! !!($opt & IMPORT_FORCE)) {
        // inconsistency ... TODO: optionnel ?
        return FALSE;
      }
    }

    if(! $recette && ! $data['id_plat']) return FALSE;
    // (pour une recette récupérée, nous pourrions rendre ça optionnel)

    // un recette à importer doit avoir un plat associé existant
    $plat = \vgdb\Plat\get($data['id_plat']);
    if(! $plat) return FALSE;


    // UPDATE si recette existante, INSERT autrement
    if($recette) {
      $stmt = $db->prepare("UPDATE {$DBPX}recette SET nom = ?, metadata = ?, freetext = ? WHERE id = ?");
      // TODO: import the plat_id ?
      $x = $stmt->execute( [@$data['nom'],
                            isset($data['metadata']) && $data['metadata'] ? : NULL,
                            isset($data['freetext']) && $data['freetext'] ? : NULL,
                            $recette['id'] ]);

      if(!$x) return FALSE;
      // réinsérée "correctement", mais sans de mise à jour nécessaire
      if($stmt->rowCount() == 0) $rid = $recette['id'];
      else {
        $rid = $db->lastInsertId(); // TODO... recette insérée ?
        \vgdb\log\record("Import de recette, recette #$rid modifiée");
      }
    } else {
      //TODO: if "id" spec: use it !
      $stmt = $db->prepare("INSERT INTO {$DBPX}recette (nom, metadata, freetext, id_plat) VALUES (?, ?, ?, ?)");
      $x = $stmt->execute(array(
        $data['nom'],
        $data['metadata'] ? : NULL,
        $data['freetext'] ? : NULL,
        $plat['id']));

      if(!$x) return FALSE;
      $rid = $db->lastInsertId();
      \vgdb\log\record("Import de recette, recette insérée #$rid");
    }


    /* finalisation d'import d'une recette, que ce soit mise à jour ou
       insert, on peut passer aux ingrédients s'ils ont été passés en argument.
       Note $rid doit être défini ! */
    // var_dump("import recette = ", $rid);
    if(! $data_ingredient) return $rid;
    return importIngredients($rid, $data_ingredient, $opt);
  }

  function _ingredientFormToArray($data, $id_recette, $plat_id) {
    if(is_array($data) && ! count($data)) throw new \Exception('error: aucun ingrédient');
    if(! $data) return [];

    $ingredients = [];
    foreach($data as $i) {
      // commun à tous les records à enregistrer
      $x = [$id_recette, $plat_id];
      list($x[2], $x[3], $x[4]) = explode(':', $i);
      ksort($x);
      $ingredients[] = $x;
    }

    return $ingredients;
  }

  function addIngredients($id_recette, $ingredients) {
    global $db, $DBPX;

    $nb_exclam = count($ingredients);

    $ingredients_flat = [];
    foreach($ingredients as $i) $ingredients_flat = array_merge($ingredients_flat, $i);

    $stmt_vars = [];
    while($nb_exclam--) $stmt_vars[] = '(?, ?, ?, ?, ?)';

    $s = $db->prepare("INSERT INTO {$DBPX}composition"
                      . " (id_recette, id_plat, ORIGFDCD, quantite, unite)"
                      . " VALUES "
                      . implode(', ', $stmt_vars)); 

    // supprime les anciens ingrédients
    $db->prepare("DELETE FROM {$DBPX}composition WHERE id_recette = $id_recette")->execute();
    // log la modification qui démarre
    \vgdb\log\record("Modification d'ingrédient de la recette #$id_recette");
    // et insère tous les nouveaux... si nécessaire
    if($ingredients_flat) return $s->execute($ingredients_flat);
    else return TRUE;
  }

  function importIngredients($rid, $data, $opt = IMPORT_ALL) {

    if(! $data || ! is_array($data)) return FALSE;
    if(! ($recette = get($rid)) ) return FALSE;

    $ingredients = [];
    foreach($data as $ingr) {
      $x = array_intersect_key($ingr, array_flip(['ORIGFDCD', 'quantite', 'unite']));
      if(count($x) != 3) { echo "warning!\n"; continue; }
      $ingredients[] = [$recette['id'],
                        $recette['id_plat'],
                        $ingr['ORIGFDCD'],
                        $ingr['quantite'],
                        $ingr['unite']
      ];
    }

    return addIngredients($recette['id'], $ingredients);
  }
}

namespace vgdb\Ingredient {
  use PDO;
  require_once(__DIR__ . '/../connect.php');

  /* unused : currently fetch directly by \vgdb\Recette\getIngredients() */
  /*
    function getMassesVolumiques($ORIGFDCDs) {
    global $db, $DBPX;

    if(! $ORIGFDCDs) return [];
    $res = $db->query(sprintf("SELECT * FROM {$DBPX}masvol WHERE ORIGFDCD IN (%s)",
    implode(',',$ORIGFDCDs)))->fetchAll(PDO::FETCH_KEY_PAIR);
    var_dump($res);die;
    // si l'un des ingrédients n'a pas sa masse volumique renseignée ...
    if(count($res) != count($ORIGFDCDs)) return FALSE;
    return $res;
    }
  */

  /*
    Retourne la masse *normalisée* d'une recette à partir du tableau de ses ingrédients sous
    la forme:
    array( 9410 => array ( 'quantite' => '180', 'unite' => 'g', 'masvol' => null ) )

    La valeur retournée est un entier indiquant une valeur en grammes.
    Ici sont/seront effectuées les conversions nécessaires pour passer d'un ingrédient indiqué en volume
    (mL, cuillère à café ou à soupe) vers une valeur en grammes.
  */
  function sum($ingredients = NULL, &$missing = NULL) {
    if(! $ingredients) return FALSE;

    $converted_to_grammes = array_map(function($t) {
                                                      return masse($t['unite'], $t['quantite'], $t['masvol']);
                                                    }, $ingredients);

    if(in_array(0, $converted_to_grammes, TRUE)) {
      if(! is_null($missing)) $missing = array_keys($converted_to_grammes, 0, TRUE);
      return FALSE;
    }
    return array_sum($converted_to_grammes);
  }

  function sumFromRecetteId($recette_id = NULL, &$missing = NULL) {
    if(! is_numeric($recette_id)) return FALSE;
    return sum(\vgdb\Recette\getIngredients($recette_id, 0x02, PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE), $missing);
  }

  /*
    Retourne la masse (en grammes) d'un aliment exprimé en volume,
    prenant en compte sa masse volumique
  */
  function masse($unite, $quantite, $masvol) {
    switch($unite) {
    case 'g':
      return floatval($quantite);
    case 'mL':
      return $quantite * $masvol;
    case 'cs':
      return $quantite * VOL_CS * $masvol;
    case 'cc':
      return $quantite * VOL_CC * $masvol;
    default:
      die("aie: unité non-supportée: $unite: ($quantite, $masvol)");
    }
  }

  // TODO: idéalement: méthode d'une \vgdb\Recette pour un ingrédient donné
  /*
    Donne la masse d'un ingrédient relativement à 100g de recette à partir
    de la masse totale de la recette ainsi que des caractériques de l'ingrédient
    comme sa masse volumique.
  */
  function getMasseRelative($masse_recette, $ORIGFDCD, $ingredient) {
    $quantite_normalisee = masse($ingredient['unite'], $ingredient['quantite'], $ingredient['masvol']);
    $i_pourcent_masse = round($quantite_normalisee / $masse_recette * 100, 2);
    return $quantite_normalisee / $masse_recette;
  }
}

namespace vgdb\io {
  use PDO;
  require_once(__DIR__ . '/../connect.php');

  function export_masvol() {
    global $db, $DBPX;

    return $db->query("SELECT * FROM {$DBPX}masvol")->fetchAll(PDO::FETCH_ASSOC);
  }

  /* return an associative array where:
     "log" => array of text logs
     "total" => total number of recipes provided
     "succeeded" => number of recipes successfully imported */
  function import($filesnames) {
    $total = 0;
    $log = []; $succeeded = 0;

    foreach($filesnames as $f) {
      $t = json_decode(file_get_contents($f), TRUE);
      // single recette ?
      if($t && array_key_exists("recette", $t)) $t = [ $t ];
      $total += count($t);

      foreach($t as $recette) {
        try {
          $x = \vgdb\Recette\import($recette['recette'], $recette['ingredients']);
          // var_dump($recette, $x);
          if($x) $succeeded++;
          sprintf("debug: import %d ingrédients for recette %s<br/>", $x, $recette['recette']['nom']);
        } catch(Exception $e) { $log[] = $e->getMessage(); }
      }
    }
    return [ "log" => $log, "total" => $total, "succeeded" => $succeeded ];
  }
}
