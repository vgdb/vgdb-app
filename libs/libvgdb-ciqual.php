<?php

namespace vgdb\CiqualTools;

require_once(__DIR__ . '/../connect.php');
require_once('libCiqual.php');
use PDO;

// XXX: use "const" when it support math expr: 
// https://wiki.php.net/rfc/const_scalar_expressions


// eg: SELVALtexte: < 0.42
define('VGDB_APPROX_IGNORE',	1 << 0);
define('VGDB_APPROX_KEEP',		1 << 1);
define('VGDB_APPROX_DISCARD',	1 << 2);
define('VGDB_APPROX_ROUND',		1 << 3);

// eg: SELVALtexte: NULL
define('VGDB_NULL_IGNORE',		1 << 4);
define('VGDB_NULL_KEEP',			1 << 5);
define('VGDB_NULL_DISCARD',		1 << 6);

// eg: SELVALtexte: traces
define('VGDB_TRACES_IGNORE',	1 << 7);
define('VGDB_TRACES_KEEP',		1 << 8);
define('VGDB_TRACES_DISCARD',	1 << 9);


// eg: SELVALtexte: -
define('VGDB_DASH_IGNORE',		1 << 10);
define('VGDB_DASH_KEEP',			1 << 11);
define('VGDB_DASH_DISCARD',		1 << 12);


// shortcuts
define('VGDB_ALL_IGNORE',			VGDB_APPROX_IGNORE|VGDB_NULL_IGNORE|VGDB_TRACES_IGNORE|VGDB_DASH_IGNORE);
define('VGDB_ALL_KEEP',				VGDB_APPROX_KEEP|VGDB_NULL_KEEP|VGDB_TRACES_KEEP|VGDB_DASH_KEEP);
define('VGDB_ALL_DISCARD',		VGDB_APPROX_DISCARD|VGDB_NULL_DISCARD|VGDB_TRACES_DISCARD|VGDB_DASH_DISCARD);

/*
  Return a NULL/0 value according to the Ciqual Stats calc mode.
  If the value itself is changed by the mode, just change it and return FALSE.
  If there is nothing we can do : return FALSE.
 */
function procCiqualStr(&$str, $opt, $approx_round_discard = TRUE) {
  if(is_null($str) && ($opt & VGDB_NULL_DISCARD)) { return NULL; }
  if(is_null($str) && ($opt & VGDB_NULL_IGNORE)) { return 0; }
  if(is_null($str) && ($opt & VGDB_NULL_KEEP)) { $str = 0; return FALSE; } // TODO: how to keep this ??
  if($str == '-' && ($opt & VGDB_DASH_DISCARD)) { return NULL; }
  if($str == '-' && ($opt & VGDB_DASH_IGNORE)) { return 0; }
  // if($str == '-' && ($opt & VGDB_DASH_KEEP)) { $str = 0; return FALSE; } // TODO: how to keep this ?? we don't want "-" to mess with \myeval()
  if($str == 'traces' && ($opt & VGDB_TRACES_DISCARD)) { return NULL; }
  if($str == 'traces' && ($opt & VGDB_TRACES_IGNORE)) { return 0; }
  if(strpos($str, '<') !== FALSE && ($opt & VGDB_APPROX_DISCARD)) { return NULL; }
  if(strpos($str, '<') !== FALSE && ($opt & VGDB_APPROX_IGNORE)) { return 0; }
  /* if(strpos($str, '<') !== FALSE) {
    if($approx_round_discard) return NULL;
    $str = substr($str, strpos($str, '<') + 1);
    return FALSE;
  } */
  return FALSE;
}

function extractValidIngredients($ingredients, $approx_recette) {
  return array_filter($ingredients,
                      function($v) use($approx_recette)
                      {
                        if(is_null($v['SELVALtexte']) ||
                           $v['SELVALtexte'] == '-' ||
                           $v['SELVALtexte'] == 'traces') return FALSE;
                        return TRUE;
                      });
}


function unsafeApproximation($valid_ingredients) {
  $exact_ingredients = array_filter($valid_ingredients,
                                    function($v) /*use($approx_recette)*/
                                    {
                                      return strpos($v['SELVALtexte'], '<') === FALSE;
                                    });

  $approx_ingredients = array_filter($valid_ingredients,
                                     function($v) /*use($approx_recette)*/
                                     {
                                       return strpos($v['SELVALtexte'], '<') !== FALSE;
                                     });

  if(! $approx_ingredients) return TRUE; // shortcut (sum = 0 < sum total)

  $exact_ingredients_mass = array_sum(array_map(
    function($v) {
      return \Ciqual\Component\myeval("( {$v['SELVALtexte']} ) * {$v['masseabs']} / 100");
    }, $exact_ingredients));

  $approx_ingredients_mass = array_sum(array_map(
    function($v) {
      return \Ciqual\Component\myeval("(" .
                                      substr($v['SELVALtexte'], strpos($v['SELVALtexte'], '<') + 1) . 
                                      ") * {$v['masseabs']} / 100");
    }, $approx_ingredients));

  if($approx_ingredients_mass && $approx_ingredients_mass >= $exact_ingredients_mass * 0.05) {
    // the approximated values could represent as much as 5% of the total mass for
    // this nutrient. This is way too much uncertainity. abort!
    return FALSE;
  }

  return TRUE;
}

/* at least one of the ingredients of this recipe met both conditions:
   - a volume has been input rather than a mass
   - no volumic mass has been registered
   This blocks calculation of *all* the nutrients for this recipe. */
function missingDensity($data) {
  $massevols = array_unique(array_map(function ($n) { return $n['isok']; }, $data));
  return array_search('0', $massevols, TRUE) !== FALSE;
}

function missingDensityFDCD($data) {
  return array_filter(array_map(function ($n) {
                                                 return $n['isok'] ? NULL : $n['ORIGFDCD'];
                                               },
                                $data ));
}


/* Example:
   SELECT quantite, unite, masvol, quantite * IF(masvol IS NOT NULL AND unite != 'g', masvol, 1) * IF(unite = "g", 1, IF(unite = "mL", 1, IF(unite = "cs", 6.6, IF(unite = "cs", 20, 0)))) AS masseabs, SELVALtexte
   FROM vgdb_composition a
   LEFT JOIN vgdb_COMPILED_DATA d ON a.ORIGFDCD = d.ORIGFDCD
   LEFT JOIN vgdb_COMPONENT c ON c.ORIGCPCD = d.ORIGCPCD
   LEFT JOIN vgdb_masvol m ON a.ORIGFDCD = m.ORIGFDCD
   WHERE id_recette = 1 AND C_ORIGCPNMABR = "Selenium (µg/100g)" ...
   +----------+-------+--------+----------+-------------+
   | quantite | unite | masvol | masseabs | SELVALtexte |
   +----------+-------+--------+----------+-------------+
   | 180      | g     | NULL   |      180 | < 5         |
   | 180      | g     | NULL   |      180 | 25,6        |
   | 18       | mL    | 8      |      144 | 2,2         |
   | 16       | g     | NULL   |       16 | 2,2         |
   | 18       | g     | NULL   |       18 | 0           |
   +----------+-------+--------+----------+-------------+
*/
function RecetteGetNutrimentsByCPNMABR($recette_id, $nutname = \Ciqual\Component\KCAL_NAME /* "Energie, Règlement UE 1169/2011 (kcal/100g) : 328 */) {
  global $db, $DBPX;

  $req = sprintf("SELECT"
                 . " REPLACE(quantite, ',', '.') * IF(masvol IS NOT NULL AND unite != 'g', masvol, 1) * IF(unite = 'g', 1, IF(unite = 'mL', 1, IF(unite = 'cc', " . VOL_CC . ", IF(unite = 'cs', " . VOL_CS . ", 0)))) AS masseabs, "
                 . " REPLACE(SELVALtexte, ',', '.') AS SELVALtexte"

                 . " FROM ${DBPX}composition a"

                 . " LEFT JOIN ${DBPX}COMPILED_DATA d ON a.ORIGFDCD = d.ORIGFDCD"
                 . " LEFT JOIN ${DBPX}COMPONENT c ON c.ORIGCPCD = d.ORIGCPCD"
                 . " LEFT JOIN ${DBPX}masvol m ON a.ORIGFDCD = m.ORIGFDCD"

                 . " WHERE id_recette = %d AND C_ORIGCPNMABR = %s",
                 $recette_id,
                 $db->quote($nutname));
  return $db->query($req)->fetchAll(PDO::FETCH_ASSOC);
}

function RecetteGetNutrimentsByCPCD($recette_id, $ORIGCPCD = \Ciqual\Component\KCAL_CODE /* "Energie, Règlement UE 1169/2011 (kcal/100g)  : 328 */) {
  global $db, $DBPX;

  return $db->query(sprintf("SELECT"
                            . " REPLACE(quantite, ',', '.') * IF(masvol IS NOT NULL AND unite != 'g', masvol, 1) * IF(unite = 'g', 1, IF(unite = 'mL', 1, IF(unite = 'cc', " . VOL_CC . ", IF(unite = 'cs', " . VOL_CS . ", 0)))) AS masseabs, "
                            . " REPLACE(SELVALtexte, ',', '.') AS SELVALtexte"

                            . " FROM ${DBPX}composition a"

                            . " LEFT JOIN ${DBPX}COMPILED_DATA d ON a.ORIGFDCD = d.ORIGFDCD"
                            . " LEFT JOIN ${DBPX}COMPONENT c ON c.ORIGCPCD = d.ORIGCPCD"
                            . " LEFT JOIN ${DBPX}masvol m ON a.ORIGFDCD = m.ORIGFDCD"

                            . " WHERE id_recette = %d AND C_ORIGCPNMABR = %s",
                            $recette_id, $ORIGCPCD))->fetchAll(PDO::FETCH_ASSOC);
}







/*
  $approx_ingredient, $approx_recette and $approx_plat are bitwise-based constant used to
  manage "special values" from the Ciqual database.
  We could IGNORE them, KEEP them, DISCARD a calculation using them or even, sometimes, ROUND them.
  This applies at 3 distinct level of processing:
  - fetching SELVALtexte from the DB
  - calculating a mean for a given recipe from the ingredients
  - calculating a mean for a given dish from the recipes' means

  No consistency-checks are done here, but obviously, IGNORING an ingredient special value makes
  impossible to KEEP it later in the process.

  Note: DISCARD > IGNORE > ROUND > KEEP.
  If a recipe is DISCARDed because of one of its clause (eg: VGDB_DASH_DISCARD).
  The VGDB_NULL_* policy will be used in the following calculations relying on it.

  eg:
  * ingredients SELVALtexte      = -
  * ingredients real SELVALtexte = NULL (because VGDB_DASH_DISCARD) for ingredients
  * recipes mean                 = NULL (because VGDB_NULL_DISCARD) for recettes
  * dish    mean                 = NULL (because VGDB_NULL_DISCARD) for dishes

  eg:
  * ingredients SELVALtexte      = -
  * ingredients real SELVALtexte = -    (because VGDB_DASH_KEEP)    for ingredients
  * recipes mean                 = NULL (because VGDB_DASH_DISCARD) for recettes
  * dish    mean                 = NULL (because VGDB_NULL_DISCARD) for dishes

  It's advised to KEEP_ALL at the lowest level ($approx_ingredient) then filter-out unreliable
  values for the respective calculation of each recipe's mean then the dish' mean.
  For a maximum safety, it's advised to VGDB_NULL_DISCARD at the highest level ($approx_plat)
*/

function PlatGetStatistics(&$tab, $plat_id, $approx_ingredient, $approx_recette, $with_min_max = FALSE) {
  if(!$tab) {
    $tab = ['recettes_totaux' => [],
            'total' => [],
            'meta' => ['count' => NULL,
                       'ignored' => NULL,
                       'can_mean' => NULL] ];
  }

  $x = GetAllNutrientsForPlat($tab, $plat_id, $approx_ingredient, $approx_recette, $with_min_max);
  return RecipeNormalizationFromData($tab, $x, $approx_recette, $with_min_max);
}

function GetAllNutrientsForPlat(&$tab, $plat_id, $approx_ingredient, $approx_recette, $with_min_max = FALSE) {

  // list the recipes's ids (later used as index in $stats_tab)
  $recettes = \vgdb\Recette\getFromPlatIDIndexed($plat_id);
  $tab['count'] = count($recettes);

  $tmp_tab = []; // contains raw nutritionnal values per-ingredient per-recipe
  $tmp_tab_m = []; // contains the total mass of a given recipe (sum of the ingredient's mass')
  /* Grab all recipes and returns the raw table of ingredient nutritionnal values. */
  foreach(array_keys($recettes) as $rid) {
    GetAllNutrientsForRecipe($rid, $tmp_tab[$rid], $tmp_tab_m[$rid], $approx_ingredient, $with_min_max);
  }
  return ['per-rid' => $tmp_tab, 'masse' => $tmp_tab_m];
}

function RecipeNormalizationFromData(&$tab, $data, $approx_recette, $with_min_max = FALSE) {
  $can_mean = TRUE;
  $stats_tab = [];
  $ignored_recipes = [];
  /* Compute the means for each of the recipes.
     This will fill-in $stats_tab indexed by recette_id.
     Note iterating over $data keys or $recettes should be identical*/
  foreach($data['per-rid'] as $rid => $v) {
    if(! $data['masse'][$rid]) { $ignored_recipes[] = $rid; continue; } // TODO: empty recipe: specs ?!

    $can_mean &= ComputeRecetteValNut($v, $data['masse'][$rid], $stats_tab[$rid], $approx_recette, $with_min_max);
  }

  $tab['recettes_totaux'] = $stats_tab;
  $tab['meta']['ignored'] = $ignored_recipes;
  $tab['meta']['can_mean'] =  $can_mean;

  return $can_mean;
}


/* HELPERS */


/*
  We may need to fetch the nutritional values for all compononents of all ingredient of a given recette_id.
  And we must avoid this code pattern which would be damn slow :

  >>>>>>>>>>>>>>>>>>>>>
  $masse_x = NULL;
  foreach(\Ciqual\Component\getCPN() as $k => &$v) {
  $product_table = \vgdb\CiqualTools\RecetteGetNutrimentsByCPNMABR($recette_id, $v['C_ORIGCPNMABR']);
  if(!$masse_x) $masse_x = array_sum(array_map(function($n) { return $n['masseabs']; }, $product_table));
  $somme_nut = \Ciqual\Component\sum(\Ciqual\Component\getAllQuantities($product_table, CIQUAL_CPN_CONSERVE), CIQUAL_CPN_SET0);
  $v['somme'] = $somme_nut / $masse_x * 100;
  }
  var_dump($tab);
  <<<<<<<<<<<<<<<<<<<<<

  Thus the one-time function below which will fill both $masse_recette (int) and $tab as:
  Array (
  [327] => Array (
  [C_ORIGCPNMABR] => Energie, Règlement UE 1169/2011 (kJ/100g)
  [somme] => 1351.4260223048
  )
  [328] => Array (
  [C_ORIGCPNMABR] => Energie, Règlement UE 1169/2011 (kcal/100g)
  [somme] => 321.25568773234
  )
  ...
  [10300] => Array (
  [C_ORIGCPNMABR] => Zinc (mg/100g)
  [somme] => 1.1697397769517
  )
  [10340] => Array (
  [C_ORIGCPNMABR] => Sélénium (µg/100g)
  [somme] => 9.2193308550186
  )
  [10530] => Array (
  [C_ORIGCPNMABR] => Iode (µg/100g)
  [somme] => 2.2029739776952
  )
  [25000] => Array (
  [C_ORIGCPNMABR] => Protéines (g/100g)
  [somme] => 8.2636431226766
  )
  ...
  )
*/

function GetAllNutrientsForRecipe($recette_id, &$tab, &$masse, $approx_ingredient, $with_min_max = FALSE) {
  global $db, $DBPX;


  $selectSELVALtexte = "REPLACE(SELVALtexte, ',', '.')";
  if($approx_ingredient != VGDB_ALL_KEEP) {
    $selectSELVALtextePrintf = 
      "IF(SELVALtexte IS NULL, %s, " . (
        "IF(SELVALtexte = 'traces', %s, " . (
          "IF(SELVALtexte = '-', %s, " . (
            "IF(LOCATE('<', SELVALtexte) != 0, %s, " . (
              "REPLACE(SELVALtexte, ',', '.')"
            )
          )
        )
      ) . '))))';
    $selectSELVALtexte = sprintf(
      $selectSELVALtextePrintf,
      // NULL values:
      ($approx_ingredient & VGDB_NULL_DISCARD) ? "NULL" : ( ($approx_ingredient & VGDB_NULL_IGNORE) ? 0 : "NULL" ),
      // "traces"
      ($approx_ingredient & VGDB_TRACES_DISCARD) ? "NULL" : ( ($approx_ingredient & VGDB_TRACES_IGNORE) ? 0 : "'traces'" ),
      // dash (-)
      ($approx_ingredient & VGDB_DASH_DISCARD) ? "NULL" : ( ($approx_ingredient & VGDB_DASH_IGNORE) ? 0 : "'-'" ),
      // approx (< 0.42)
      ($approx_ingredient & VGDB_APPROX_DISCARD) ? "NULL" : ( ($approx_ingredient & VGDB_APPROX_IGNORE) ? 0 :
                                                          (($approx_ingredient & VGDB_APPROX_KEEP) ? "REPLACE(SELVALtexte, ',', '.')" :
                                                           /* APPROX_ROUND: */ "SUBSTRING(REPLACE(SELVALtexte, ',', '.') FROM 3)") )
    );
  }

  $select_min_max = ", VALMIN, VALMAX, N";

  // WARN, with 950 ingredients, this can reach 100M
  $req = sprintf("SELECT"
                 . " c.ORIGCPCD, c.C_ORIGCPNMABR, a.ORIGFDCD, "
                 . " IF(unite != 'g' AND masvol IS NULL, 0, 1) AS isok, "
                 . " REPLACE(quantite, ',', '.') * IF(masvol IS NOT NULL AND unite != 'g', masvol, 1) * IF(unite = 'g', 1, IF(unite = 'mL', 1, IF(unite = 'cc', " . VOL_CC . ", IF(unite = 'cs', " . VOL_CS . ", 0)))) AS masseabs, "
                 . " %s AS SELVALtexte"
                 . " %s" // min/max/N
			      
                 . " FROM ${DBPX}composition a"
			      
                 . " LEFT JOIN ${DBPX}COMPILED_DATA d ON a.ORIGFDCD = d.ORIGFDCD"
                 . " LEFT JOIN ${DBPX}COMPONENT c ON c.ORIGCPCD = d.ORIGCPCD"
                 . " LEFT JOIN ${DBPX}masvol m ON a.ORIGFDCD = m.ORIGFDCD"

                 . " WHERE id_recette = %d ORDER BY ORIGCPCD",
                 
                 $selectSELVALtexte,
                 $with_min_max ? $select_min_max : '',
                 $recette_id);

  //echo($req);die;
  $tab = $db->query($req)->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP);

  /* warn;
     since we join to COMPILED_DATA and we may not have data for this ingredient,
     the array key could thus be NULL, what's traduced by an <empty> key in the
     array */
  unset($tab['']);

  // recipe's masse is initialized here for the 1st $ORIGCPCD since masseabs values are identical for all of them
  // but the recipe could be NULL also (no ingredients)
  if(! array_key_exists(\Ciqual\Component\KCAL_CODE, $tab)) {
    $masse = 0;
    return;
  }
  $v = $tab[\Ciqual\Component\KCAL_CODE];
  if($v) $masse = array_sum(array_map(function($n) { return $n['masseabs']; }, $v));
  //TODO: set NULL for all isok = 0
  //TODO: double-ref array [FDCD-indexed] & [CPCD-indexed]
  // var_dump($tab);die;
}



function GetAllNutrientsForRecipeNEXT($recette_id, &$tab, $approx_ingredient, $with_min_max = FALSE) {
  global $db, $DBPX;

  $selectSELVALtexte = "REPLACE(SELVALtexte, ',', '.')";
  if($approx_ingredient != VGDB_ALL_KEEP) {
    $selectSELVALtextePrintf = 
      "IF(SELVALtexte IS NULL, %s, " . (
        "IF(SELVALtexte = 'traces', %s, " . (
          "IF(SELVALtexte = '-', %s, " . (
            "IF(LOCATE('<', SELVALtexte) != 0, %s, " . (
              "REPLACE(SELVALtexte, ',', '.')"
            )
          )
        )
      ) . '))))';
    $selectSELVALtexte = sprintf(
      $selectSELVALtextePrintf,
      // NULL values:
      ($approx_ingredient & VGDB_NULL_DISCARD) ? "NULL" : ( ($approx_ingredient & VGDB_NULL_IGNORE) ? 0 : "NULL" ),
      // "traces"
      ($approx_ingredient & VGDB_TRACES_DISCARD) ? "NULL" : ( ($approx_ingredient & VGDB_TRACES_IGNORE) ? 0 : "'traces'" ),
      // dash (-)
      ($approx_ingredient & VGDB_DASH_DISCARD) ? "NULL" : ( ($approx_ingredient & VGDB_DASH_IGNORE) ? 0 : "'-'" ),
      // approx (< 0.42)
      ($approx_ingredient & VGDB_APPROX_DISCARD) ? "NULL" : ( ($approx_ingredient & VGDB_APPROX_IGNORE) ? 0 :
                                                          (($approx_ingredient & VGDB_APPROX_KEEP) ? "REPLACE(SELVALtexte, ',', '.')" :
                                                           /* APPROX_ROUND: */ "SUBSTRING(REPLACE(SELVALtexte, ',', '.') FROM 3)") )
    );
  }

  $select_min_max = ", VALMIN, VALMAX, N";

  // WARN, with 950 ingredients, this can reach 76 M
  $req = sprintf("SELECT"
                 . " c.ORIGCPCD, a.ORIGFDCD, "
                 . " IF(unite != 'g' AND masvol IS NULL, 0, 1) AS isok, "
                 . " REPLACE(quantite, ',', '.') * IF(masvol IS NOT NULL AND unite != 'g', masvol, 1) * IF(unite = 'g', 1, IF(unite = 'mL', 1, IF(unite = 'cc', " . VOL_CC . ", IF(unite = 'cs', " . VOL_CS . ", 0)))) AS masseabs, "
                 . " %s AS SELVALtexte"
                 . " %s" // min/max/N
			      
                 . " FROM ${DBPX}composition a"
			      
                 . " LEFT JOIN ${DBPX}COMPILED_DATA d ON a.ORIGFDCD = d.ORIGFDCD"
                 . " LEFT JOIN ${DBPX}COMPONENT c ON c.ORIGCPCD = d.ORIGCPCD"
                 . " LEFT JOIN ${DBPX}masvol m ON a.ORIGFDCD = m.ORIGFDCD"

                 . " WHERE id_recette = %d ORDER BY ORIGCPCD",
                 
                 $selectSELVALtexte,
                 $with_min_max ? $select_min_max : '',
                 $recette_id);

  // echo($req . "<br/>\n");
  $tab['per-cpn'] = $db->query($req)->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP);

  /* warn;
     since we join to COMPILED_DATA and we may not have data for this ingredient,
     the array key could thus be NULL, what's traduced by an <empty> key in the
     array */
  if(array_key_exists('', $tab['per-cpn'])) {
    foreach($tab['per-cpn'] as $k => &$v) {
      if($k === '') continue;
      foreach($tab['per-cpn'][''] as $v2) {
        $v[] = $v2;
      }
    }
  }

  // &$v : dont' use $v up to the end of the function
  foreach($tab['per-cpn'] as $cpncd => &$ingredients) {
    foreach($ingredients as $k2 => $v2) {
      $tab['per-fdcd'][$v2['ORIGFDCD']][$cpncd] = & $tab['per-cpn'][$cpncd][$k2];
    }
  }

  // var_dump($tab);
  // TODO: set NULL for all isok = 0 ?
}




function ComputeDetailledRelativeRecetteValNut($all_ingredients, $masse_recette, &$tab = [], $approx_recette = NULL) {

  $buggy_fdcd = missingDensityFDCD($all_ingredients[\Ciqual\Component\KCAL_CODE]);

  foreach($all_ingredients as $ORIGCPCD => $ingredients) {
    foreach($ingredients as $ingredients) {
      if(in_array($ingredients['ORIGFDCD'], $buggy_fdcd)) {
        $tab[$ingredients['ORIGFDCD']][$ORIGCPCD] = NULL;
        continue;
      }

      $ret = procCiqualStr($ingredients['SELVALtexte'], $approx_recette);

      if($ret !== FALSE) {
        $tab[$ingredients['ORIGFDCD']][$ORIGCPCD] = $ret;
        continue;;
      }

      /* Will return the relative quantity of this nutrient for 100g of final dish.
         Eg, if we talk about:
         - 103g of wheat flour
         - phosphorus is 190 mg/100g of wheat flour
         - total recipe's mass is 580g.
         The amount of wheat flour's phosphorus for 100g of final recipe is:
         <abs quantity of phosphorus in the current recipe> * 100 / 580
         = $nutr_abs_quantity * 100 / 580
         = 190 * 103 / 100 * 100 / 580
         = $ingredients['SELVALtexte'] * $ingredients['masseabs'] / $masse_recette */
      $tab[$ingredients['ORIGFDCD']][$ORIGCPCD] = \Ciqual\Component\myeval("( {$ingredients['SELVALtexte']} ) * {$ingredients['masseabs']} / $masse_recette");
    }
  }
}



function applyIngredientsNormalization(&$all_ingredients_per_cpn, $masse_recette, $with_min_max = FALSE) {

  if($with_min_max) {
    foreach($all_ingredients_per_cpn as $ORIGCPCD => &$ingredients) {
      // set MIN
      array_walk($ingredients, function(&$v) use($masse_recette)
                 {
                   $v['VALMIN'] =
                     is_null($v['VALMIN']) ? NULL :
                     $v['VALMIN'] * $v['masseabs'] / 100 // valeur absolue dans la recette
                     * 100 / $masse_recette; // et normalisée pour 100g de recette
                 });

      // set MAX
      array_walk($ingredients, function(&$v) use($masse_recette)
                 {
                   $v['VALMAX'] =
                     is_null($v['VALMAX']) ? NULL :
                     $v['VALMAX'] * $v['masseabs'] / 100 // valeur absolue dans la recette
                     * 100 / $masse_recette; // et normalisée pour 100g de recette
                 });
    }
  }

  foreach($all_ingredients_per_cpn as $ORIGCPCD => &$ingredients) {
    foreach($ingredients as &$ingredient) {
      if($ingredient['SELVALtexte'] == '-' ||
         $ingredient['SELVALtexte'] == 'traces' ||
         is_null($ingredient['SELVALtexte'])) {
        $ingredient['NormSELVALtexte'] = $ingredient['SELVALtexte'];
      }

      elseif($ingredient['SELVALtexte'][0] == '<') {
        $ingredient['NormSELVALtexte'] = '< ' .
          // valeur absolue dans la recette
          (substr($ingredient['SELVALtexte'], 2) * $ingredient['masseabs'] / 100
           // et normalisée pour 100g de recette);
           * 100 / $masse_recette);
      }

      elseif(is_numeric($ingredient['SELVALtexte'])) {
        $ingredient['NormSELVALtexte'] =
          $ingredient['SELVALtexte'] * $ingredient['masseabs'] / $masse_recette;
      }


      else {
        var_dump($ingredient);
        die('big error ' . __FILE__ . ' : ' .  __LINE__);
      }

      // var_dump($exact_ingredients, $exact_ingredients_mass, $approx_ingredients, $x, $approx_ingredients_mass);    echo "<hr/>";
      // TODO: only process $valid_ingredients and forget about VGDB_ options ?
    }
  }
}






/* $ingredients : array where keys are a nutrient ID and the value an array of ingredients
   containing the value for this specific nutrient */
function ComputeRecetteValNut($all_ingredients, $masse_recette, &$tab = [], $approx_recette = NULL, $with_min_max = FALSE) {

  // initialize the total array
  foreach($all_ingredients as $ORIGCPCD => $ingredients) {
    $tab[$ORIGCPCD] = ['C_ORIGCPNMABR' => $ingredients[0]['C_ORIGCPNMABR'],
                       'somme' => NULL, /* init */
                       'VALMIN' => NULL,
                       'VALMAX' => NULL,
                       /* 'N' => NULL */ ];
  }

  if(missingDensity($all_ingredients[\Ciqual\Component\KCAL_CODE])) {
    return FALSE;
  }

  if($with_min_max) {
    foreach($all_ingredients as $ORIGCPCD => $ingredients) {
      $all_min = array_map(function($v) { return is_null($v['VALMIN']) ? NULL : $v['VALMIN'] * $v['masseabs'] / 100; }, $ingredients);
      if(array_search(NULL, $all_min, TRUE) !== FALSE) $tab[$ORIGCPCD]['VALMIN'] = NULL;
      else $tab[$ORIGCPCD]['VALMIN'] = array_sum($all_min) * 100 / $masse_recette;

      $all_max = array_map(function($v) { return is_null($v['VALMAX']) ? NULL : $v['VALMAX'] * $v['masseabs'] / 100; }, $ingredients);
      if(array_search(NULL, $all_max, TRUE) !== FALSE) $tab[$ORIGCPCD]['VALMAX'] = NULL;
      else $tab[$ORIGCPCD]['VALMAX'] = array_sum($all_max) * 100 / $masse_recette;

      // $n = array_sum(array_map(function($v) { return $v['N']; }, $ingredients));
    }
  }


  foreach($all_ingredients as $ORIGCPCD => $ingredients) {
    $map = array_map(function ($v) { return $v['SELVALtexte']; }, $ingredients);
    if(( array_search(NULL, $map, TRUE) !== FALSE && !!($approx_recette & VGDB_NULL_DISCARD)) ||
       ( array_search("traces", $map, TRUE) !== FALSE && !!($approx_recette & VGDB_TRACES_DISCARD)) ||
       ( array_search("-", $map, TRUE) !== FALSE && !!($approx_recette & VGDB_DASH_DISCARD)) ) {
      continue;
    }

    $valid_ingredients = extractValidIngredients($ingredients, $approx_recette);
    if(($approx_recette & VGDB_APPROX_ROUND)) {
      if(! unsafeApproximation($valid_ingredients)) continue;
    }

    // var_dump($exact_ingredients, $exact_ingredients_mass, $approx_ingredients, $x, $approx_ingredients_mass);    echo "<hr/>";
    // TODO: only process $valid_ingredients and forget about VGDB_ options ?

    $nutquantities = array_map(
      function($v) use ($approx_recette) {
        // var_dump("pre", $v['SELVALtexte']);

        // probably redundant with the above array_search() checks
        $ret = procCiqualStr($v['SELVALtexte'], $approx_recette);
        if($ret !== FALSE) return $ret;

        if(($approx_recette & VGDB_APPROX_ROUND)) {}

        // var_dump("post", $ret, $v['SELVALtexte']);

        /* Will return the "real" quantity of this nutrient according to the absolute mass
           of this ingredient in the recipe.
           Eg: 103g of Wheat flour, where Phosphorus is 190 mg/100g will return
           190 * 103 / 100 = 195.7 (mg) */
        return \Ciqual\Component\myeval("( {$v['SELVALtexte']} ) * {$v['masseabs']} / 100");
      },
      $ingredients);

    /* empty (= filtered array) means:
       For this recipe, and for this nutrient, none of the ingredients have a
       meaningful quantity of this nutrient (eg: all NULL) */
    $nutquantities = array_filter($nutquantities, function($n) { return ! is_null($n); });
    if(! $nutquantities) continue;

    /* ... and here we sum'up all the raw absolute quantities of this nutrient which will first
       bring a result of eg: 480 mg of Phosphorus for the recipe (which is 500g).
       So we then divide by $masse_recette then multiply by 100 to get the amount of Phosphorus in mg
       for 100g of recipe. */
    $somme = \Ciqual\Component\myeval('(' . implode('+', $nutquantities) . ") * 100 / $masse_recette");

    // if($ORIGCPCD == 10110) { var_dump(__FILE__ . ':' . __LINE__, $ingredients, $nutquantities, $somme); }

    // set $somme
    $tab[$ORIGCPCD]['somme'] = $somme;
  }

  return TRUE;
}

// Compute the mean for the *whole* dish into $total
// using $stats['recettes_totaux'] as generated from \PlatGetStatistics()
function _PlatComputeStatistics(&$total, &$meta, $stats_tab, $approx_plat, $with_min_max) {
  // Just don't count() because some of the above recipe could be empty returning NULL
  // which we won't take into account. Hopefully there's no other possibility of buggy recipes
  // throwing NULL values.
  // $total = []; // XXX
  $count = count($stats_tab);
  // $ignored = 0;

  foreach ($stats_tab as $rid => $nutri_per_rid) {
    /*
    // For this recipe, the amount of this nutrient for 100g of recipe is NULL
    // (unknown or with bad precision (DEPENDS upon the VGDB_* constants for $approx_recette and $approx_ingredient)
    if(is_null($nutri_per_rid)) {
      // if we are to ignore NULL we just make the dish's mean from other recipes :
      if(!!($approx_plat & VGDB_APPROX_IGNORE)) {
        $count--; continue;
      }
      if(!!($approx_plat & VGDB_APPROX_)) {
        $count--; continue;
      }

    }
    */
    if(is_null($nutri_per_rid)) { $count--; /* var_dump("is_null, rid = ", $rid); */ continue; } // empty recipes

    foreach ($nutri_per_rid as $nid => $data) {
      // if($nid == 10110) { var_dump($data); }
      if(! array_key_exists($nid, $total)) {
        $total[$nid] = ['C_ORIGCPNMABR' => $data['C_ORIGCPNMABR'],
                        'somme' => NULL,
                        'VALMIN' => NULL,
                        'VALMAX' => NULL ];
      }
      $total[$nid]['tosomme'][] = $data['somme'];

      if($with_min_max) {
        $total[$nid]['VALMIN_toSum'][] = $data['VALMIN'];
        $total[$nid]['VALMAX_toSum'][] = $data['VALMAX'];
      }
      // if($nid == 10110) { echo "<hr/>blah"; var_dump($total[$nid]['tosomme'], $data['somme']); echo "<hr/>"; }
    }
  }

  if($with_min_max) {
    array_walk($total, function(&$v) use($count)
               {
                 if(array_search(NULL, $v['VALMIN_toSum'], TRUE) === FALSE) {
                   $v['VALMIN'] = round(array_sum($v['VALMIN_toSum']) / $count, 2);
                 }
                 if(array_search(NULL, $v['VALMAX_toSum'], TRUE) === FALSE) {
                   $v['VALMAX'] = round(array_sum($v['VALMAX_toSum']) / $count, 2);
                 }
               });
  }

  // echo "$count recipes<br/><hr/>" . '<table border="1"><tr style="vertical-align: top"><td>'; var_dump($total);
  array_walk($total, function(&$v) use($count, $approx_plat)
             {
               if(! isset($v['tosomme'])) return;
               if(array_search(NULL, $v['tosomme'], TRUE) !== FALSE) {
                 if(!!($approx_plat & VGDB_NULL_DISCARD)) return NULL;
                 if(! $v['tosomme']) return NULL;

                 // In both of these case below, we are going to do the maths.
                 // so strip the NULL for the equation... but...

                 /* With VGDB_NULL_KEEP, the above NULL will affect the result (counted as 0,
                    but taken into account inside the "/ $count".
                    The global dish' means for this nutrient will thus take into
                    account all recipes ! NULL will only be counted as 0 ! */
                 if(!!($approx_plat & VGDB_NULL_KEEP)) {
                   $v['tosomme'] = array_filter($v['tosomme']);
                 }
                 /* But using VGDB_NULL_IGNORE we assume we can do the maths reliably
                    while just IGNORing this/these recipe(s) for this particular nutrient. */
                 elseif(!!($approx_plat & VGDB_NULL_IGNORE)) {
                   $v['tosomme'] = array_filter($v['tosomme']);
                   $count = count($v['tosomme']);
                 }
               }

               // TODO: Here recette's "notoriété" (market-share) could be used for ponderation
               $v['somme'] = \Ciqual\Component\myeval('(' . implode('+', $v['tosomme']) . ") / $count");
               // echo "somme = " . '(' . implode('+', $v['tosomme']) . ") / $count == {$v['somme']}<br/>";
             });
  // echo '</td><td>'; var_dump($total);die;
  $meta['count'] = $count;
  // $meta['ignored'] = $ignored;
}



function getOrderedIngredientsFromNutrientsArray($oneIndex) {

  array_map(function($v) use(&$sample) { $sample[$v['ORIGFDCD']] = $v['isok'] ? $v['masseabs'] : NULL; return; }, $oneIndex);
  $sum = array_sum($sample);
  array_walk($sample, function (&$v) use($sum) { $v = $v ? ($v * 100 / $sum) : "XXX" ; });
  asort($sample);
  return $sample;
}



// useful shorthand to generate header from statistical array as returned by ComputeRecetteValNut()
function generateNutriHeader($stats_total) {
  return array_map(function($n)
                   {
                     preg_match('!(.*) \((\w+)/100g\)$!u', $n['C_ORIGCPNMABR'], $m);
                     return ['text' => $m[1], 'unit' => $m[2] ];
                   }, $stats_total);
}


function FormatNutVal(&$tab, $opt = NULL) {
  array_walk($tab, function(&$v, $k) use($opt)
             {
               $v = is_null($v) ? "XXX" : (is_numeric($v) ? round($v, 4) : $v);
               // \Ciqual\Component\cpmValIsSpecial($v) ? $v : round($v, 4);
             });
}

function FormatNutValBis(&$tab, $opt = NULL) {
  array_walk($tab, function(&$v, $k) use($opt)
             {
               $v['somme'] = is_null($v['somme']) ? "XXX" : (is_numeric($v['somme']) ? round($v['somme'], 4) : $v['somme']);
             });
}

function reliable_check_kcal($recette_id) {
  $sql_calories_map = RecetteGetNutrimentsByCPNMABR($recette_id, \Ciqual\Component\KCAL_NAME /* kcal/100g */);
  $masse_recette2 = array_sum(array_map(function($n) { return $n['masseabs']; }, $sql_calories_map));

  // $somme_calories = array_sum(array_map(function($n) { return $n['masseabs'] * $n['SELVALtexte'] / 100; }, $sql_calories_map));
  $somme_calories = \Ciqual\Component\sum(\Ciqual\Component\getAllQuantities($sql_calories_map, CIQUAL_CPN_CONSERVE, CIQUAL_CPN_SET0));

  return [ $masse_recette2, $somme_calories, $somme_calories / $masse_recette2 * 100 ];
}
