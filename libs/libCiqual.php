<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

namespace Ciqual {
  class Exception extends \Exception {}
}

namespace Ciqual\Food {
  use PDO;
  require_once(__DIR__ . '/../connect.php');

  function get($ORIGFDCD) {
    global $db, $DBPX;
    return $db->query("SELECT ORIGFDCD, ORIGFDNM, ORIGGPCD FROM {$DBPX}FOOD WHERE ORIGFDCD = $ORIGFDCD")->fetch(PDO::FETCH_ASSOC);
  }


  /*
    Used this way before we relied on CSV header columns names.
    It also could cause trouble on shared-hosting providers */
  /*
    if(! ($s = \Ciqual\Food\importQuick($file, $reset))) {
    list($last_code, $last_message) = [1, "Erreur à l'import du fichier d'aliments"]; 
    } else {
    \vgdb\log\record("Import d'un nouveau fichier d'aliments: effectué");
    list($last_code, $last_message) = [0, sprintf("Import du fichier d'aliments : succès, %d lignes affectées", $s->rowCount()) ];
    // printf("%d lignes affectées : %s\n", $res->rowCount(), $res->rowCount() != 0 ? "OK" : "erreur ?");
    }
  */
  function importQuick($file, $reset = FALSE) {
    global $db, $DBPX;
    if($reset) $db->query("DELETE FROM ${DBPX}FOOD where ORIGFDCD >= 100000");
    return $db->query(<<<EOF
LOAD DATA INFILE "$file" REPLACE INTO TABLE ${DBPX}FOOD
CHARACTER SET utf8 FIELDS TERMINATED BY ';' ENCLOSED BY '"' ESCAPED BY '\\\\' IGNORE 1 LINES
(ORIGGPCD, @unused, ORIGFDCD, ORIGFDNM)
EOF
    );
  }


  function importFromFilePrepare($dfile, &$warn, $force = FALSE) {
    global $db, $DBPX;

    $x = fopen($dfile, 'r');
    if(! $x) throw new \Ciqual\Exception("can't open file");

    $header = fgetcsv($x, 1024 * 4, "\t");

    // check all columns
    if(count($header) < 3) throw new \Ciqual\Exception("Less than 3 columns or invalid format/separator");
    $off_header = ["ORIGGPCD (code groupe)", "ORIGFDCD (code ingrédient)", "ORIGFDNM (nom ingrédient)"];

    // bail-out if one of the expected header is absent
    $diff = array_diff($off_header, array_intersect($header, $off_header));
    if($diff) {
      throw new \Ciqual\Exception("missing columns: {implode(', ', $diff)}");;
    }

    $header = array_flip($header);
    $ORIGGPCDs_valid = array_keys($db->query("SELECT ORIGGPCD FROM ${DBPX}FOOD_GROUPS")
                                     ->fetchAll(PDO::FETCH_UNIQUE));

    // will check that:
    // 1) check all ORIGFDCD are >= 100000
    // 2) check all ORIGGPCD exist
    $stmt = "INSERT INTO {$DBPX}FOOD (ORIGFDCD, ORIGFDNM, ORIGGPCD) VALUES ";
    $stmt_vars = $values = []; $i = 1; $test_new_values = NULL; $test_id = rand(4, 50);
    while( ($d = fgetcsv($x, 1024 * 4, "\t")) ) {
      $ORIGFDCD = $d[$header['ORIGFDCD (code ingrédient)']];
      $ORIGFDNM = $d[$header['ORIGFDNM (nom ingrédient)']];
      $ORIGGPCD = $d[$header['ORIGGPCD (code groupe)']];

      if($i == $test_id) $test_new_values = [ $ORIGFDCD, $ORIGFDNM, $ORIGGPCD ];

      if(! in_array($ORIGGPCD, $ORIGGPCDs_valid)) {
        var_dump($ORIGGPCD);
        fclose($x);
        throw new \Ciqual\Exception("$ORIGGPCD in an invalid group code, line $i");
      }
      if($ORIGFDCD < 100000) {
        fclose($x);
        throw new \Ciqual\Exception("$ORIGFDCD < 100000");
      }
      if(empty($ORIGFDNM)) {
        fclose($x);
        throw new \Ciqual\Exception("\"$ORIGFDNM\" is an invalid name for food n°$ORIGFDCD");
      }

      $stmt_vars[] = '(?, ?, ?)';
      array_push($values, $ORIGFDCD, $ORIGFDNM, $ORIGGPCD);
      $i++;
    }
    fclose($x);

    if($test_new_values) {
      $current_values = $db->query(sprintf("SELECT ORIGFDNM, ORIGGPCD FROM ${DBPX}FOOD WHERE ORIGFDCD = %d",
                                           $test_new_values[0]))->fetch(PDO::FETCH_ASSOC);
      if($current_values) {
        if($current_values['ORIGFDNM'] != $test_new_values[1] ||
           $current_values['ORIGGPCD'] != $test_new_values[2]) {
          $warn1 = "unconsistent update\n" . 
            "previously\t({$current_values['ORIGFDNM']}, {$current_values['ORIGGPCD']}),\nbecame\t\t({$test_new_values[1]}, {$test_new_values[2]}) !!\n";
          if(! $force) {
            throw new \Ciqual\Exception("$warn1");
          }
          else $warn[] = $warn;
        }
        // else test pass !
      }
      // else: this value will be new, can't compare with a previous one
    }

    return [ $stmt, $stmt_vars, $values ];
  }




  function importFromFile($stmt, $stmt_vars, $values, $reset = FALSE) {
    global $db, $DBPX;

    // préparation de l'insertion
    $s = $db->prepare($stmt . ' ' . implode(', ', $stmt_vars));
    $deleted = 0;
    // préparation de la suppression des anciennes données
    if($reset) {
      $s2 = $db->prepare("DELETE FROM ${DBPX}FOOD where ORIGFDCD >= 100000");
      $s2->execute();
      $deleted = $s2->rowCount();
    }

    // insertions proprement dite
    $s->execute($values);
    $added = $s->rowCount();

    return [ $deleted, $added ];
  }
}






namespace Ciqual\NutriData {
  use PDO;
  require_once(__DIR__ . '/../connect.php');

  function importFromFilePrepare($dfile) {
    global $db, $DBPX;

    $x = fopen($dfile, 'r');
    if(! $x) throw new \Ciqual\Exception("can't open file");

    $orig_header = fgetcsv($x, 1024 * 4, "\t");

    // check all columns
    if(! in_array('ORIGFDCD (code ingrédient)', $orig_header)) throw new \Ciqual\Exception("no ORIGFDCD column");

    // check all nutrients exists, perfectly spelled 
    $normal_off_header = \Ciqual\Component\getCPN();
    $off_header = array_flip($normal_off_header);

    $diff = array_diff_key($off_header, array_intersect_key(array_flip($orig_header), $off_header));
    if($diff) {
      $x = implode('", "', array_keys($diff));
      throw new \Ciqual\Exception("missing columns: \"$x\"");
    }

    $header = array_flip($orig_header);

    // will check that:
    // 1) check all ORIGFDCD exist and are >= 100000
    // 2) finally check the request is valid
    $stmt = "INSERT INTO {$DBPX}COMPILED_DATA (ORIGFDCD, ORIGCPCD, SELVALtexte) VALUES ";
    $stmt_vars = $values = [];
    $ORIGFDCDs = [];
    while( ($d = fgetcsv($x, 1024 * 2, "\t")) ) {
      $ORIGFDCD = $d[$header['ORIGFDCD (code ingrédient)']];
      $ORIGFDCDs[] = $ORIGFDCD;

      foreach($normal_off_header as $k => $cpn_str) {
        $SELVALtexte = $d[$header[$cpn_str]];
        // format CSV: "plat vgl", "plat nutrinet", "prioritaire"
        if($SELVALtexte !== "") {
          $stmt_vars[] = '(?, ?, ?)';
          array_push($values, $ORIGFDCD, $k, $SELVALtexte);
        }
        // printf ($ORIGFDCD . " " . $off_header[$header[$index]] . " " . $SELVALtexte . "\n");
      }
    }
    fclose($x);

    // 1) check all ORIGFDCD exist and are >= 100000
    $ids = array_filter(array_filter($ORIGFDCDs,
                                     function ($v) { return is_numeric($v) && $v >= 100000; }));
    // some non-numeric or default (Ciqual) ids
    if(count($ids) != count($ORIGFDCDs)) {
      throw new \Ciqual\Exception("some invalid ID found");
    }

    $counted = $db->query(sprintf(
      "SELECT COUNT(ORIGFDCD) AS c FROM {$DBPX}FOOD WHERE ORIGFDCD IN (%s)",
      implode(',', $ids)
    ))->fetch(PDO::FETCH_ASSOC);

    // some ORIGFDCD are missing for the FOOD table
    if($counted['c'] != count($ORIGFDCDs)) {
      throw new \Ciqual\Exception("some non-existant ID found"); 
    }

    return [ $stmt, $stmt_vars, $values, $ORIGFDCDs ];
  }


  function importFromFile($stmt, $stmt_vars, $values, $fdcd, $reset = FALSE) {
    global $db, $DBPX;

    $deleted = $added = 0;

    // préparation de l'insertion
    $s = $db->prepare($stmt . ' ' . implode(', ', $stmt_vars));
    // préparation de la suppression des anciennes données
    if($reset) {
      $s2 = $db->prepare("DELETE FROM {$DBPX}COMPILED_DATA WHERE ORIGFDCD >= 100000");
    } else {
      $s2 = $db->prepare(sprintf("DELETE FROM {$DBPX}COMPILED_DATA WHERE ORIGFDCD IN (%s)",
                                 implode(',', $fdcd)));
    }

    $s2->execute(); $deleted = $s2->rowCount();

    // insertions proprement dite
    if($values) {
      $s->execute($values);
      $added = $s->rowCount();
    }

    return [ $deleted, $added ];
  }
}

namespace Ciqual\Component {
  use PDO;
  require_once(__DIR__ . '/../connect.php');

  /* Ignore les composants nutritionnels dont la valeurs n'est pas numérique, ou n'est pas
     exact. Exemple: "traces de ..." ou "-" ou "< 5" sont des valeurs qui seront traitées comme
     nulle (0) si CIQUAL_CPN_SET0 est utilisé. */
  define('CIQUAL_CPN_SET0',						1 << 0);
  // on peut aussi conserver l'approximation et la retourner lors de calculs (produits et sommes)
  // Le terme est mal choisi car dans sum() l'addition est tenté ce qui n'est pas vraiment de la
  // "conservation"
  define('CIQUAL_CPN_CONSERVE',				1 << 2);
  // ou enfin avertir en lançant une exception qui permettra de considérer les calculs
  // ultérieurs pour ce qui sont
  define('CIQUAL_CPN_THROW',					1 << 3);
  // usually "15"" sum()ed with "< 5.2" will return "< 20.2" (prépending the approximation sign)
  // but in some calculation, it's preferable to return 15 + < 5.2 (appending the approximated expr')
  define('CIQUAL_CPN_APPEND_APPROX',	1 << 4);
  // TODO: some of the above are absolutely exclusive in certain contexts


  // utilisé pour reconnaître une valeur non-numérique (à faible précision) lors
  // des calculs et utilisations de valeurs agrégées
  define('CIQUAL_CPN_NAN',		"NaN");

  // simply useful
  const KCAL_CODE = 			328;

  // Ciqual v2012
  // const KCAL_NAME = 			"Energie, Règlement UE 1169/2011 (kcal/100g)";
  const KCAL_NAME = 			"Energie, Règlement UE N° 1169/2011 (kcal/100g)";

  // Ciqual v2012 is 57
  // Ciqual v2013 is 58
  const NB_NUTRI  = 			58;

  /* For a given $ORIGFDCD return the nutrients optionnally applying mass normalization.
     $indexed will return the ORIGCPCD as both a value and the array index.
     (there can't be multiple tuple of a given ORIGCPCD associated to a ORIGFDCD and an
     ORIGFDCD can only be given at most NB_NUTRI values for NB_NUTRI distincts nutrients */
  function getAll($ORIGFDCD, $with_names = TRUE, $indexed = FALSE) {
    global $db, $DBPX;

    $mode = PDO::FETCH_ASSOC;
    $select = ['d.ORIGCPCD', 'd.SELVALtexte'];
    $join = '';

    if($indexed) {
      // adds d.ORIGCPCD a second time as the first one will be used
      // as an index in PDO::FETCH_GROUP|PDO::FETCH_UNIQUE mode.
      array_unshift($select, 'd.ORIGCPCD');
      $mode = PDO::FETCH_ASSOC|PDO::FETCH_GROUP|PDO::FETCH_UNIQUE;
    }

    if($with_names) {
      $select[] = 'c.C_ORIGCPNMABR';
      $join = "LEFT JOIN {$DBPX}COMPONENT c ON c.ORIGCPCD = d.ORIGCPCD";
    }

    // ORDER BY ORIGCPCD is a must-have if we do not account on $with_names !
    $nutri = $db->query(sprintf("SELECT %s FROM {$DBPX}COMPILED_DATA d %s WHERE ORIGFDCD = %s ORDER BY ORIGCPCD",
                                implode(',', $select),
                                $join ? : '',
                                $ORIGFDCD))->fetchAll($mode);

    if(count($nutri) != NB_NUTRI) die("error: nb de nutriments != " . NB_NUTRI . " pour $ORIGFDCD");
    return $nutri;
  }

  function cpmValIsSpecial($str) {
    return
      $str !== 0 &&
      ! is_float($str) &&
      ($str == '-' || $str == 'traces' || strpos($str, '<') === 0);
  }

  function cpmSpecialValRound($str, &$margin = FALSE) {
    $margin = FALSE;
    if(! cpmValIsSpecial($str)) return $str;
    if(strpos($str, '<') !== 0) return $str;
    $margin = TRUE;
    // pour les valeurs type "< 0.5"
    return floatval(substr($str, 2));
  }

  /* Spectrum of possible value of SELVALtexte in the
     Ciqual COMPILED_DATA table.
     Will return the "normalized" quantity from:
     - a quantity in g ($val)
     - */
  function divideOne($val,  $coef, $opt = CIQUAL_CPN_SET0) {
    // Dosage de nutriments pour 100g dont X% de cet aliment
    // (représenté par $coef):
    if(is_numeric($val)) return $val * $coef;

    if(!!($opt & CIQUAL_CPN_SET0)) {
      if(cpmValIsSpecial($val)) return 0;
      if(is_null($val) || $val == '-') return 0;
      if(strpos($val, '<', 0) === 0) return 0; // TODO ?
      else goto errdivideOne;
    }

    if(!!($opt & CIQUAL_CPN_CONSERVE)) {
      if(is_null($val) || $val == '-') return $val;
      if($val == 'traces') return 'traces'; //  . ' * ' . $coef;
      $val = str_replace(',', '.', $val); // TODO: fix this during DB fetch
      if(strpos($val, '<', 0) === 0) {
        $val = substr($val, 2) * $coef;
        if($val == 0) return 0;
        return "< " . ($val > 100 ? round($val, 3) : round($val, 5));
      }
      $x = divide_str($val, $coef);
      if($x) return $x;
      // echo "<hr/> From $val to $x<hr/>";

    }

    errdivideOne:
    var_dump($val);
    var_dump(throw_xdebug());
    die('monstruous error: ' . __FILE__ . ':' . __LINE__ . ' : ');

  }

  // Just returns an associative array whose values are quantities
  function divideAll($nutri, $coef, $opt = CIQUAL_CPN_SET0) {
    $ret = array();
    foreach($nutri as $k => $v) $ret[$k] = divideOne($v['SELVALtexte'], $coef, $opt);
    return $ret;
  }

  // same as above but add a key to the existing array of components
  // as it applies the mass normalization of nutrients as returned by getAll()
  function divideAll_r(&$nutri, $coef, $opt = CIQUAL_CPN_SET0) {
    foreach($nutri as &$v) $v['divide'] = divideOne($v['SELVALtexte'], $coef, $opt);
  }

  /*
    Return an array of values representing the absolute quantity of a nutrient
    taking into account the special Ciqual notations.
    = the multiplication of SELVALtexte * masseabs
    +----------+-------------+
    | masseabs | SELVALtexte |
    +----------+-------------+
    |      180 | < 5         |
    |      156 | 25,6        |
    |      144 | 2,2         |
    |       16 | 2,2         |
    |       18 | 0           |
    +----------+-------------+
    SELVALtexte sont les valeurs pour un composant nutritionnel et peuvent
    contenir diverses notations non-numériques.
    Cela peut-être un (int), (float) ou (string).

    En clair: retourne un tableu de valeurs contenant les quantités absolues
    des valeurs nutritionnelles pour chaque ingrédient
    Ex:
    array[0] = "<" . ( 5 / 180 * 100 )	= < 2.77
    array[1] = 25.6		 	 / 156 * 100			= 16.41
    ...
  */
  function getAllQuantities($arr, $opt = CIQUAL_CPN_SET0) {
    $ret = [];
    foreach($arr as $v) $ret[] = divideOne($v['SELVALtexte'], $v['masseabs'] / 100, $opt);
    return $ret;
  }

  /*
    Équivalent à array_sum() prenant en compte les notations d'approximations, séparateurs de décimales
    et autres subtilités liées aux notations Ciqual des quantité de nutriments telles que présentes dans
    COMPILED_DATA.
    Rappel: Si $x représente le tableau de valeurs retourné par : ## \vgdb\CiqualTools\RecetteGetNutrimentsByCPNMABR($rid, "Énergie")
    Alors:
    ## sum(getAllQuantities($x) ## est une version améliorée équivalente à :
    ## array_sum(array_map(function($n) { return $n['masseabs'] * $n['SELVALtexte'] / 100; }, $x))
    TODO: sum(): handles CIQUAL_CPN_CONSERVE in combination with a NULL value !!
  */
  function sum($arr, $opt = CIQUAL_CPN_SET0) {
    $sum = 0; $append = $prepend =  '';
    foreach($arr as $SELVALtexte) {
      if(! cpmValIsSpecial($SELVALtexte)) {
        $sum += $SELVALtexte;
        continue;
      }

      if(!!($opt & CIQUAL_CPN_SET0)) continue;

      $pos = strpos($SELVALtexte, '<', 0);
      if(!!($opt & CIQUAL_CPN_APPEND_APPROX)) {
        if($SELVALtexte == '-') $append .= ' & -';
        elseif($SELVALtexte == 'traces') $append .= ' + traces';
        elseif($pos === 0) $append .= ' + ' . $SELVALtexte;
        else $append .= " strange: <$SELVALtexte>";
        continue;
      }
      // CIQUAL_CPN_CONSERVE ?
      if($pos !== 0) $sum = $sum . ' & ' . $SELVALtexte;
      // pour les valeurs type "< 0.5"
      $prepend = '< ';
      $sum += floatval(substr($SELVALtexte, 2));
    }

    if(!!($opt & CIQUAL_CPN_SET0)) return $sum; // int
    if(!!($opt & CIQUAL_CPN_APPEND_APPROX)) return $sum . $append; // string
    return $prepend . $sum; // string
  }

  // $v1 = 4 + < 2
  // $v2 = 2
  // do $vint = 2 + < 1
  // return: < 3
  function divide_str($v1, $v2 /* int */) {
    /* 
       function ss(&$str, $pos, $l, $new) {
      echo "length = $l<br/> === " . substr($str, 0, $pos) . " ==== " . $new . " ===== " . substr($str, $pos + $l) . "<br/>";
      $str = substr($str, 0, $pos) . $new . substr($str, $pos + $l);
    }
    */

    $newval = $v1;
    // echo "======= divide_str: pre $newval / $v2<br/>";

    /*
    $next_offset = 0;
    split:
    $x = preg_split('/([\s+<])/', $newval, -1, PREG_SPLIT_OFFSET_CAPTURE|PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);
    var_dump($v2, $x);
    $prev = NULL;
    foreach($x as $v) {
      if($v[1] < $next_offset) continue;
      echo "in: $newval ( lors de {$v[0]})<br/>";
      if(is_numeric($v[0])) {
        ss($newval, $v[1], strlen($v[0]), $v[0] / $v2);
        $next_offset = $v[1] + strlen($v[0] / $v2);
        echo "out: $newval ( lors de {$v[0]}), nextoff = $next_offset<br/>";
        goto split;
      }
      if($v[0] == '<') {
        ss($newval, $v[1], 1, '');
        // we are processing '<' which is at char $v[1]
        // so restart here
        $next_offset = $v[1];

        // but if we prepend it (if not already the case)
        if($newval[0] != '<') {
          $newval = '< ' . $newval;
          $next_offset = $v[1] + 2;
        }
        echo "out: $newval ( lors de {$v[0]}), nextoff = $next_offset<br/>";
        goto split;
      }
      echo "out: $newval ( lors de {$v[0]})<br/>";
    }
    var_dump("post", $newval);
    */

    $newval = preg_replace_callback('/\b(\d+(?:\.\d+)?)\b/',
                                    function($m) use($v2) { return $m[1] / $v2; },
                                    $newval);
    do {
      $c1 = $c2 = $c3 = $c4 = 0;
      $newval = preg_replace_callback('/\b(\d+(?:\.\d+)?)\s*([*\/])\s*(\d+(?:\.\d+)?\b)/',
                                      function($m) { if($m[2] == '*') return $m[1] * $m[3]; return $m[1] / $m[3]; },
                                      $newval, -1, $c1);
      $newval = preg_replace_callback('/\b(\d+(?:\.\d+)?)\s*\+\s*(\d+(?:\.\d+)?\b)/',
                                      function($m) { return $m[1] + $m[2]; },
                                      $newval, -1, $c2);
      $newval = preg_replace_callback('/\b(\d+(?:\.\d+)?)\s*\+\s*<\s(\d+(?:\.\d+)?)\b/',
                                      function($m) { /* var_dump("!!!!!", $m[1], $m[2]); */ return "< " . ($m[1] + $m[2]); },
                                      $newval, -1, $c3);
      // var_dump("after adding", $newval);
      $newval = preg_replace('/<\s*</', '<', $newval);
      // var_dump("after replacing", $newval);

      $newval = trim(preg_replace('/(?:[\+&\s]*)traces\s+/', '', $newval, -1, $c4));
      if($c4) $newval .= ' + traces';

      // var_dump("after traces", $newval);
    } while($c1 + $c2 + $c3 + $c4 > 0);

    // var_dump("divide_str: postpost", $newval);
    return $newval;
  }

  function myeval($expr) {
    // echo "premyeval : $expr<br/>";
    require_once(__DIR__ . '/matheval.class.php');
    static $m;
    $m = new \EvalMath;

    $is_traces = $is_lowerThan = $tmp = 0;
    $expr = preg_replace('/\++\s*traces\b/', '', $expr, -1, $is_traces);
    $is_traces = !!($tmp);
    $expr = preg_replace('/\btraces\b/', '0', $expr, -1, $is_traces);
    $is_traces |= !!($tmp);
    $expr = str_replace('<', '', $expr, $is_lowerThan);
    $expr = str_replace('& -', '', $expr);
    //  echo "myeval : $expr<br/>";
    return ($is_lowerThan ? '< ' : '') . $m->evaluate($expr) . ($is_traces ? ' + traces' : '');
  }


  function getCPN() {
    static $cpn;
    if(isset($cpn)) return $cpn;

    global $db, $DBPX;
    $cpn = array_map(function ($v) { return $v['C_ORIGCPNMABR']; },
                     $db->query("SELECT ORIGCPCD, C_ORIGCPNMABR FROM ${DBPX}COMPONENT ORDER BY ORIGCPCD")
                        ->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP|PDO::FETCH_UNIQUE));
    return $cpn;
  }

  function getCPNCode() {
    static $cpncode;
    if(isset($cpncode)) return $cpncode;

    global $db, $DBPX;
    $cpn  = array_map(function ($v) { return $v['ORIGCPCD']; },
                      $db->query("SELECT C_ORIGCPNMABR, ORIGCPCD FROM ${DBPX}COMPONENT ORDER BY ORIGCPCD")
                         ->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP|PDO::FETCH_UNIQUE));
    return $cpn;
  }

  function getFDNM($fdnm) {
    static $fdnms = [];
    if($fdnms &&
       ! is_array($fdnm) &&
       array_key_exists($fdnm, $fdnms)) return $fdnms[$fdnm];

    global $db, $DBPX;

    if(! is_array($fdnm)) {
      $fdnms[$fdnm] = $db->query(sprintf(
        "SELECT ORIGFDNM FROM ${DBPX}FOOD WHERE ORIGFDCD = %d",
        $fdnm))->fetch(PDO::FETCH_ASSOC)['ORIGFDNM'];
      return $fdnms[$fdnm];
    }

    $int = array_intersect_key($fdnms, array_flip($fdnm));
    if(count($int) == count($fdnm)) return $int;

    $fdnm_num = array_filter(array_filter($fdnm, 'is_numeric'));

    $new_fdnm  = array_map(function ($v) { return $v['ORIGFDNM']; },
                           $db->query(sprintf("SELECT ORIGFDCD, ORIGFDNM FROM ${DBPX}FOOD WHERE ORIGFDCD IN (%s)", implode(',', $fdnm_num)))
                              ->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP|PDO::FETCH_UNIQUE));

    $fdnms = $fdnms + $new_fdnm;
    return $new_fdnm;
  }
}

namespace {

  function import_csv($data, &$id_recette, $opt = IMPORT_ALL) {
  }

  function makedump() {
    require_once(__DIR__ . "/vgdb-export.php");
    require_once(__DIR__ . "/vgdb-export.class.php");

    $export = new VgdbExport();
    $export->setFormat(\vgdb\export\FMT_CSV
                       , \vgdb\export\OUT_SAVE,
                       [ 'strict' => FALSE, 'valnut' => TRUE, 'rangenut' => FALSE ]);

    $entete_line = VgdbExport::genHeader(NULL);
    $export->setLocal();
    $export->setOutputFile(realpath(__DIR__ . '/../dumps/') . '/' . $export->getFileName());
    $export->begin($entete_line);

    require_once("libs/libvgdb-ciqual.php");

    foreach($export->getPlats() as $p) {
      foreach(\vgdb\Recette\getFromPlatID($p['id']) as $r) {
        \vgdb\export\_export_recette2($p['id'], $p['nom_vegetalise'], $p['nom_traditionnel'], $export, $r);
        $export->changeBGColor();
      }
    }
    return 'http://' . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '') . '/' . rtrim(MY_CONST_PATH . '/', '/') . $export->end(FALSE, TRUE);
  }


  function makedump_old() {

    $s = curl_init();

    // we may have neither of them (eg: OVH and its special Autorization tricks).
    curl_setopt($s, CURLOPT_URL, "http://" . $_SERVER['SERVER_NAME'] . '/vgdb/export.php?out=save&fmt=csv&fmtopt=-strict');
    if(isset($_SERVER['PHP_AUTH_USER'])) {
      $user = $_SERVER['PHP_AUTH_USER'];
      $pass = $_SERVER['PHP_AUTH_PW'];
    } else {
      $user = $_SERVER['REMOTE_USER'];
      $pass = $_SERVER['PHP_AUTH_PW'];
    }
    if(! $_SERVER['PHP_AUTH_PW']) {
      // TODO
    }

    curl_setopt($s, CURLOPT_USERPWD, $user . ':' . $pass);
    curl_setopt($s, CURLOPT_HEADER, true);
    curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
    $ret = curl_exec($s);
    $status = curl_getinfo($s,CURLINFO_HTTP_CODE);
    if($status != 200) return FALSE;
    preg_match('/^X-VGDB-LAST-DUMP: (.*)/m', $ret, $x);
    // TODO, depends upon output-buffering (die; vs CSV output in export.php) 
    if(preg_match('/(.*ERROR.*)/m', $ret, $err)) {
      throw new \Ciqual\Exception($err[1]);
    }
    if(! isset($x[1]) || ! $x[1]) {
      throw new \Ciqual\Exception("unknown error while saving");
    }
    return $x[1];
  }
}
