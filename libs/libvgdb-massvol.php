<?php

namespace vgdb\massvol;

require_once(__DIR__ . '/../connect.php');
require_once("libs/libvgdb-sys.php");
use PDO;



function import($filename = NULL, $reset = FALSE, $filter_recipes = FALSe) {
  global $db, $DBPX;

  $x = json_decode(file_get_contents($filename), TRUE);
  if(! $x) return NULL;


  $existing_recettes = [];
  if($filter_recipes) {  
    $recettes = array_unique(array_filter(array_filter(array_map(function ($n) { return $n['id_recette']; }, $x), 'is_numeric')));
    if(!$recettes) return NULL; // no valid recipes.
    $existing_recettes = array_keys($db->query(sprintf("SELECT id FROM {$DBPX}recette WHERE id IN (%s)", implode(',', $recettes)))->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE));
  }

  $origfdcd = array_unique(array_filter(array_filter(array_map(function ($n) { return $n['ORIGFDCD']; }, $x), 'is_numeric')));
  if(! $origfdcd) return NULL;
  $existing_origfdcd = array_keys($db->query(sprintf("SELECT ORIGFDCD FROM {$DBPX}FOOD WHERE ORIGFDCD IN (%s)", implode(',', $origfdcd)))->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE));

  $valid_x = array_filter($x, function ($e) use($existing_recettes, $existing_origfdcd)
                          { return
                            ( ! $existing_recettes || in_array($e['id_recette'], $existing_recettes) ) && in_array($e['ORIGFDCD'], $existing_origfdcd); 
                          });

  $q = "INSERT INTO {$DBPX}masvol (ORIGFDCD, masvol) VALUES %s";

  if($reset) $db->query("DELETE FROM {$DBPX}masvol");
  else $q .= " ON DUPLICATE KEY UPDATE masvol = VALUES(masvol)";

  $placeholders = 0; $values = []; $traces_uniques = [];

  foreach($valid_x as $mv) {
    if(isset($traces_uniques[$mv['ORIGFDCD']])) continue;
    $placeholders++;
    $values[] = $mv['ORIGFDCD'];
    $values[] = $mv['masvol'];
    $traces_uniques[$mv['ORIGFDCD']] = true;
  }

  if(!$placeholders) return NULL;
  $s = $db->prepare(sprintf($q, implode(', ', array_fill(0, $placeholders, '(?, ?)'))));
  $s->execute($values);
  return [ $s ? 0 : 1, $s->rowCount()];
}


function export($outfile, $filename = NULL) {
  global $db, $DBPX;

  if($outfile && ! $filename) {
    list($last_code, $last_message) = [1, "Ne peut exporter: problème de fichier de configuration: 'export.masvol'"];
    return FALSE;
  }

  // pas de "%s" dans les noms de dossier: realpath offre la possibilité de chemins relatifs
  // mais impose la pré-existence des dossiers
  $absfilename = realpath(dirname($filename)) . DIRECTORY_SEPARATOR . sprintf(basename($filename), time());
  $sql = <<<EOF
SELECT "ORIGFDCD", "masvol", "ORIGFDNM", "id_recette", "recette_nom" UNION ALL
SELECT c.ORIGFDCD, m.masvol, F.ORIGFDNM, c.id_recette, r.nom
    FROM {$DBPX}composition c
    INNER JOIN {$DBPX}FOOD F ON (c.ORIGFDCD = F.ORIGFDCD)
    INNER JOIN {$DBPX}recette r ON (c.id_recette = r.id)
    LEFT JOIN {$DBPX}masvol m ON (c.ORIGFDCD = m.ORIGFDCD)
    WHERE unite != 'g'
%s
EOF;
  
  if(! $outfile) {
    $sql = sprintf($sql, '');
    if(PHP_SAPI != 'cli') {
      header('Content-type: application/json');
      header('Content-Disposition: attachment; filename="massvol.json"');
    }
    print json_encode($db->query($sql)->fetchAll(PDO::FETCH_ASSOC));
    die;
  }

  $sql = sprintf($sql, <<<EOF
INTO OUTFILE "$absfilename"
 FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
 LINES TERMINATED BY "\n"
EOF
  );
  if(! $db->query($sql)) {
    return [1, "L'export des masses volumiques à échoué'"];
  }
  else {
    $uriname = \vgdb\sys\path2uri($absfilename);
    return [0, sprintf('Export des masses volumiques effectué : fichier <a href="%s">%s</a>', $uriname, $absfilename) ];
  }
}

function purge($ingr_volume) {
  global $db, $DBPX;

  $ORIGFDCDs = array_map(function ($v) { return $v['ORIGFDCD']; }, $ingr_volume);
  if($ORIGFDCDs) {
    $s = $db->prepare(sprintf("DELETE FROM {$DBPX}masvol WHERE ORIGFDCD NOT IN (%s)",
                              implode(',', $ORIGFDCDs)));
    $s->execute(); $x = $s->rowCount();
    if($x) return [0, sprintf("%d masses volumiques inutilisées ont été supprimées", $x) ];
    return [0, "Rien à faire" ];
  }
  return [0, "Rien à faire" ];
}
