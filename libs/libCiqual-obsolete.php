<?php

namespace Ciqual\ObsoleteComponent;

function getAllQuantities_r(&$arr, $opt = CIQUAL_CPN_SET0) {
  foreach($arr as &$v) $v['quantity'] = Ciqual\Component\divideOne($v['SELVALtexte'], $v['masseabs'] / 100, $opt);
}

function sum2($base, $val) {
  if(is_int($base) || is_float($base)) {
    if(is_string($val)) {
      if(strpos($val, '<') >= 1) {
        return $base + rtrim(substr($val, 0, strpos($val, '<')), '+ ')
          . ' + ' . trim(substr($val, strpos($val, '<')));
      }
      else {
        return $base . ' ' . $val;
      }
    }
    return $base + $val;
  }
  else { // base = 33.4 + < 0.5
    if(is_string($val)) return $base . ' ' . $val;
    return substr($base, 0, strpos($base, ' ')) + $val .
      ' ' . trim(substr($base, strpos($base, ' ')));
  }
}


function divide2($base, $val /* int */) {
  if(is_int($base) || is_float($base)) {
    return $base / $val;
  }
  else { // base = 33.4 + < 0.5
    return substr($base, 0, strpos($base, ' ')) / $val .
      ' + (' . trim(substr($base, strpos($base, ' +'))) . " / $val)";
  }
}


class CP {
  var $numval = NULL;
  var $prefix = '';
  var $suffix = '';

  function CP($numval, $prefix, $suffix) {
    $this->numval = $numval;
    $this->prefix = $prefix;
    $this->suffix = $suffix;
  }
  static function sum(CP $v1, CP $v2) {
    return new CP($v1->numval + $v2->numval,
                  trim($v1->prefix . ' ' . $v2->prefix),
                  trim($v1->suffix . ' ' . $v2->suffix));
  }
  function get() {
    return trim($this->prefix . ' ' . $this->numval . ' ' . $this->suffix);
  }
}
