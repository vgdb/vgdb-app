<?php

class VgdbExport {

  public static $htmlcolors = [
    "AliceBlue", "DarkOliveGreen", /*"Indigo",*/ "MediumPurple", /*"Purple",*/
    "AntiqueWhite", "DarkOrange", "Ivory", "MediumSeaGreen", "Red",
    "Aqua", "DarkOrchid", "Khaki", "MediumSlateBlue", "RosyBrown",
    "AquaMarine", /*"DarkRed",*/ "Lavender", "MediumSpringGreen", "RoyalBlue",
    "Azure", "DarkSalmon", "LavenderBlush", "MediumTurquoise", /*"SaddleBrown",*/
    "Beige", "DarkSeaGreen", "LawnGreen", "MediumVioletRed", "Salmon",
    "Bisque", /*"DarkSlateBlue",*/ "LemonChiffon", /*"MidnightBlue",*/ "SandyBrown",
    /*"Black",*/ /*"DarkSlateGray",*/ "LightBlue", "MintCream", "SeaGreen",
    "BlanchedAlmond", "DarkTurquoise", "LightCoral", "MistyRose", "SeaShell",
    /*"Blue",*/ "DarkViolet", "LightCyan", "Moccasin", "Sienna",
    /*"BlueViolet",*/ "DeepPink", "LightGoldenrodYellow", "NavajoWhite", "Silver",
    /*"Brown",*/ "DeepSkyBlue", "LightGray", /*"Navy",*/ "SkyBlue",
    "BurlyWood", /*"DimGray",*/ "LightGreen", "OldLace", "SlateBlue",
    "CadetBlue", "DodgerBlue", "LightPink", "Olive", "SlateGray",
    "Chartreuse", "FireBrick", "LightSalmon", "OliveDrab", "Snow",
    "Chocolate", "FloralWhite", "LightSeaGreen", "Orange", "SpringGreen",
    "Coral", "ForestGreen", "LightSkyBlue", "OrangeRed", "SteelBlue",
    "CornFlowerBlue", "Fuchsia", "LightSlateGray", "Orchid", "Tan",
    "Cornsilk", "Gainsboro", "LightSteelBlue", "PaleGoldenRod", "Teal",
    "Crimson", "GhostWhite", "LightYellow", "PaleGreen", "Thistle",
    "Cyan", "Gold", "Lime", "PaleTurquoise", "Tomato",
    /*"DarkBlue",*/ "GoldenRod", "LimeGreen", "PaleVioletRed", "Turquoise",
    "DarkCyan", "Gray", "Linen", "PapayaWhip", "Violet",
    "DarkGoldenRod", "Green", "Magenta", "PeachPuff", "Wheat",
    "DarkGray", "GreenYellow", "Maroon", "Peru", "White",
    "DarkGreen", "HoneyDew", "MediumAquaMarine", "Pink", "WhiteSmoke",
    "DarkKhaki", "HotPink", /*"MediumBlue",*/ "Plum", "Yellow",
    /*"DarkMagenta",*/ "IndianRed", "MediumOrchid", "PowderBlue", "YellowGreen"
  ];

  // internal table printing system
  private $cur_table_bg = NULL;
  private $htmlcol = 0;

  // formatting option
  public $is_table = FALSE;
  private $fmt = FALSE;

  // output options
  private $csv_filename = "php://output";
  private $csv_handler = NULL;
  private $is_local = FALSE;
  private $out = NULL;

  // TODO: non-static class attribute
  static public $fmtopts = [
    // should we bail-out on common error
    // (eg: unknown density for one recipe's ingredient)
    'strict' => TRUE,
    // should we display nutritionnal values or 
    // only export recipes content, but nothing related to nutrients
    'valnut' => TRUE,
    // should we display nutritionnal ranges or not (VALMIN, VALMAX, N)
    'rangenut' => FALSE
  ];

  // filtering option
  public $filters = [
    'filter-recette' => NULL,
    'filter-plat' => NULL
  ];



  public function VgdbExport() {
    // shuffle au cas ou l'entrée qui nous intéresse
    // tombe sur un background pas très lisible
    shuffle(self::$htmlcolors);
  }

  public function setFormat($fmt, $out, $fmtopts) {
    $this->fmt = $fmt;
    $this->out = $out;
    if($fmt == \vgdb\export\FMT_TABLE) $this->is_table = TRUE;
    $o = array_intersect_key($fmtopts, self::$fmtopts);
    self::$fmtopts = array_merge(self::$fmtopts, $o);
  }

  public function setLocal() {
    $this->is_local = TRUE;
  }

  public function setOutputFile($filename) {
    $this->csv_filename = $filename;
  }

  /* ['prio_only' => .
     'filter-plat' => .
     'filter-recette' => . ] */
  public function setFilter($array) {
    $this->filters = $array;
    if(! $this->filters['prio']) unset($this->filters['prio']);
  }

  public function getFileName() {
    return sprintf('dump-uren%s%s-%d.csv',
                   $this->filters['filter-plat'] ? ('-plat:' . implode(',', $this->filters['filter-plat'])) : '',
                   $this->filters['filter-recette'] ? ('-recette:' . implode(',', $this->filters['filter-recette'])) : '',
                   time());
  }

  // (highly variable) header definition
  static function genHeader($agr = NULL) {
    $entete_line = [ "Nom végétalisé", "Nom nutrinet" ];
    if($agr == \vgdb\export\AGR_LVL_RECETTE || is_null($agr)) array_push($entete_line, "Nom de recette");
    if($agr == \vgdb\export\AGR_LVL_RECETTE) array_push($entete_line, "Nombre d'ingrédients");
    if($agr == \vgdb\export\AGR_LVL_PLAT) array_push($entete_line, "Nombre de recettes");

    if(is_null($agr)) array_push($entete_line, "Code de l'ingrédient", "Nom de l'ingrédient", "% de la masse de la recette");

    if(VgdbExport::opt('valnut')) {
      $nutriments_intitules = \Ciqual\Component\getCPN();
      if(VgdbExport::opt('rangenut')) $nutriments_intitules = \vgdb\export\insert_minmax($nutriments_intitules, is_null($agr));
      $entete_line = array_merge($entete_line, $nutriments_intitules);
    }

    return $entete_line;
  }


  public function begin($header) {
    if($this->out == \vgdb\export\OUT_DOWNLOAD) {
      if($this->is_table) header('Content-type: text/html');
      else header('Content-type: text/csv');
      header('Content-Disposition: attachment; filename="' . $this->getFileName() . '"');
    }

    // CSV on webpage: add a <pre>
    if($this->fmt == \vgdb\export\FMT_CSV && ! $this->out && PHP_SAPI !== 'cli') echo "<pre>\n";

    if($this->fmt == \vgdb\export\FMT_CSV) {
      $this->csv_handler = fopen($this->csv_filename, 'w');
      if(! $this->csv_handler) die("ERROR: can't save into {$this->csv_filename}");
      fputcsv($this->csv_handler, $header, "\t");
      return;
    }

    // table formatting
    // too many rows to see the thead ?
    // https://bugs.webkit.org/show_bug.cgi?id=17205
    printf(<<<EOF
<!doctype html>
<html>
<head><title>vgdb: Export</title></head>
<body>
<table border='1' width='100%%'>
<col style="width: 30%%">
<col style="width: 40%%">
<col style="width: 10%%">
<thead><tr><th>%s</th></tr></thead>
<tbody>

EOF
           , implode("</th><th>", $header));
  }

  public function getPlats() {
    require_once(__DIR__ . '/libvgdb.php');

    $ids_filters = [];
    if($this->filters['filter-plat']) $ids_filters[] = 'p.id IN (' . implode(',', $this->filters['filter-plat']) . ')';
    if($this->filters['filter-recette']) $ids_filters[] = 'r.id IN (' . implode(',', $this->filters['filter-recette']) . ')';
    return \vgdb\Plat\getAllFiltered($this->filters, $ids_filters);
  }

  public function addTableLine($array) {
    printf("<tr style=\"background: %s\">\n", self::$htmlcolors[$this->htmlcol % count(self::$htmlcolors)]);
    print("<td>" . implode("</td><td>", $array) . '</td>');
    print("</tr>\n");
  }

  public function changeBGColor() {
    $this->htmlcol ++;
  }

  public function addCSVLine($array) {
    fputcsv($this->csv_handler, $array, "\t");
  }

  static function opt($option) {
    return self::$fmtopts[$option];
  }

  public function end($verbose = TRUE, $ret = FALSE) {
    if($this->is_table) {
      die(<<<EOF
</tbody></table>
<p><em>Using the Ciqual French food composition table version 2013<br/>
Calculs basés sur la table de composition nutritionnelle des aliments Ciqual 2013</em></p>
<p>
<em>© L214 et contributeurs</em><br/>
<img src="http://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
</p>
</body></html>
EOF
      );
    }

    if(fclose($this->csv_handler)) {
      if($this->is_local) {
        require_once(__DIR__ . '/libvgdb-sys.php');
        vgdb\log\record("Export effectué dans " . basename($this->csv_filename));
        header("X-VGDB-LAST-DUMP: /vgdb/dumps/" . basename($this->csv_filename));
        if($verbose) {
          printf('La sauvegarde à été effectuée avec succès. Le fichier CSV résultant est nommé <u>%s</u> et peut être téléchargé en cliquant <a href="http://%s">ici</a>.',
                 basename($this->csv_filename),
                 (isset($_SERVER['HTTP_HOST']) ? : '') .
                 '/' . MY_CONST_PATH . '/dumps/' . basename($this->csv_filename));
        }
      }

      // CSV on webpage: add a <pre>
      if($this->fmt == \vgdb\export\FMT_CSV && ! $this->out && PHP_SAPI !== 'cli') echo "<pre>\n";
      if($ret && $this->is_local) return '/dumps/' . basename($this->csv_filename);
      die;
    }
    else {
      die('I/O ERROR');
    }
  }
}
