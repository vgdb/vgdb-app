<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

namespace vgdb\scripts {
  use PDO;
  require_once(__DIR__ . '/../connect.php');

  define('OMNI_FOOD', serialize(['05.1', '05.1', '05.2', '05.3', '05.4', '05.5', '05.7', '05.8',
				 '07', '08.1', '08.2', '09', '10', '11', '12', '25.1', '25.2']));
  define('OMNI_FOOD_GLOBS', serialize(['06.*', '13.*', '14.*']));


  function import_plats($ifile = NULL) {
    require_once('libvgdb.php');
    // TODO: pre-delete ?
    // TODO: make save dump ?
    if(!$ifile) $ifile = DATADIR . '/in/liste_plats.csv';

    try {
      $warn = [];
      list($stmt, $stmt_vars, $values) = \vgdb\Plat\importFromFilePrepare($ifile, $warn);
    } catch (\vgdb\Exception $e) {
      fprintf(STDERR, "error: can't import dishes: {$e->getMessage()}\n");
      return FALSE;
    }

    if(! $stmt_vars) die("can't import dishes (nothing to import)");
    list($deleted, $added) = \vgdb\Plat\importFromFile($stmt, $stmt_vars, $values);
    printf("%d plats retirés, %d plats insérés.\n", $deleted, $added);
  }

  function import_cpn($ifile = NULL) {
    require_once('libCiqual.php');
    require_once('libvgdb-sys.php');

    // TODO: pre-delete ?
    // TODO: make save dump ?
    if(!$ifile) $ifile = DATADIR . '/in/COMPILED_DATA-vgdb.csv';

    try {
      list($stmt, $stmt_vars, $values, $fdcd) = \Ciqual\NutriData\importFromFilePrepare($ifile);
    } catch (\Ciqual\Exception $e) {
      printf(STDERR, "error: can't import nutrients: {$e->getMessage()}\n");
      return FALSE;
    }

    list($deleted, $added) = \Ciqual\NutriData\importFromFile($stmt, $stmt_vars, $values, $fdcd);
    \vgdb\log\record("Import de nutriments ($deleted valeurs nutritionnelles supprimées)");
    printf("%d valeur nutritionnelles supprimées, %d ajoutées pour %d aliments.\n", $deleted, $added, count($fdcd));
  }


  // php nutrinet_gen_plats_input.php > ../templates/plats-select.html
  function gen_plats($ifile = NULL, $ofile = NULL) {
    global $db, $DBPX;

    if(!$ifile) $ifile = DATADIR . '/in/noms-plats-nutrinet.txt';
    if(!$ofile) $ofile = TPLDIR . '/plats-select.html';

    $h = fopen($ifile, 'r');

    $html = '<select name="nom_nutrinet[]" id="nom-nutrinet" multiple="multiple">'
      . "\n"
      . '<option value="" selected="selected">Aucun</option>'
      . "\n";

    // wc -L data/in/noms-plats-nutrinet.txt
    while( ($f = fgets($h)) ) {
      $html .= sprintf('<option value="%s">%s</option>' . "\n",
		       addcslashes(trim($f), '"'), trim($f));
    }

    fclose($h);
    $html .= '</select>' . "\n";
    file_put_contents($ofile, $html);
  }


  // php ciqual_gen_group_input.php > templates/group-select.html
  function gen_groups($ofile = NULL) {
    global $db, $DBPX;

    if(!$ofile) $ofile = TPLDIR . '/group-select.html';

    $html = '<select name="groups" id="groups">' . "\n";
    // way 1:
    /*
      foreach($db->query("SELECT ORIGGPCD, ORIGGPFR FROM {$DBPX}FOOD_GROUPS") as $cat) {
      echo '<option value="' . $cat['ORIGGPCD'] . '">' . $cat['ORIGGPFR'] . '</option>' . "\n";
      }
    */

    // way 2: (group'ed <option>)
    $q = sprintf(<<<EOF
SELECT g.ORIGGPCD, g.ORIGGPFR, COUNT(ORIGFDNM) AS c
FROM {$DBPX}FOOD_GROUPS g LEFT JOIN {$DBPX}FOOD f ON f.ORIGGPCD = g.ORIGGPCD
WHERE g.ORIGGPCD NOT IN (%s)
AND g.ORIGGPCD NOT REGEXP %s
GROUP BY g.ORIGGPCD
ORDER BY CAST(g.ORIGGPCD AS DECIMAL(5,2));
EOF
		 ,
		 implode(',', array_map([$db, "quote"], unserialize(OMNI_FOOD))),
		 $db->quote('^(' . implode('|', unserialize(OMNI_FOOD_GLOBS)) . ')$') );

    fprintf(STDERR, "Running:\n$q\n");

    $optgroup = NULL;
    $groups = [];
    $subgroups = [];
    $countgroups = [];
    $counttotgroups = [];
    foreach($db->query($q, PDO::FETCH_ASSOC) as $cat) {
      $is_a_group = strpos($cat['ORIGGPCD'], '.') === FALSE;
      if($cat['c'] === 0 && ! $is_a_group) continue;

      if($is_a_group) {
        $groups[$cat['ORIGGPCD']] = $cat['ORIGGPFR'];
        @$counttotgroups[$cat['ORIGGPCD']] += $cat['c'];
        @$countgroups[$cat['ORIGGPCD']] = $cat['c'];
      } else {
        $upgroup = substr($cat['ORIGGPCD'], 0, strpos($cat['ORIGGPCD'], '.'));
        if(! isset($groups[$upgroup])) $groups[$upgroup] = '';
        $subgroups[$upgroup][] = [ $cat['ORIGGPCD'], $cat['ORIGGPFR'] ];
        $counttotgroups[$cat['ORIGGPCD']] = $cat['c'];
        $counttotgroups[$upgroup] += $cat['c'];
      }
    }

    //var_dump($groups);die;
    foreach($groups as $k => $v) {
      if($counttotgroups[$k] == 0) continue;
      $html .= '<optgroup label="' . htmlentities($v) . '">' . "<!-- $k -->" . PHP_EOL;
      if($countgroups[$k]) {
        $html .= '<option value="' . $k . '">' . $v . '</option>' . "<!-- n:{$countgroups[$k]}:{$counttotgroups[$k]} -->" . PHP_EOL;
      }
      if(isset($subgroups[$k])) {
        foreach($subgroups[$k] as $k2 => $v2) {
          if($counttotgroups[$v2[0]] == 0) continue;
          $html .= '<option value="' . $v2[0] . '">' . $v2[1] . '</option>' . PHP_EOL;
        }
      }
      $html .= '</optgroup>' . PHP_EOL;
    }

    /*
      foreach($db->query($q) as $cat) {
      if($cat['c'] === 0) {
        // un "group" d'<option> est ouvert ? on le referme
        if(! is_null($optgroup)) {
          $html .= '</optgroup>' . "\n";
          $optgroup = NULL;
        }
        // et on ouvre le nouveau
        $html .= '<optgroup label="' . $cat['ORIGGPFR'] . '">' . "\n";
        $optgroup = $cat['ORIGGPCD'];
        continue;
      }

      // un optgroup est ouvert et l'option n'a pas le même préfix ?
      // alors on ferme cet optgroup.
      if($optgroup && $is_a_group) {
        $html .= '</optgroup>' . "\n";
        $optgroup = NULL;
      }
      $html .= '<option value="' . $cat['ORIGGPCD'] . '">' . $cat['ORIGGPFR'] . '</option>' . "\n";       
    }
    */

    $html .= '</select>' . PHP_EOL;
    file_put_contents($ofile, $html);
  }


  function gen_alim($ofile = NULL) {
    global $db, $DBPX;

    if(!$ofile) $ofile = DATADIR . '/gen/ciqual.json';
    $tab1 = $db
      ->query(sprintf(<<<EOF
SELECT G.ORIGGPCD, G.ORIGGPFR, ORIGFDCD, ORIGFDNM
FROM {$DBPX}FOOD_GROUPS G
LEFT JOIN {$DBPX}FOOD F ON G.ORIGGPCD = F.ORIGGPCD
WHERE G.ORIGGPCD NOT IN (%s)
AND G.ORIGGPCD NOT REGEXP %s
EOF
			       ,
			       implode(',', array_map([$db, "quote"], unserialize(OMNI_FOOD))),
			       $db->quote('^(' . implode('|', unserialize(OMNI_FOOD_GLOBS)) . ')$') ))
      ->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP);
    $tab = array_map(
      function($t1)
      {
        // pour chaque G.ORIGGPCD, tous les ORIGGPFR sont les mêmes
        // par contre les aliments sont différents... s'ils existent
        // (car LEFT JOIN retourne des ORIGFD?? NULL pour les groupes sans ingrédients
        $elems = array_filter(array_map(function($t2) { return array_filter([ $t2['ORIGFDCD'], $t2['ORIGFDNM'] ]) ? : NULL; }, $t1));
        return [ $t1[0]['ORIGGPFR'], $elems ];
      },
      $tab1);
    if(file_exists($ofile)) {
      rename($ofile, $ofile . '-' . time());
    }

    file_put_contents($ofile, json_encode($tab));

    $req = str_replace([ 'fulltxt', 'FOOD' ],
                       [ $DBPX . 'fulltxt', $DBPX . 'FOOD' ],
                       file_get_contents(DATADIR . '/sql/completion_gen_table.sql'));
    $x = $db->prepare($req);
    $x->execute();
  }
}
