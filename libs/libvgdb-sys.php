<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

namespace vgdb\log {
  function record($action = '') {
    if(PHP_SAPI === 'cli') { fprintf(STDERR, $action . "\n"); return; }
    $h = fopen(TMPDIR . '/LASTLOG', 'a');
    fwrite($h,
           time() . ':' .
           (isset($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'] : $_SERVER['REMOTE_ADDR']) . ':' .
           $action . "\n");
    fclose($h);
  }

  function htmllinks($str) {
    $str = preg_replace('/(\bplat\b.*?(\d+))/', '<a href="recettes.php?plat_id=\2">\1</a>', $str);
    $str = preg_replace('/(\brecette\b.*?(\d+))/', '<a href="ingredients.php?recette_id=\2">\1</a>', $str);
    return $str;
  }

  function show() {
    $data = explode("\n", file_get_contents(TMPDIR . '/LASTLOG'));
    $entries = array_filter(array_reverse($data), function($v) { return ($v && strpos($v, ':') !== FALSE); });
    $last_entry = array_shift($entries);
    list($t, $w, $a) = explode(':', $last_entry, 4);

    // check par IP ou htaccess
    $vous = $w && (
      (isset($_SERVER['REMOTE_USER']) && $w == $_SERVER['REMOTE_USER'])
      ||
      (strpos($w, '.', 0) !== FALSE && $w == $_SERVER['REMOTE_ADDR']));

    return sprintf('Dernière modification il y a <span class="time">%s</span> par %s%s.',
                   $t ? humanTiming($t) : '[longtemps?]',
                   $vous ? "vous" : "<span class=\"contributeur\">$w</span>",
                   $a ? ' : <span class="action">' . htmllinks($a) . '</span>' : ''
    );
  }

  function getFullLog() {
    $data = explode("\n", file_get_contents(TMPDIR . '/LASTLOG'));
    $entries = array_filter(array_reverse($data), function($v) { return ($v && strpos($v, ':') !== FALSE); });
    $ret = [];
    foreach($entries as $entry) {
      list($t, $w, $a) = explode(':', $entry, 4);
      $ret[] = [ humanTiming($t), $w, htmllinks($a) ];
    }
    return $ret;
  }

  // http://stackoverflow.com/questions/2915864/php-how-to-find-the-time-elapsed-since-a-date-time
  function humanTiming ($time) {
    $time = time() - $time; // to get the time since that moment
    $tokens = [ 31536000 => 'année', 2592000 => 'mois', 604800 => 'semaine',
		86400 => 'jour', 3600 => 'heure', 60 => 'minute', 1 => 'seconde' ];

    foreach ($tokens as $unit => $text) {
      if ($time < $unit) continue;
      $numberOfUnits = floor($time / $unit);
      return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }
  }
}

namespace vgdb\users {
  function getAll() {
    preg_match_all('/^(\w+):.*/m', @file_get_contents('.htpasswd'), $x);
    return @$x[1];
  }

  /**
     1) OVH forbids
     exec("htpasswd -mb .htpasswd " . escapeshellarg($user) . " " . escapeshellarg($pass));
   
     2) Apache md5 implementation is very specific
   
     En conséquence, on utilise sha1() puisque les 2 lignes commandes suivantes sont équivalentes:
     $ htpasswd -sb .htpasswd n k
     $ php -r 'echo "n:{SHA}" . base64_encode(sha1("k", 1));'
  */
  function add($file, $user, $pass) {
    if(! preg_match('/^[a-z][a-z0-9]+$/', $user) ) return [ 1 , "Nom d'utilisateur incorrect" ];
    if(! ($c = file_get_contents($file)) ) return [ 1, "Problème d'accès au fichier d'utilisateurs" ];

    $new_string = sprintf("%s:{SHA}%s", $user, base64_encode(sha1($pass, 1)));

    // mise à jour pass d'utilisateur existant
    if(preg_match('/^' . preg_quote($user) . ':/m', $c)) {
      $ret = file_put_contents(".htpasswd", preg_replace('/^' . preg_quote($user) . ':.*/m', $new_string, $c));
      if($ret > 0) {
        \vgdb\log\record("Utilisateur mis à jour: \"$user\"");
        return [ 0, "Mot de passe utilisateur mis à jour" ];
      }
      return [ 1, "Erreur à la mise à jour de l'utilisateur" ];
    }
    // ajout nouvel utilisateur
    $h = fopen($file, "a+");
    $ret = fwrite($h, $new_string . "\n");
    fclose($h);

    if($ret > 0) {
      \vgdb\log\record("Utilisateur ajouté: \"$user\"");
      return [ 0, "Nouvel utilisateur ajouté" ];
    }
    return [ 1, "Erreur à l'inscription du nouvel utilisateur" ];
  }
}

namespace vgdb\sys {
  function deb() {
    // echo func_get_args()[0] . PHP_EOL;
  }

  function path2uri($path) {
    $uri_path = $_SERVER['SCRIPT_NAME'];
    if($uri_path == '-') return FALSE; // cli
    $left_over = [];
    deb("<pre>");

      
    while(true) {
      // si le répertoire du script n'est pas présent dans le répertoire du fichier
      // on strip le chemin du script
      if($uri_path != '/' && $uri_path != '.' && strpos($path, basename($uri_path)) === false) {
        deb("strip " . basename($uri_path) . " d'URI");
        $uri_path = dirname($uri_path);
        continue;
      }
      // bien: la totalité de l'URI a été trouvé dans le chemin:
      // et l'extrémité du chemin se retrouve dans l'URI
      deb("======= cmp $uri_path     AVEC $path");
      if(($x = strrpos($path,$uri_path)) !== FALSE && strrpos($uri_path, basename($path)) !== FALSE) {
        deb("$x !== FALSE && $uri_path IN basename($path)");
        $href = substr($path, $x) . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $left_over);
        deb("href = {substr($path, 18)} . leftover = " . implode(DIRECTORY_SEPARATOR, $left_over));
        return $href;
        // var_dump($x, $path, $uri_path, $href); die;
      }
      elseif($path && $path != '.' && $path != '/' && strrpos($uri_path, basename($path)) === FALSE) {
        array_unshift($left_over, basename($path));
        deb("strip " . basename($path) . " de path");
        $path = dirname($path);
      }
      elseif($path == $uri_path && $path == '/') {
        return FALSE; // root is the only common-directory, but not an Apache vhost
      }
      else {
        // error
        var_dump($path, $uri_path);
        die('error');
        return FALSE;
      }
    }
  }

  function isadmin() {
    if((! isset($_SERVER['PHP_AUTH_USER']) || ! $_SERVER['PHP_AUTH_USER']) &&
       (! isset($_SERVER['REMOTE_USER']) || ! $_SERVER['REMOTE_USER']) )
       return FALSE;

    $cuser = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : $_SERVER['REMOTE_USER'];

    global $O_params;
    if(! isset($O_params['admins']) || ! $O_params) return TRUE; // no admins defined
    $x = explode(',', $O_params['admins']);
    if(! $x) return TRUE; // idem
    return in_array($cuser, $x, TRUE);
  }

/* die if we aren't an registered administrator (from .ini configuration) */
  function noAdmin_bailOut() {
    if(! isadmin()) {
      header("HTTP/1.1 401 Unauthorized");
      exit('Not an admin !');
    }
  }

}