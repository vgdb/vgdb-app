namespace vgdb\ObsoleteCiqualTools {
  use PDO;
  require_once(__DIR__ . '/../connect.php');

  /*
    Consistency check could be done using the following snippet:
    $tab1; $m1; $tab2; $m2;

    
    \vgdb\ObsoleteCiqualTools\__RecetteGetAllNutrimentsSlow($recette_id, $tab1, $m1);
    // == more than 13s for simple recipes

    \vgdb\CiqualTools\GetAllNutrientsForRecipe($recette_id, $tab2, $m2)
    \vgdb\CiqualTools\ComputeRecetteValNut($tab2, $m2);
    // == no more than 0.5s

    print_r(array_udiff_assoc($tab1, $tab2, function($t1, $t2) { return count(array_diff_assoc($t1, $t2)); }));die;
  */
  function __RecetteGetAllNutrimentsSlow($recette_id, &$tab = [], &$masse_recette = 0) {
    global $db, $DBPX;

    $tab = $db->query("SELECT ORIGCPCD, C_ORIGCPNMABR FROM {$DBPX}COMPONENT ORDER BY ORIGCPCD")->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
    $masse_recette = NULL;
    foreach($tab as $k => &$v) {
      $product_table = \vgdb\CiqualTools\RecetteGetNutrimentsByCPNMABR($recette_id, $v['C_ORIGCPNMABR']);
      if(!$masse_recette) $masse_recette = array_sum(array_map(function($n) { return $n['masseabs']; }, $product_table));
      $somme_nut = \Ciqual\Component\sum(\Ciqual\Component\getAllQuantities($product_table, CIQUAL_CPN_CONSERVE), CIQUAL_CPN_SET0);
      $v['somme'] = $somme_nut / $masse_recette * 100;
    }
  }



  /* Here we multiply by (100 / $masse_recette).
     "somme" is the nutritional value for 100g of these ingredients.
     $approx_recette on of VGDB_APPROX_IGNORE, VGDB_APPROX_KEEP, VGDB_APPROX_DISCARD

     if($opt & CIQUAL_CPN_THROW)
     $throwed_ORIGCPCD will return an array of the problematic ORIGCPCD containing an array of several corresponding
     problematic $ORIGFDCD.
     Eg:
     $throwed_ORIGCPCD = Array ( [10110] => Array ( [0] => 11043 ) ) // 10110 = Sodium, 11043 = Abricot, séché, dénoyauté 
     meaning that the values of the Sodium of the Abricot, in one of the recipes, is unknown.
     It helps identifying and avoiding possibly buggy calculations of means */
  // \vgdb\ObsoleteCiqualTools\ComputeRecetteValNut($data['per-rid'], $m, $stats_tab[$rid], $approx_recette);
  function ComputeRecetteValNut($all_ingredients, $masse_recette, &$tab = [], $approx_recette = NULL, &$throwed_ORIGCPCD = []) {

    foreach($all_ingredients as $ORIGCPCD => $ingredients) {
      $somme = NULL;

      // as we rather want to avoid undefined in order to generate CSV or HTML table
      // FETCH_GROUP induces that all ingredients from $ingredients share the same value of
      // $ingredients[X]['C_ORIGCPNMABR'] which is a key of the COMPONENT table and we group by COMPONENT.ORIGCPCD
      $tab[$ORIGCPCD] = ['C_ORIGCPNMABR' => $ingredients[0]['C_ORIGCPNMABR'],
                         'somme' => NULL /* this is what we gonna fill below with $somme */ ];

      /* ==== 1: from the SELVALtexte, the masseabs of all the ingredients of a recipe
         for a given nutrient, we get a list of their absolute quantity of nutrient
         for each converting volume to masse on-the-fly. */
      $nutquantities = array_map(
        function ($v) {
          return \Ciqual\Component\divideOne($v['SELVALtexte'],
                                             100 / $v['masseabs'], CIQUAL_CPN_CONSERVE);
        },
        $ingredients);


      // warning: here is handled the "trace", "> à" and other non-numeric value
      // which could make the result unreliable...

      // There is one NULL among the quantities ...
      if(($x = array_search(NULL, $nutquantities, TRUE))) {
        $throwed_ORIGCPCD[$ORIGCPCD][] = $ingredients[$x]['ORIGFDCD'];
        /* It does not means that this ingredients does *not* contain this nutrient !
           It's just unknown.
           So we can't calc for sure the total quantity of this nutrient for the whole
           recipe. Keep somme = NULL for it and skip to the next nutrient, as well
           for CIQUAL_CPN_THROW than for CIQUAL_CPN_CONSERVE */
        continue;
      }

      // Here lets remember that $throwed_ORIGCPCD could be common to several successive call
      // in order to keep the knownledge about unknown nutrient values.
      // So if this $ORIGCPCD already got one wrong value in a previous recipe.
      // The total for the whole dish would for sure be unreliable.
      // So...
      if(array_key_exists($ORIGCPCD, $throwed_ORIGCPCD)) {
        // we could just skip over an uneeded computation
        if($approx_recette == VGDB_APPROX_DISCARD) continue;

        /* XXX: or we could also compute this value and delegate to the "final mean computation"
           the task of ignoring this nutrient.
           This is useful if we are going to not only show the Total for the whole dish
           but also subtotal for every recipes. */
      }

      /* ==== 2: In order to get the amount of this nutrient, we will first sum()
         all their absolute nutritive quantities. Eg: $sommeabs is the total amount of Sodium
         for this recipe (which could be 580 g) */
      if($approx_recette == VGDB_APPROX_KEEP) {
        $sommeabs = \Ciqual\Component\sum($nutquantities, CIQUAL_CPN_APPEND_APPROX);
      }
      elseif($approx_recette == VGDB_APPROX_IGNORE) {
        $sommeabs = \Ciqual\Component\sum($nutquantities, CIQUAL_CPN_SET0);
      }
      elseif($approx_recette == VGDB_APPROX_DISCARD) { // discard sum()ing for this nutrient
        die('TODO 158799');
        $sommeabs = \Ciqual\Component\sum($nutquantities, "XXXXX");
      }
      else { // what to with non-numeric values if no directive is given ?
        die('something bad happened !');
      }

      /* ==== 3: We truly define the way sum()ing nutrients will appear in the end.
         If the sum() is numeric, just divide by the recipes's masse then multiply by 100.
         Otherwise use \Ciqual\Component\divideOne() */
      if(is_numeric($sommeabs)) {
        $somme = $sommeabs / $masse_recette * 100;
      }
      elseif($approx_recette == VGDB_APPROX_KEEP) {
        /* Here we apply the porportionality for 100g (with a check for NULL and others)
           because we are clever and attempt hardly to reach a meaningful result */
        // $somme = \Ciqual\Component\divideOne($sommeabs, 100 / $masse_recette, CIQUAL_CPN_CONSERVE);
        // echo '>>>>>>>> (' . $sommeabs . ") * 100 / $masse_recette == ";
        $somme = \Ciqual\Component\myeval( '(' . $sommeabs . ") * 100 / $masse_recette");
        // echo "$somme<br/>";
      }
      elseif($approx_recette == VGDB_APPROX_IGNORE) {
        // just round NON numeric vals
        $somme = \Ciqual\Component\divideOne($sommeabs, 100 / $masse_recette, CIQUAL_CPN_SET0);
      }
      elseif($approx_recette == VGDB_APPROX_DISCARD) { // discard sum()ing for this nutrient
        $somme = NULL;
        $throwed_ORIGCPCD[$ORIGCPCD][] = $ingredients[$x]['ORIGFDCD'];
      }
      else { // what to with non-numeric values if no directive is given ?
        die('something bad happened !');
      }

      if($ORIGCPCD == 10150) { var_dump(__FILE__ . ':' . __LINE__, $ingredients, $nutquantities, $sommeabs, $somme); }
      // if($ORIGCPCD == 10290) { var_dump($ingredients, $nutquantities, $sommeabs); die; }

      // set $somme
      $tab[$ORIGCPCD]['somme'] = $somme;
    }
  }



