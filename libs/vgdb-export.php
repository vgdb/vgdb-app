<?php

namespace vgdb\export;
use PDO;

require_once('vgdb-export.class.php');

// aggregation
const AGR_LVL_RECETTE =	1;
const AGR_LVL_PLAT		=	2;

// format
const FMT_TABLE				= 3;
const FMT_CSV					= 4;

// output
const OUT_DOWNLOAD		= 5;
const OUT_SAVE				=	6;

function divide_nutris_and_minmax($code_ingredient, $ingr, $nutri, $coef) {
  $ret = [];
  $i = 0;
  foreach($nutri as $k => $v) {
    //if($code_ingredient == 18340 && $v['ORIGCPCD'] == 400) { var_dump($v, func_get_args()); die('a'); }
    $ret[] = \Ciqual\Component\divideOne($v['SELVALtexte'], $coef);
    $i++;
    if($i < 5) continue; // NO valmin/max for energy (the first 4)
    // VALMIN & VALMAX : always numeric ! ouf !
    $ret[] = !empty($v['VALMIN']) ? $v['VALMIN'] * $coef : NULL;
    $ret[] = !empty($v['VALMAX']) ? $v['VALMAX'] * $coef : NULL;
    $ret[] = $v['N'];
  }
  return $ret;
}

/*
  N has no sense if we aggregate per recipe or per dish ($with_n)
 */
function insert_minmax($nutriments_intitules, $with_n = TRUE) {
  $t = [];
  $i = 0;
  foreach($nutriments_intitules as $n) {
    $t[] = $n;
    $i++;
    if($i < 5) continue; // NO valmin/max for energy (the first 4)
    $t[] = "VALMIN (Ciqual) pour $n";
    $t[] = "VALMAX (Ciqual) pour $n";
    if($with_n) $t[] = "N (Ciqual) pour $n";
  }
  return $t;
}


// 0.0001156 => 0.00012
// 8.166 => 8.27
function adaptiveRound($n, $nan = 'n.c.') {
  if(! is_numeric($n)) {
    if(strpos($n, '<', 0) === FALSE) return $nan;
    return '< ' . adaptiveRound(substr($n, 2));
  }
  // integers stay the same
  if(is_int($n) || strpos($n, '.') === FALSE) return $n;

  $dec_part = $n - floor($n);
  $number_0s = - log10($dec_part);
  // log10(0.0001) = -4
  // log10(0.000n) = -3.x (for n != 1)
  if(is_int($number_0s)) $number_0s--;
  else $number_0s = floor($number_0s);

  return round($n, $number_0s + 2);

  /* no:
     we don't do the below: debate about what is precision */

  // $r = round($n, $number_0s + 2);
  // 8.1 => 8.10
  if(strlen($r - floor($r)) - 2 == 1)
    return number_format(round($n, $number_0s + 2), 2, '.', '');
  // digits with more than 1 decimal are just round as they should
  else
    return $r;
}

function _export_recettes($plat_id, $nom_vg, $nom_tradi, $exportClass) {
  global $db, $DBPX;

  $recettes_selected_for_export = \vgdb\Recette\getFromPlatID($plat_id);
  foreach($recettes_selected_for_export as $r) {
    $can_mean = 1;

    $nom_recette = $r['nom'];
    if($exportClass->filters['filter-recette'] && ! in_array($r['id'], $exportClass->filters['filter-recette'])) continue;

    $tous_ingredients = \vgdb\Recette\getIngredients($r['id'],
                                                     0x02,
                                                     PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);

    $tmp_tab = $tmp_tab_m = $stats = [];
    \vgdb\CiqualTools\GetAllNutrientsForRecipe(
      $r['id'],
      $tmp_tab,
      $tmp_tab_m,
      VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_KEEP|VGDB_TRACES_KEEP,
      \VgdbExport::opt('rangenut'));

    $can_mean2 = \vgdb\CiqualTools\ComputeRecetteValNut(
      $tmp_tab,
      $tmp_tab_m,
      $stats, 
      VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_KEEP|VGDB_TRACES_KEEP,
      \VgdbExport::opt('rangenut'));

    if(!$tous_ingredients) {
      // no ingredients, ignore recipe or die() if STRICT
      if(\VgdbExport::opt('strict')) die("STRICT ERROR: Recette <a href=\"ingredients.php?recette_id={$r['id']}\">$nom_recette</a> sans aucun ingrédients !");
      continue;
    }
    // et sommons le total
    $masse_recette = \vgdb\Ingredient\sum($tous_ingredients);
    if($masse_recette === FALSE) {
      if(\VgdbExport::opt('strict')) {
        die("ERROR: La conversion en grammes d'un des ingrédients de la recette {$r['id']} retourne 0 !");
      }
      else {
        $nutriments = array_fill(0, \Ciqual\Component\NB_NUTRI, 'n.c.');
        $can_mean = 0;
      }
    }

    // var_dump($masse_recette, $tous_ingredients, $tmp_tab, $tmp_tab_m);die;
    // var_dump($can_mean2, $stats);die;

    // La recette "pèse" donc $masse_recette grammes.

    /* Ainsi:
       - chaque ingrédient participe d'un total de $masse_recette
       - le dosage de chaque nutriment est donné pour 100g d'un ingrédient
       Or nous souhaitons les nutriments pour 100g de plat final.
       Pour obtenir les nutriments, et le total des ingrédients faisant 100g,
       on peut donc additionner les nutriments et les déterminer pour "100g de plat final".

       Pour une valeur de nutriment (en mg/100g) X :
       X / 100 : valeur pour 1g de cet aliment
       X / 100 * $quantite_normalisée : valeur nutritionnelle pour cet aliment pour la recette
       X / 100 * $quantite_normalisée * 100 / $masse_recette : valeur nutritionnelle pour cet aliment pour 100g de la recette
       = X / $masse_recette * $quantite_normalisée
       avec $masse_recette = Sum($quantite_normalise)
    */

    // var_dump($nom_recette, count($tous_ingredients), $masse_recette); continue;
    foreach($tous_ingredients as $code_ingredient => $i) {
      $i_info = \Ciqual\Food\get($code_ingredient);
      $nom_ingredient = $i_info['ORIGFDNM'];

      if($can_mean) {
        $quantite_normalisee = \vgdb\Ingredient\masse($i['unite'], $i['quantite'], $i['masvol']);
        $i_pourcent_masse = round($quantite_normalisee / $masse_recette * 100, 2);
      } else {
        $quantite_normalisee = NULL;
        $i_pourcent_masse = "?";
      }


      if(! \VgdbExport::opt('valnut')) {
        $nutriments = [];
      }
      elseif($can_mean) {
        // cf ci-dessus
        $coef = $quantite_normalisee / $masse_recette;
        // var_dump($i); echo "==== $coef = $quantite_normalisee / $masse_recette<hr/>";


        // SELECT d.ORIGCPCD, d.SELVALtexte, d.VALMIN, d.VALMAX, d.N FROM vgdb_COMPONENT C LEFT JOIN vgdb_COMPILED_DATA d ON C.ORIGCPCD = d.ORIGCPCD WHERE d.ORIGFDCD = 7225;

        $nutri = $db->query(sprintf(
          "SELECT ORIGCPCD, REPLACE(SELVALtexte, ',', '.') AS SELVALtexte %s FROM ${DBPX}COMPILED_DATA WHERE ORIGFDCD = %s ORDER BY ORIGCPCD",
          \VgdbExport::opt('rangenut') ? ", IFNULL(VALMIN, '') AS VALMIN, IFNULL(VALMAX, '') AS VALMAX, IFNULL(N, '') AS N" : "",
          $code_ingredient))->fetchAll(PDO::FETCH_ASSOC);

        if(count($nutri) != \Ciqual\Component\NB_NUTRI) {
          die(sprintf("ERROR: nb de nutriments != %d pour %s (%s)", \Ciqual\Component\NB_NUTRI, $nom_ingredient, $code_ingredient));
        }

        // var_dump($nutri, array_map(function($n) { return $n['SELVALtexte']; }, $nutri));
        if(\VgdbExport::opt('rangenut')) $nutriments = divide_nutris_and_minmax($code_ingredient, $i, $nutri, $coef);
        else $nutriments = \Ciqual\Component\divideAll($nutri, $coef);
        //var_dump($nutriments);die;
      }

      array_walk($nutriments, function (&$v) { $v = adaptiveRound($v, $v); });

      if($exportClass->is_table) {
        $nutriments = str_replace("<", "&lt;", $nutriments);
        $exportClass->addTableLine(array_merge([
          $nom_vg, $nom_tradi,
          sprintf('<a href="ingredients.php?recette_id=%d">%s</a>', $r['id'], $nom_recette),
          $code_ingredient, $nom_ingredient, $i_pourcent_masse . '%' ],
                                             
                                               $nutriments));
      }
      else {
        $exportClass->addCSVLine(array_merge([$nom_vg, $nom_tradi,
                                              $nom_recette, $code_ingredient,
                                              $nom_ingredient, $i_pourcent_masse ],

                                             $nutriments));
      }
    }
    $exportClass->changeBGColor();
  }
}


function _export_recette2($plat_id, $nom_vg, $nom_tradi, $exportClass, $r) {
  global $db, $DBPX;

  $nom_recette = $r['nom'];
  $tous_ingredients = \vgdb\Recette\getIngredients($r['id'], 0x03, PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);

  if(! $tous_ingredients) {
    // no ingredients, ignore recipe or die() if STRICT
    if(\VgdbExport::opt('strict')) die("STRICT ERROR: Recette <a href=\"ingredients.php?recette_id={$r['id']}\">$nom_recette</a> sans aucun ingrédients !");
    return;
  }

  $ingredients_with_nutrival = $masse_recette = $norma = $stats = [];
  \vgdb\CiqualTools\GetAllNutrientsForRecipeNEXT(
    $r['id'],
    $ingredients_with_nutrival,
    VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_KEEP|VGDB_TRACES_KEEP,
    \VgdbExport::opt('rangenut')
  );

  // recipe's masse is initialized here for the 1st $ORIGCPCD since masseabs values are identical for all of them
  // but the recipe could be NULL also (no ingredients)
  // and could also exist the '' index (if no ingredient had a kcal data [328])
  $food_list = reset($ingredients_with_nutrival['per-cpn']);
  $masses = array_map(function($n) { return $n['masseabs']; }, $food_list);

  if(! $food_list || ! $masses) {
    var_dump($food_list);
    die("unexpected ERROR: sum()ing mass for recipe {$r['id']}, no ingredients ? please take a screenshot and reach the admin\n");
  }
  $masse_recette = array_sum($masses);
  if(! $masse_recette) {
    if(\VgdbExport::opt('strict')) die("STRICT ERROR: calcul de masse de recette impossible, recette {$r['id']} !");
    return;
  }


  if(\vgdb\CiqualTools\missingDensity($food_list)) {
    if(\VgdbExport::opt('strict')) {
      die(sprintf("STRICT ERROR: ingrédient sans masse volumique ou sans données nutritionnelles, recette %d, food code %d", $r['id'], $food_list));
    }
    return;
  }

  \vgdb\CiqualTools\applyIngredientsNormalization($ingredients_with_nutrival['per-cpn'],
                                                  $masse_recette,
                                                  \VgdbExport::opt('rangenut'));

  // var_dump($ingredients_with_nutrival['per-fdcd'][100017]);die;
  foreach($tous_ingredients as $code_ingredient => $i) {
    $nom_ingredient = $i['ORIGFDNM'];
    $index = $ingredients_with_nutrival['per-fdcd'][$code_ingredient];

    $pc_masse = round(current($index)['masseabs'] / $masse_recette * 100, 2);

    $round_nutriments = array_map(function ($v) { return $v['NormSELVALtexte']; }, $index);

    $nutriments = [];
    $i=0;
    foreach(\Ciqual\Component\getCPN() as $k => $v) {
      $nutriments[] = array_key_exists($k, $index)
        ? adaptiveRound($index[$k]['NormSELVALtexte'])
        : 'n.c.'; // this ingredient as NO line for this nutrient in COMPILED_DATA

      $i++;
      if($i < 5 || ! \VgdbExport::opt('rangenut')) continue;

      if(!array_key_exists($k, $index)) {
        $nutriments[] = 'n.c.';
        $nutriments[] = 'n.c.';
        $nutriments[] = 'n.c.';
      } else {
        $nutriments[] = adaptiveRound($index[$k]['VALMIN']);
        $nutriments[] = adaptiveRound($index[$k]['VALMAX']);
        $nutriments[] = $index[$k]['N'] ? : 'n.c.';
      }
    }

    if($exportClass->is_table) {
      $nutriments = str_replace("<", "&lt;", $nutriments);
      $exportClass->addTableLine(array_merge([
        sprintf('<a href="recettes.php?plat_id=%d">%s</a>', $plat_id, $nom_vg),
        $nom_tradi,
        sprintf('<a href="ingredients.php?recette_id=%d">%s</a>', $r['id'], $nom_recette),
        $code_ingredient, $nom_ingredient, $pc_masse . '%' ],
                                             
                                             $nutriments));
    }
    else {
      $exportClass->addCSVLine(array_merge([$nom_vg, $nom_tradi,
                                            $nom_recette, $code_ingredient,
                                            $nom_ingredient, $pc_masse ],

                                           $nutriments));
    }
  }
}


function _export_recettes_means($recettes_selected_for_export, $nom_vg, $nom_tradi, $exportClass) {
  if(! $recettes_selected_for_export) {
    // TODO: if strict: warning ?
    return;
  }

  $stats = ['per-rid' => [],
            'masse' => [],
            'recettes_totaux' => [],
            'total' => [],
            'meta' => ['count' => NULL,
                       'ignored' => NULL,
                       'can_mean' => NULL] ];

  if($exportClass->filters['filter-recette']) {
    $recettes_selected_for_export = array_intersect_key($recettes_selected_for_export,
                                                        $exportClass->filters['filter-recette']);
  }


  $tab['count'] = count($recettes_selected_for_export);

  foreach($recettes_selected_for_export as $r) {
    $rid = $r['id'];
    \vgdb\CiqualTools\GetAllNutrientsForRecipe($rid,
                             $stats['per-rid'][$rid],
                             $stats['masse'][$rid],
                             /* individual ingrédients approximation handling during DB retrieval */
                             VGDB_ALL_KEEP,
                             \VgdbExport::opt('rangenut') /* VALMIN, VALMAX */ );
  }

  \vgdb\CiqualTools\RecipeNormalizationFromData(
    $stats,
    $stats,
    VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_ROUND|VGDB_TRACES_KEEP, /* recette */
    \VgdbExport::opt('rangenut') /* VALMIN, VALMAX */ );

  /*
  \vgdb\CiqualTools\PlatGetStatistics(
    $stats,
    $plat_id,
    VGDB_ALL_KEEP,
    VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_ROUND|VGDB_TRACES_KEEP,
    \VgdbExport::opt('rangenut'));
  */
  
  if(\VgdbExport::opt('strict') && count($stats['meta']['ignored'])) {
    die("STRICT ERROR: Recipes " . implode(', ', $stats['meta']['ignored']) . " cause trouble");
  }
  // var_dump($stats);die;

  foreach($recettes_selected_for_export as $r) {
    $nutriments = [];
    $nom_recette = $r['nom'];
    $nb_ingredients = $r['count'];

    // var_dump($stats['recettes_totaux'][$r['id']]);die;
    if(! isset($stats['recettes_totaux'][$r['id']]) || ! $stats['recettes_totaux'][$r['id']]) {
      // big issue for this one, probably unfilled volumic mass
      if(\VgdbExport::opt('strict')) die("ERROR: recette {$r['id']} returns NULL !");
      if(\VgdbExport::opt('rangenut')) $nutriments = array_fill(0, \Ciqual\Component\NB_NUTRI * 3 /* valsel, valmin & valmax */ - 4 /* no min/max for energy*/ , 'n.c.');
      else $nutriments = array_fill(0, \Ciqual\Component\NB_NUTRI, 'n.c.');
    } else {
      $i=0;
      foreach($stats['recettes_totaux'][$r['id']] as $v) {
        $nutriments[] = adaptiveRound($v['somme']);
        $i++;
        if($i < 5) continue; // NO valmin/max for energy (the first 4)
        if(\VgdbExport::opt('rangenut')) {
          $nutriments[] =  adaptiveRound($v['VALMIN']);
          $nutriments[] =  adaptiveRound($v['VALMAX']);
        }
      }
    }

    // array_walk($nutriments, function (&$v) { $v = is_numeric($v) ? round($v, 2) : $v; return; });

    if($exportClass->is_table) {
      $nutriments = str_replace("<", "&lt;", $nutriments);
      // NULL in $nutriments should represent an empty <td></td> (actually replace in array_map())
      $exportClass->addTableLine(array_merge(
        [
          sprintf('<a href="recettes.php?plat_id=%d">%s</a>', $r['id_plat'], $nom_vg),
          $nom_tradi,
          sprintf('<a href="ingredients.php?recette_id=%d">%s</a>', $r['id'], $nom_recette),
          $nb_ingredients
        ],
        $nutriments));
      $exportClass->changeBGColor();
    }
    else {
      $exportClass->addCSVLine(array_merge( [ $nom_vg, $nom_tradi, $nom_recette ], $nutriments ));
    }
    //if($r['id'] == 2) { var_dump($stats['recettes_totaux'][$r['id']], $nutriments);die; }
  }
}


function _export_plats_means($plat_id, $nom_vg, $nom_tradi, $exportClass) {
  $nutriments = NULL;
  $usable = FALSE;
  $nb_for_mean = 0;
  $stats = [];

  $can_mean = \vgdb\CiqualTools\PlatGetStatistics(
    $stats,
    $plat_id,
    VGDB_ALL_KEEP, /* individual ingrédients approximation handling during DB retrieval */
    VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_ROUND|VGDB_TRACES_KEEP, /* recette */
    \VgdbExport::opt('rangenut') /* VALMIN, VALMAX */ );

  if(! \VgdbExport::opt('valnut')) {
    $nutriments = [];
    $nb_for_mean = $stats['count'] - count($stats['meta']['ignored']);
  }
  else {
    if($can_mean) {
      \vgdb\CiqualTools\_PlatComputeStatistics($stats['total'],
                                               $stats['meta'],
                                               $stats['recettes_totaux'],
                                               VGDB_DASH_DISCARD|VGDB_NULL_DISCARD|VGDB_APPROX_ROUND|VGDB_TRACES_KEEP,
                                               \VgdbExport::opt('rangenut') /* VALMIN, VALMAX */ );
    } else {
      $stats['meta']['count'] = count($stats['recettes_totaux']);
      $stats['total'] = current($stats['recettes_totaux']);
      array_walk($stats['total'], function (&$n) { $n['somme'] = NULL; });
    }


    //var_dump($stats);die;
    if(\VgdbExport::opt('strict') && $stats['meta']['ignored']) {
      die("STRICT ERROR: Recipes " . implode(', ', $stats['meta']['ignored']) . " cause trouble [plats]");
    }

    $nb_for_mean = $stats['meta']['count'];
    $usable = $stats['meta']['can_mean'];
  }


  if(! $usable && is_null($nutriments)) {
    if(\VgdbExport::opt('rangenut')) $nutriments = array_fill(0, \Ciqual\Component\NB_NUTRI * 3 /* valsel, valmin & valmax */ - 4 /* no min/max for energy*/ , 'n.c.');
    else $nutriments = array_fill(0, \Ciqual\Component\NB_NUTRI, 'n.c.');
  }
  elseif(! $nb_for_mean) return;
  elseif(! $usable) {
    // this is the  ! VgdbExport::opt('valnut')) case : just print
  }
  else {
    $i=0;
    foreach($stats['total'] as $v) {
      $nutriments[] = adaptiveRound($v['somme']);
      $i++;
      if($i < 5) continue; // NO valmin/max for energy (the first 4)
      if(\VgdbExport::opt('rangenut')) {
        $nutriments[] =  $v['VALMIN'] ? : 'n.c.';
        $nutriments[] =  $v['VALMAX'] ? : 'n.c.';
      }
    }
  }

  // array_walk($nutriments, function (&$v) { $v = is_numeric($v) ? round($v, 2) : $v; return; });

  if($exportClass->is_table) {
    $nutriments = str_replace("<", "&lt;", $nutriments);
    // NULL in $nutriments should represent an empty <td></td>
    $exportClass->addTableLine(array_merge(
      [
        sprintf('<a href="recettes.php?plat_id=%d">%s</a>', $plat_id, $nom_vg),
        $nom_tradi,
        $nb_for_mean
      ],
      $nutriments));
    $exportClass->changeBGColor();
  }
  else {
    $exportClass->addCSVLine(array_merge( [ $nom_vg, $nom_tradi, $nb_for_mean ], $nutriments ));
  }
}
