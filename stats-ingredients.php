<?php

require_once("libs/libvgdb-ciqual.php");



// https://github.com/ramsey/array_column/blob/master/src/array_column.php
function simple_array_column($paramsInput, $paramsColumnKey) {
  $resultArray = [];
  foreach ($paramsInput as $row) {
    $key = $value = null;
    $keySet = $valueSet = false;

    if (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
      $valueSet = true;
      $value = $row[$paramsColumnKey];
    }

    if ($valueSet) {
      if ($keySet) {
	$resultArray[$key] = $value;
      } else {
	$resultArray[] = $value;
      }
    }
  }
  return $resultArray;
}



// TODO: SQL refetch (format is !=)
// should use $ingr_sql_liste
$ingr_sql_indexed_liste = \vgdb\Recette\getIngredients($recette_id, 0x03, PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE); // grab (very) extended data
$missing_codes = [];
$masse_recette = \vgdb\Ingredient\sum($ingr_sql_indexed_liste, $missing_codes);
if(! $masse_recette) {
  $masse_recette = "?";
  if(! isset($last_code)) { // don't override "important" messages
    /* $missing_ingr = array_filter($ingr_sql_liste, function($v) use($missing_codes) { return in_array($v['ORIGFDCD'], $missing_codes); });
       $missing_ingr = array_filter($ingr_sql_liste, function($v) use($missing_codes) { return in_array($v['ORIGFDCD'], $missing_codes); });
       $missing_name = array_map(function($v) { return $v['ORIGFDNM']; }, $missing_ingr); */
    $missing_name = array_map(function($v) use($ingr_sql_indexed_liste) { return $ingr_sql_indexed_liste[$v]['ORIGFDNM']; }, $missing_codes);
    list($last_code, $last_message) = [1, sprintf(
      "Statistiques inconnues pour \"<em>%s</em>\". Ingrédient%s sans <a href=\"masvol.php\">masse volumique renseignée</a>",
      implode('</em>", "<em>', $missing_name), count($missing_name) > 1 ? "s" : ""
    )];
  }
}
else {
  $nutri_lines = $ingr_lines = [];

  // simply to sort ingredients array lines by decreasing weight 
  uasort($ingr_sql_indexed_liste, function($a, $b)
         {
           return
             \vgdb\Ingredient\masse($a['unite'], $a['quantite'], $a['masvol'])
             <
             \vgdb\Ingredient\masse($b['unite'], $b['quantite'], $b['masvol']);
         });

  /* This build 2 arrays indexed by $ORIGFDCD.
     The first one, $ingr_lines stores text/info data about the ingredient generally and in this recipe :
     - name
     - relative masse
     The second one, $nutri_lines stores all the \Ciqual\Component\NB_NUTRI nutrients, indexed by $ORIGCPCD
     Both will be used to build the statistical table.
  */
  foreach($ingr_sql_indexed_liste as $ORIGFDCD => $ingredient) {
    // statistics
    $masse = \vgdb\Ingredient\masse($ingredient['unite'], $ingredient['quantite'], $ingredient['masvol']);
    $coef = $masse / $masse_recette;
    $masse_relative = $coef * 100;

    $nutri = \Ciqual\Component\getAll($ORIGFDCD, TRUE, TRUE);
    /* Here we ask for components (quantities) for a given quantity of FOOD which probably not be 100g.
       We retrieved the header of these components which are expressed eg:
       "Eau (g/100g)".
       This is obviously error-prone since after divideAll_r() applied, the quantity will not be representative of 100g.
       A substitution will be needed eg :
       array_walk($nutri, function(&$v) { $v['C_ORIGCPNMABR'] = preg_replace('! \(\w+/100g\)$!u', '', $v['C_ORIGCPNMABR']); });
       See the preg_replace() below */
    \Ciqual\Component\divideAll_r($nutri, $coef, CIQUAL_CPN_CONSERVE);

    // ['divide'] because we use TRUE above (we keep the raw-value, add the component's name
    // and add the proportional value as a new array key
    $nutri_lines[$ORIGFDCD] = array_map(function ($n) { return $n['divide']; }, $nutri);
    ksort($nutri_lines[$ORIGFDCD]); // see below
    $ingr_lines[$ORIGFDCD] = [ 'nom' => $ingredient['ORIGFDNM'], 'masserel' => $masse_relative ];
  }

  /* sum()ing to generate the "total"
     equivalent to using :
     ## \vgdb\CiqualTools\GetAllNutrientsForRecipe($recette_id, $tab2, $m2);
     puis
     ## \vgdb\CiqualTools\ComputeRecetteValNut($tab2, $m2)
     but here we rather try compute the $total since we already fetched all the necessary atomic data to build the table
     Note: array_column() is PHP 5.5 (OVH is 5.4)

     TODO !!! array_sum() will add 0 for NULL or for "> X", when it should throw and error-out a bunch of approx' warnings !!
     This is why we need \Ciqual\Component\sum() */
  foreach($nutri as $i => $v) $total[$i] = array_sum(simple_array_column($nutri_lines, $i));
  $total_calories_100g = $total[\Ciqual\Component\KCAL_CODE];

  // treat NULL values, special value + round()
  // apply *after* total calcultation to avoid the round()ing imprecision
  foreach($nutri_lines as &$v) \vgdb\CiqualTools\FormatNutVal($v, NULL);
  // then round() the value of the total
  \vgdb\CiqualTools\FormatNutVal($total, NULL);

  // le dernier appel à \Ciqual\Component\getAll() nous a fourni $nutri qui contient l'en-tête du tableau
  $stats = [ "data" => $ingr_lines,
            "nutri" => $nutri_lines,
            "header" => array_map(function($n) { return preg_replace('! \((\w+)/100g\)$!u', ' (en \1)', $n['C_ORIGCPNMABR']); }, $nutri),
            "total" => $total ];

  // template stats-recette.html use a simple foreach() loop to generate the table. We *must* ensure that keys (ORIGCPCD) are correctly ordered !
  // each $nutri_lines elements has also be sorted in the above loop
  ksort($stats['total']);
  ksort($stats['header']);
  
  list($masse_recette2, $somme_calories, $total_calories_100g2) = reliable_check_kcal($recette_id);
  if(intval($total_calories_100g) != intval($total_calories_100g2)) {
    list($last_code, $last_message) = [1, "ERREUR de CALCUL des calories : $total_calories_100g <> $total_calories_100g2 !" ];
  }
  $stats['misc'] = ['masse2' => $masse_recette2, 'sum_kcal2' => $somme_calories ];
  var_dump($stats);


  // $tab2; $m2;
  // \vgdb\CiqualTools\RecetteGetAllNutriments($recette_id, $tab2, $m2);
  // var_dump($tab2);
}
