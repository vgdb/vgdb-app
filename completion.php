<?php
/* vgdb
 * Copyright (C) 2014  Raphaël Droz <raphael.droz+floss@gmail.com>
 *
 * This file is part of vgdb <http://gitorious.org/vgdb/vgdb-app/>.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General
 * Public License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

require_once("libs/libCiqual.php");

switch ($_GET["t"]) {
  // ORIGGPFR
case "catg":
  break;
  
  // ORIGFDNM
case "ingr":
  $terms = explode(' ', $_GET["term"]);
  $sql = "SELECT id, CONCAT(ORIGGPFR, ' > ', ORIGFDNM) AS label FROM {$DBPX}fulltxt WHERE ";
  $sql_where_clause = array();
  foreach($terms as $term) {
    $sql_where_clause[] = "tokens LIKE " . $db->quote("%" . $term . "%");
  }
  $sql .= implode(" AND ", $sql_where_clause);
  print json_encode($db->query($sql)->fetchAll(PDO::FETCH_ASSOC));
  break;
  
  // unités (pour les recettes)
case "unit":
  break;
}
  
